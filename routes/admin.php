<?php

Route::get('admin/sign-in', ['uses' => 'Admin\Auth\LoginController@getLogin', 'as' => 'admin.login']);
Route::post('admin/sign-in', 'Admin\Auth\LoginController@postLogin');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth:admin'], function () {
    Route::get('logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'admin.logout']);
    Route::post('changePassword', 'Auth\AdminController@changePassword');

    Route::get('categories/childs', ['uses' => 'CategoryController@childs', 'as' => 'categories.childs']);
    Route::resource('categories', 'CategoryController')->except(['create', 'show']);
    Route::resource('tags', 'TagController')->except(['show']);
    Route::resource('advertisements','AdvertisementController')->except(['create','show']);
    Route::resource('films', 'FilmController');
    Route::get('films/{slug}/watch', 'FilmController@watchFilm');
    Route::resource('actors','ActorController');
    Route::resource('directors', 'DirectorController');

    // Route dashboard
    Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');
    Route::get('dashboard/mostview', 'DashboardController@mostFilmView');
    Route::get('dashboard/viewedmonth', 'DashboardController@viewedFilmByMonth');
    Route::get('dashboard/viewbybrowser', 'DashboardController@reportViewedByBrowser');
    Route::get('dashboard/viewbydevice', 'DashboardController@reportViewedByDevice');
    Route::get('dashboard/viewedinmonth',['as'=>'viewedInMonth','uses'=>'DashboardController@viewedFilmInMonth']);

    Route::resource('trailers','TrailerController')->except(['create','show']);
    Route::resource('users','UserController');
    Route::resource('comments','CommentController');
    Route::resource('episodes','EpisodeController');

    Route::get('media-management', ['as' => 'mediashow', 'uses' => 'MediaController@index']);
    Route::delete('mediaremove/{id}', ['as' => 'media.deletefile', 'uses' => 'MediaController@destroy']);
    Route::delete('mediasremove', 'MediaController@deleteManyImage')->name('media.deletefiles');

    Route::resource('global-setting', 'GlobalSettingController');
});

