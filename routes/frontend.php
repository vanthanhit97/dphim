<?php

Route::group(['namespace' => 'FrontEnd'], function () {
	Route::get('/', ['as' => 'front-end.index', 'uses' => 'FrontEndController@homePage']);

	Route::get('sign-up', ['as' => 'front-end.sign-up', 'uses' => 'FrontEndController@getSignUp']);
	Route::post('sign-up', ['as' => 'front-end.sign-up', 'uses' => 'FrontEndController@postSignUp']);
	Route::get('login', ['as' => 'front-end.login', 'uses' => 'LoginController@getLogin']);
	Route::post('login', ['as' => 'front-end.login', 'uses' => 'LoginController@postLogin']);

	Route::group(['middleware' => ['auth:user']], function () {
		Route::get('profile', ['as' => 'front-end.profile', 'uses' => 'FrontEndController@showProfile']);
		Route::post('changepassword', 'LoginController@changepassword');
		Route::get('logout', ['as' => 'front-end.logout', 'uses' => 'LogoutController@getLogout']);
	});

	Route::get('the-loai/{slug}', ['as' => 'front-end.genres', 'uses' => 'FrontEndController@getFilmsByGenres']);
	Route::get('quoc-gia/{slug}', ['as' => 'front-end.countries', 'uses' => 'FrontEndController@getFilmsByGenres']);
	Route::get('{slug}', ['as' => 'front-end.films', 'uses' => 'FrontEndController@getFilmsByGenres']);
	Route::get('phim-le/{slug}', ['as' => 'front-end.filmsodd', 'uses' => 'FrontEndController@getFilmsByGenres']);
	Route::get('phim-bo/{slug}', ['as' => 'front-end.filmsseries', 'uses' => 'FrontEndController@getFilmsByGenres']);
	Route::get('phim/{slug}', ['as' => 'front-end.filminfo', 'uses' => 'FrontEndController@getFilmsInfo']);
	Route::get('phim/{slug}/xem-phim.html',['as' => 'front-end.watch', 'uses' => 'FrontEndController@watchFilm']);
	Route::get('phim/tap/{slug}/xem-phim.html', ['as' => 'front-end.episode', 'uses' => 'FrontEndController@episodeOfFilm']);
	Route::get('ho-so/dao-dien/{slug}', ['as' => 'director.profile', 'uses' => 'FrontEndController@getDirectorProfile']);
	Route::get('ho-so/dien-vien/{slug}', ['as' => 'actor.profile', 'uses' => 'FrontEndController@getActorProfile']);
	Route::any('tim-kiem', ['as' => 'search', 'uses' => 'FrontEndController@getSearch']);

}); 

