@extends('frontends.layouts.master')
@section('title', 'Hồ sơ '.$profile->title)
@section('content')
    <div class="film-box film-infor container mt-5 mb-3">
        <div class="row">
            <div class="col-sm-12 col-md-9 p-0">
                <div class="d-flex p-0 flex-wrap main-content">
                    <div class="col-5 p-0 block-movie-info">
						<img src="{{ asset('vendor/images/'.$profile->avatar) }}" alt="" class="img-responsive img-thumbnail">
					</div>
                    <div class="col-7">
                        <h3 class="text-uppercase mycolor w-100 mb-0 pb-0">Hồ sơ {{ $profile->job == 'director' ? 'đạo diễn' : 'diễn viên' }} {{ $profile->first_name.' '.$profile->last_name }}</h3>
                        <div class="col-12 mt-3 border infor-scroll" id="film-scroll">
                                <p class="py-0 my-1"><b>Ngày sinh : </b>{{ date('d/m/Y',strtotime(date($profile->release_date))) }}</p>
								<p class="py-0 my-1"><b>Nghề nghiệp : </b>{{ $profile->job ? $profile->job == 'director' ? 'Đạo diễn' : 'Diễn viên' : 'Chưa có thông tin' }} </p>
								<p class="py-0 my-1"><b>Giới tính : </b>{{ ($profile->gender) == 1 ? 'Nam' : 'Nữ' }} </p>
                                <p class="py-0 my-1"><b>Quốc gia : </b>{{ $profile->country ? $profile->country : 'Chưa có thông tin' }} </p>
                                <p class="py-0 my-1"><b>Chiều cao : </b>{{ $profile->height ? $profile->height.' mét' : 'Chưa có thông tin' }}</p>
                                <p class="py-0 my-1"><b>Cân nặng : </b>{{ $profile->weight ? $profile->weight.' kg' : 'Chưa có thông tin' }}</p>
                                <p class="py-0 my-1"><b>Nhóm máu : </b>{{ $profile->blood_group ? $profile->blood_group : 'Chưa có thông tin' }}</p>
                                <p class="py-0 my-1"><b>Sở thích : </b>{{ $profile->hobby ? $profile->hobby : 'Chưa có thông tin' }}</p>
                        </div>
                    </div>
                </div>
				<div class="films-rela mt-5">
					<h4 class="p-title-box mr-3">Một số phim đã tham gia</h4>
					<div class="d-flex flex-wrap mr-3">
						@foreach ($films as $item)
							<a href="{{ route('front-end.filminfo', $item->slug) }}" class="col-6 col-md-4 p-1" title="{{ $item->title }}">
								<img src="{{ asset('vendor/images/'.$item->avatar) }}" class="w-100 img-responsive img-thumbnail">
								<h5 class="m-2">{{ $item->title }}</h5>
							</a>
						@endforeach
					</div>
					<div class="w-100 float-left mt-4">
						{{ $films->links() }}
					</div>
				</div>
            </div>
            <div class="col-md-3 d-sm-none d-none d-md-flex p-0">
                @include('frontends.blockeds.box-right')
            </div>
        </div>
    </div>
@endsection