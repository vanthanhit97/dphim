@extends('frontends.layouts.master')
@section('title', 'Hồ sơ cá nhân')
@section('content')
    <div class="film-box film-infor container mt-3 mb-3">
        <div class="row">
            <ul class="breadcrumb w-100">
                <li><a href="{{ route('front-end.index') }}" class="text-uppercase font-weight-bold">Trang chủ</a></li>
                <li class="is-active">Hồ sơ cá nhân</li>
            </ul>
        </div>
        <div class="row">
            <div class="user-profile mt-3">

                    <div class="container">
                        <div class="row">
                            <nav class="d-none d-sm-block col-sm-3 shadow" id="myScrollspy">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link active text-dark rounded-0" href="#user-avatar">Avatar</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-dark rounded-0" href="#user-information">Thông tin</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-dark rounded-0" href="#user-saved">Phim đã lưu</a>
                                    </li>
                                </ul>
                            </nav>
                            <div class="col-sm-9 col-12">
                                <div id="user-avatar" class="m-1 d-flex">    
                                    <div class="col-6"><img src="{{ $user->profile ? asset('vendor/images/'.$user->profile->avatar) : asset('vendor/images/tony.png') }}" class="img-responsive w-100 img-circle"></div>
                                    <div class="col-6 my-auto">
                                        <form>
                                            <input type="file" class="form-control-file border border-warning">
                                            <input type="submit" value="Thay ảnh đại diện" class="form-control mt-1 btn-warning">
                                        </form>
                                    </div>
                                </div>
                                <div id="user-information" class="bg-warning d-flex flex-wrap text-center p-4"> 
                                    <h1 class="col-12">Thông tin cá nhân</h1>
									<p class="col-12"><b>Họ và tên: </b> {{ $user->profile ? $user->profile->first_name.' '.$user->profile->last_name : 'Chưa có'  }}</p>
                                    <p class="col-12"><b>Email: </b> {{ $user->email }}</p>
                                    <p class="col-12"><b>Ngày sinh: </b> {{ $user->profile ? date('d/m/Y',strtotime(date($user->profile->date_of_birth))) : 'Chưa có'  }}</p>
                                    <p class="col-12"><b>Giới tính: </b> {{ $user->profile ? $user->profile->gender == 1 ? 'Nam' : 'Nữ' : 'Chưa có'  }}</p>
                                    <p class="col-12"><b>Địa chỉ: </b> {{ $user->profile ? $user->profile->address : 'Chưa có'  }}</p>
                                    <p class="col-12"><b>Số điện thoại: </b> {{ $user->profile ? $user->profile->phone : 'Chưa có'  }}</p>
                                    <button class="btn btn-danger mx-auto">Đổi mật khẩu</button>
                                </div>        
                                <div id="user-saved" class="border border-warning mt-3 p-4">         
                                    <h2>Phim đã lưu</h2>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Tên phim</th>
                                                <th>Chất lượng</th>
                                                <th>Thời lượng</th>
                                            </tr>	
                                        </thead>
                                        <tbody>
											@if ($user->storages)
												@foreach ($user->storages as $item)
													<tr>
														<td>{{ $item->title }}</td>
														<td>{{ $item->quality }}</td>
                                                        <td>{{ $item->run_time.' phút' }}</td>
													</tr>
												@endforeach
											@endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
               
            </div>
        </div>
    </div>
@endsection