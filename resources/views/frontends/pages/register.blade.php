@extends('frontends.layouts.auth')
@section('title', 'Đăng nhập - Đăng ký')

@section('content')
	<div class="register">
		<div class="container">
			<div class="d-flex justify-content-center">
				<div class="card mt-5">
					<div class="card-header">
						<h3>Đăng ký</h3>
					</div>
					<div class="card-body">
						<form>
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
								<input type="text" class="form-control" placeholder="fullname">
							</div>
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-envelope"></i></span>
								</div>
								<input type="text" class="form-control" placeholder="email">
							</div>
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
								<input type="password" class="form-control" placeholder="mật khẩu">
							</div>
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-check-square"></i></span>
								</div>
								<input type="password" class="form-control" placeholder="xác nhận mật khẩu">
							</div>
							<div class="form-group">
								<input type="submit" value="Đăng ký" class="btn float-right login_btn">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection