@extends('frontends.layouts.master')
@section('title', 'Dphim Online - Xem phim : '.$film_info->title)
@section('content')
    <div class="film-box film-infor container mt-3 mb-3">
        <div class="row">
            <ul class="breadcrumb w-100">
                <li><a href="{{ route('front-end.index') }}" class="text-uppercase font-weight-bold">Trang chủ</a></li>
                @if ($cat_film)
                    @foreach ($categories as $item)
                        @if ($item->id == $cat_film->parent_id)
                            <li><a href="{{ route('front-end.films', $item->slug) }}" class="text-uppercase font-weight-bold">{{ $item->name }}</a></li>
                        @endif
                    @endforeach
                @endif
                @foreach ($film_info->categories as $item)  
                    @if ($item->parent_id == 1)
                        <li><a href="{{ route('front-end.genres', $item->slug) }}" class="text-uppercase font-weight-bold">{{ $item->name }}</a></li>
                    @endif
                @endforeach
                <li class="is-active">{{ $film_info->title }}</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-9 p-0">
                <div class="d-flex p-0 flex-wrap main-content">
                    <div class="col-5 p-0 block-movie-info" style="background-image: url('{{ url('vendor/images/'.$film_info->avatar) }}')">
                        <div class="tools-box">
                            <div class="tools-box-bookmark normal" data-filmid="7399" style="display: block;" data-added="false"><span
                                    class="bookmark-status"></span><span class="bookmark-action"></span></div>
                        </div>
                        <div class="col-12 text-center mt-2 btn-block">
                            {{-- <button class="btn btn-success mt-1 mt-md-1 mt-lg-0">Download</button> --}}
                            @if ($film_info->trailer)
                                <button class="btn btn-primary btn-trailer" data-toggle="modal" data-target="#trailer">Trailer</button>
                            @endif
                            @if (count($film_info->episodes) > 0)
                                <a href="{{ route('front-end.watch', $film_info->slug) }}" class="btn btn-danger">Xem phim</a>  
                            @endif
                            <div class="modal fade rounded-0" id="trailer">
                                <div class="modal-dialog modal-lg rounded-0">
                                    <div class="modal-content border-dark">
                                        <div class="modal-header p-2">
                                            <h4 class="modal-title text-capitalize">Trailer {{ $film_info->title }}</h4>
                                            <button type="button" class="close hanging-close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="embed-responsive embed-responsive-16by9">
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-7">
                        @php
                            $val_1 = !empty($f_kind[0]) ? $f_kind[0] : 9999;
                            $val_2 = !empty($f_country[0]) ? $f_country[0] : 9999;
                            $val_3 = !empty($f_year[0]) ? $f_year[0] : 9999;
                        @endphp
                        <h3 class="text-uppercase mycolor w-100 mb-0 pb-0">{{ $film_info->title }}</h3>
                        <h6 class="w-100 mt-0 pt-0 text-secondary">{{ $film_info->title_eng }}</h6>
                        <div class="col-12 mt-3 border infor-scroll" id="film-scroll">
                                <p class="py-0 my-1"><b>Đạo diễn: </b><a href="{{ route('director.profile', $film_info->director->slug) }}">{{ $film_info->director->first_name.' '.$film_info->director->last_name }}</a></p>
                                <p class="py-0 my-1"><b>Quốc gia: </b>
                                    @foreach ($film_info->categories as $item)
                                        @if ($item->parent_id == $val_2)
                                            {{ $item->name }} ,
                                        @endif
                                    @endforeach
                                </p>
                                <p class="py-0 my-1"><b>Năm phát hành: </b>
                                    @foreach ($film_info->categories as $item)
                                        @if ($item->parent_id == $val_3)
                                            {{ str_replace('Phim lẻ', '', $item->name) }} ,
                                        @endif
                                    @endforeach
                                </p>
                                <p class="py-0 my-1"><b>Ngày khởi chiếu: </b>{{ date('d/m/Y',strtotime(date($film_info->release_date))) }}</p>
                                <p class="py-0 my-1"><b>Thời lượng: </b>{{ $film_info->run_time }} phút</p>
                                <p class="py-0 my-1"><b>Chất lượng: </b>{{ $film_info->quality ? 'Bản '.$film_info->quality : 'Chưa rõ' }}</p>
                                <p class="py-0 my-1"><b>Độ phân giải: </b>{{ $film_info->resolution ? $film_info->resolution : 'Chưa rõ' }}</p>
                                <p class="py-0 my-1"><b>Ngôn ngữ: </b>{{ $film_info->language ? $film_info->language : 'Chưa rõ' }}</p>
                                <p class="py-0 my-1"><b>Thể loại: </b>
                                    @foreach ($film_info->categories as $key => $item)
                                        @if ($item->parent_id == $val_1)
                                            <a href="{{ route('front-end.genres', $item->slug) }}" title="{{ $item->name }}">{{ $item->name }}</a> ,
                                        @endif
                                    @endforeach
                                </p>
                                <p class="py-0 my-1"><b>Công ty SX: </b>{{ $film_info->distributor ? $film_info->distributor : 'Chưa rõ' }}</p>
                                <p class="py-0 my-1"><b>Lượt xem: </b>{{ $film_info->viewed }}</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex mt-4 flex-wrap border p-md-2 p-sm-0 mr-3 single-content">
                    <h3 class="col-12 p-title-info pl-1">Diễn viên</h3>
                    <div class="container p-2">
                        <div class="row">
                            <div id="carousel" class="carousel slide" data-ride="carousel" data-intervel="">
                                <div class="carousel-inner">
                                    @foreach ($film_info->actors->chunk(6) as $key => $chunk)
                                        <div class="carousel-item {{ ++$key == 1  ? 'active' : '' }}">
                                            <div class="d-none d-lg-none d-xl-flex flex-wrap  float-left">
                                                @foreach ($chunk as $item)
                                                    <div class="slide-box col-xl-{{count($chunk) > 1 ? '2' : '4'}} p-0 w-100">
                                                        <a href="{{ route('actor.profile', $item->slug) }}"><img src="{{ asset('vendor/images/'.$item->avatar) }}" class="img-responsive w-100"
                                                                title="{{ $item->first_name.' '.$item->last_name}}"></a>
                                                        <a href="{{ route('actor.profile', $item->slug) }}" class="text-dark w-100 p-0">
                                                            <div class="my-2 w-100 text-center">{{ $item->first_name.' '.$item->last_name}}</div>
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex mt-4 flex-wrap border p-md-2 p-sm-0 mr-3">
                    <h3 class="col-12 p-title-info pl-1">Nội dung phim</h3>
                    <p class="px-2">{!! $film_info->content !!}</p>
                </div>
                <div class="d-flex mt-4 flex-wrap border p-md-2 p-sm-0 mr-3">
                    <h3 class="col-12 p-title-info pl-1">Từ khóa</h3>
                    <ul class="tag-list w-100">
                        @foreach ($film_info->tags as $item)
                        <li class="tag-item">
                            <a href="" class="tag-link">{{ $item->name }}</a>
                        </li>
                        @endforeach
                    </ul>
                    @if ($film_info->trailer)
                        <div class="col-12 mx-3 mt-3 w-100 justify-content-center">
                            {!! $film_info->trailer ? \Embed::make($film_info->trailer->link)->parseUrl()->setAttribute(['width' => '95%', 'height' => 400])->getHtml() : '' !!}
                        </div>
                    @endif
                </div>
            <div class="films-rela mt-3">
                <h4 class="p-title-box mr-3">Có thể bạn cũng muốn xem</h4>
                <div class="d-flex flex-wrap mr-3">
                    @foreach ($film_related as $item)
                        <a href="{{ route('front-end.filminfo', $item->slug) }}" class="col-6 col-md-4 p-1" title="{{ $item->title }}">
                            <img src="{{ asset('vendor/images/'.$item->avatar) }}" class="w-100 img-responsive img-thumbnail">
                            <h5 class="m-2">{{ $item->title }}</h5>
                        </a>
                    @endforeach
                </div>
            </div>
            </div>
            <div class="col-md-3 d-sm-none d-none d-md-flex p-0">
                @include('frontends.blockeds.box-right')
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).on('click', '.hanging-close, .modal-backdrop, .modal', function (event) {
            $(".embed-responsive").empty();
        });
        $(document).on('click', '.btn-trailer', function (event) {
            var sourceUrl = '{!! $film_info->trailer ? \Embed::make($film_info->trailer->link)->parseUrl()->setAttribute(['width' => '100%', 'height' => 380])->getHtml() : '' !!}';
            $(".embed-responsive").empty().append(sourceUrl);
        });
    </script>
@endsection