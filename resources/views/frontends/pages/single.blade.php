@extends('frontends.layouts.master')
@section('title','Dphim Online - Xem phim : '.$film_info->title)
@section('content')
<div class="film-box container mt-3 mb-3">
    <ul class="breadcrumb">
        <li><a href="{{ route('front-end.index') }}" class="text-uppercase font-weight-bold">Trang chủ</a></li>
        @if ($cat_film)
            @foreach ($categories as $item)
                @if ($item->id == $cat_film->parent_id)
                    <li><a href="{{ route('front-end.films', $item->slug) }}"
                        class="text-uppercase font-weight-bold">{{ $item->name }}</a></li>
                @endif
            @endforeach
        @endif
        @foreach ($film_info->categories as $item)
            @if ($item->parent_id == 1)
                <li><a href="{{ route('front-end.genres', $item->slug) }}"
                    class="text-uppercase font-weight-bold">{{ $item->name }}</a></li>
            @endif
        @endforeach
        <li class="text-uppercase font-weight-bold"><a
                href="{{ route('front-end.watch', $film_info->slug) }}">{{ $film_info->title }}</a></li>
        <li class="is-active text-uppercase">xem phim</li>
    </ul>
    <div class="d-flex">
        <div class="col-xl-9">
            <div class="row mb-5 border">
                <div class="col-3 movie-image mb-4">
                    <div class="movie-l-img">
                        <img alt="{{ $film_info->title }}" src="{{ asset('vendor/images/'.$film_info->avatar) }}" height="200" class="img-responsive w-100">
                        <div class="movie-watch-link-box"><a class="movie-watch-link btn-trailer" data-toggle="modal" href="#trailer"
                                title="{{ $film_info->title }}">Xem Trailer</a></div>
                    </div>
                </div>
                <div class="col-9 movie-description w-100 my-3 p-0">
                    <h4 class="text-uppercase mycolor">Xem phim {{ $film_info->title }}</h4>
                    <h6 class="w-100 mt-0 pt-0 text-secondary">{{ $film_info->title_eng }}</h6>
                    <p>{{ $film_info->description }}... [<a href="{{ route('front-end.filminfo', $film_info->slug) }}">xem thêm</a>]</p>
                </div>
                <div class="modal fade rounded-0" id="trailer">
                    <div class="modal-dialog modal-lg rounded-0">
                        <div class="modal-content border-dark">
                            <div class="modal-header p-2">
                                <h4 class="modal-title text-capitalize">Trailer {{ $film_info->title }}</h4>
                                <button type="button" class="close hanging-close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="embed-responsive embed-responsive-16by9">
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row w-100 mx-2 my-3">
                <div id="videoPlayer">
                </div>
            </div>
            <div class="row w-100 mx-2 mt-4">
                <div class="ah-le-server ah-le-fs35 w-100 p-title-info"><span>Tổng Hợp</span></div>
                <div id="box-episode">
                </div>
            </div>
            <div class="row w-100 mx-2 mt-4">
                <h3 class="col-12 p-title-info pl-1">Từ khóa</h3>
                <ul class="tag-list w-100 px-3">
                    @foreach ($film_info->tags as $item)
                    <li class="tag-item">
                        <a href="" class="tag-link">{{ $item->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="d-flex mt-4 flex-wrap mx-2 w-100">
                <h4 class="col-12 col-xl-12 p-title-box px-1">Bình luận về phim</h4>
                {{-- <div class="media p-3">
                    <img src="https://image.flaticon.com/icons/svg/194/194938.svg" alt="John Doe"
                        class="mr-3 mt-3 rounded-circle" style="width:60px;">
                    <div class="media-body">
                        <h4>Giáo viên <small><i>Posted on February 19, 2016</i></small></h4>
                        <p>Phim này hay quá - cho em 9 điểm</p>
                        <div class="media p-3">
                            <img src="https://image.flaticon.com/icons/svg/194/194924.svg" alt="Jane Doe"
                                class="mr-3 mt-3 rounded-circle" style="width:45px;">
                            <div class="media-body">
                                <h4>Chị y tá <small><i>Posted on February 20 2016</i></small></h4>
                                <p>Phim này không có cảnh đi bệnh viện à......!</p>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
        <div class="col-md-3 d-sm-none d-none d-md-flex p-0 ml-2">
            <div class="film-box film-infor">
                @include('frontends.blockeds.box-right')
            </div>
        </div>
    </div>
</div>
<div class="single-content">
    <div class="container border p-4">
        <div class="row">
            <h4 class="p-title-box mr-3 w-100">Có thể bạn cũng muốn xem</h4>
            <div id="carousel" class="carousel slide" data-ride="carousel" data-intervel="">
                <div class="carousel-inner">
                    <div class="carousel-inner">
                        @foreach ($film_related->chunk(6) as $key => $chunk)
                            <div class="carousel-item {{ ++$key == 1  ? 'active' : '' }}">
                                <div class="d-none d-lg-none d-xl-flex flex-wrap  float-left">
                                    @foreach ($chunk as $item)
                                        <div class="slide-box col-xl-{{count($chunk) > 1 ? '2' : '4'}} p-0 w-100">
                                            <a href="#" class="image_film_related"><img src="{{ asset('vendor/images/'.$item->avatar) }}" class="img-responsive" style="width: 96%;height:210px; margin-left: 2%;"
                                                    title="{{ $item->title }}"></a>
                                            <a href="#" class="text-dark w-100 p-0" title="{{ $item->title }}">
                                                <div class="my-2 w-100 text-center">{{ substr($item->title, 0, 20) }}{{ strlen($item->title) > 20 ? " ..." : "" }}</div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @if (count($film_related) > 6)
                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('vendor/js/n2jplayer.js') }}"></script>
<script>
    $(document).on('click', '.hanging-close, .modal-backdrop, .modal', function (event) {
        $(".embed-responsive").empty();
    });
    $(document).on('click', '.btn-trailer', function (event) {
        var sourceUrl = '{!! $film_info->trailer ? \Embed::make($film_info->trailer->link)->parseUrl()->setAttribute(['width' => '100%', 'height' => 380])->getHtml() : '' !!}';
        $(".embed-responsive").empty().append(sourceUrl);
    });
    $(document).ready(function() {
        $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: '../tap/{{ $film_info->slug }}/xem-phim.html',
            contentType: "application/json; charset=utf-8",
            datatype: 'JSON',
            success:function(data){
                data = JSON.stringify(data);
                var values = [], arrEp = [];         
                JSON.parse(data, function (key, value) {
                    if (typeof(value) != "object") {
                        values.push({[key]:value});
                    }
                });
                for(var i = 0; i < values.length; i++) {
                    const objEp = {};
                    objEp.name = Object.getOwnPropertyNames(values[i])[0];
                    objEp.link = Object.values(values[i])[0];
                    arrEp.push(objEp);
                }
                var setting = {
                    stepTime: 10,
                    width: "100%",
                    progressBarColor: "#1ab394",
                    autonext: false,
                    buttonBoxId: "box-episode",
                    isGetlinkDrive: false,
                    logo : "http://localhost/dphim/public/vendor/css/images/logo-dphim.png",
                    noNavButton: true,
                    source: arrEp
                }
                var player = n2jplayer('videoPlayer');
                player.setting(setting);
            },
            error:function(data){
                console.log(data);
            }
        });
        return false;
    });
</script>
@endsection