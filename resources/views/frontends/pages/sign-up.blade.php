<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng ký tài khoản</title>
	<link rel="stylesheet" href="{{ asset('vendor/css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/css/front-end.css') }}">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">ĐĂNG KÍ TÀI KHOẢN</h5>
                        <div class="form-label-group">
                            @if(session('status'))
                            <p class="alert alert-danger">
                                {!! session('status') !!}
                            </p>
                            @endif
                        </div>
                        {{ Form::open(array('url' => 'sign-up', 'method' => 'POST','files' => true,'enctype' => 'multipart/form-data')) }}
						{{ csrf_field() }}
							<div class="form-group required">
                                {{Form::label('inputUsername', 'Tên tài khoản', ['class' => 'form-label'])}}
                                {{Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Tên tài khoản','id' => 'inputUsername'])}}
                                @if ($errors->has('name'))
                                <span class="text-danger">
                                    <span>{{ $errors->first('name') }}</span>
                                </span>
                                @endif
                            </div>
                            <div class="form-group required">
                                {{Form::label('inputEmail', 'E-mail', ['class' => 'form-label'])}}
                                {{Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'E-mail','id' => 'inputEmail'])}}
                                @if ($errors->has('email'))
                                <span class="text-danger">
                                    <span>{{ $errors->first('email') }}</span>
                                </span>
                                @endif
                            </div>
                            <div class="form-group required">
                                {{Form::label('inputPassword', 'Mật khẩu', ['class' => 'form-label'])}}
                                {{Form::password('password', ['class' => 'form-control','placeholder' => 'Mật khẩu của bạn', 'id' => 'inputPassword'])}}
                                @if ($errors->has('password'))
                                <span class="text-danger">
                                    <span>{{ $errors->first('password') }}</span>
                                </span>
                                @endif
                            </div>
                            <div class="form-group required">
								{{Form::label('inputPasswordConfirm', 'Xác nhận mật khẩu', ['class' => 'form-label'])}}
								{{Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'inputPasswordConfirm'])}}
								@if ($errors->has('passwordconfirm'))
								<span class="text-danger">
									<span>{{ $errors->first('password_confirmation') }}</span>
								</span>
								@endif
							</div>
                            {{Form::submit('Đăng kí', ['class' => 'btn btn-lg btn-primary btn-block text-uppercase'])}}
							<hr class="my-4">
							<small class="w-100 text-secondary p-0">Nếu bạn đã có tài khoản hãy chọn "Đăng nhập" hoặc về <a href="{{ URL::to('/') }}">Trang chủ</a></small>
                            <a href="{{ route('front-end.login') }}" class="btn btn-lg btn-success btn-block text-uppercase mt-2">Đăng nhập</a>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
