@extends('frontends.master')
@section('title','Dphim - Contact us if you want...')
@section('content')
	<div class="contact">
			<div class="container">
				<div class="d-flex">
					<div class="col-xl-8 border p-3 mt-3">
						<div class="d-flex flex-wrap">
							<h3 class="col-12">About Us</h3>
							<p class="flex-wrap">According to The Los Santos Crimson, Facemash used "photos compiled from the online facebooks of nine Houses, placing two next to each other at a time and asking users to choose the "hotter" person".[9] Facemash attracted 450 visitors and 22,000 photo-views in its first four hours online.[11]

The site was quickly forwarded to several campus group list-servers, but was shut down a few days later by the Harvard administration. Zuckerberg faced expulsion and was charged by the administration with breach of security, violating copyrights, and violating individual privacy. Ultimately, the charges were dropped.[9] Zuckerberg expanded on this initial project that semester by creating a social study tool ahead of an art history final exam. He uploaded all art images to a website, each of which was featured with a corresponding comments section, then shared the site with his classmates, and people started sharing notes.[12]</p>
						</div>
						<div class="mail d-flex mt-3 flex-wrap">
							<h3 class="col-12">You can contact us with below:</h3>
							<h5 class="col-12">Phone number:<b> 0909-23x-xxx</b></h5>
							<h5 class="col-12">Email:<b> admin@dphim.com</b></h5>
							<h5 class="col-12">Phone advertise:<b> 0909-23x-xxx</b></h5>
							<h5 class="col-12">Email for advertise:<b> advertise@dphim.com</b></h5>
						</div>
					</div>
					<div class="text-dark col-xl-4 d-none d-sm-none d-xl-block p-3">
						<img src="https://images.examples.com/wp-content/uploads/2017/03/Vintage-Music-Advertisement-Poster.jpg" class="p-2 img-responsive w-100">
						<img src="https://cdn.dribbble.com/users/79547/screenshots/2168896/hamburger_template_drbl.jpg" class="p-2 img-responsive w-100">
						<img src="https://i.pinimg.com/originals/5d/0a/fd/5d0afdbe3773decc5c6bc23d49d6fcb8.jpg" class="p-2 img-responsive w-100">
					</div>
				</div>
			</div>
		</div>
@endsection