@extends('frontends.layouts.master')
@section('title',(is_numeric($category_genres->name) || $category_genres->name == 'trước 2013') ? 'Phim lẻ '.$category_genres->name : (($category_genres->parent_id == 2) ? 'Phim '.$category_genres->name : $category_genres->name))
@section('content')
	<div class="container featured-movies mt-3 p-4">
		<ul class="breadcrumb">
			<li><a href="{{ route('front-end.index') }}" class="text-uppercase font-weight-bold">Trang chủ</a></li>
			@foreach ($categories as $item)
				@if ($category_genres->parent_id != null && $category_genres->parent_id != '1' && $category_genres->parent_id != '2' && $category_genres->parent_id == $item->id)
					<li><a href="{{ route('front-end.films', $item->slug) }}" class="text-uppercase font-weight-bold">{{ $item->name }}</a></li>
				@endif
			@endforeach
            <li class="is-active text-uppercase font-weight-bold">{{ (is_numeric($category_genres->name) || $category_genres->name == 'trước 2013') ? 'Phim lẻ '.$category_genres->name : (($category_genres->parent_id == 2) ? 'Phim '.$category_genres->name : $category_genres->name) }}</li>
        </ul>
		<div class="container p-0">
			{!! Form::open(array('route' => array('front-end.filmsodd',$category_genres->slug), 'method' => 'get'))!!}	
			{{-- {!! csrf_field() !!}	 --}}
				<div class="form-group row mx-1">
					<div class="col-md-2 col-sm-4 col-xs-4">
						{!! Form::label('filter_sort', 'Sắp xếp', ['class'=>'col-form-label font-weight-bold']) !!}
						{!! Form::select('filter_sort',['posttime' => 'Thời gian cập nhật', 'viewed' => 'Lượt xem'] , '', ['class' => 'form-control rounded-0', 'placeholder' => 'Sắp xếp theo']) !!}
					</div>
					<div class="col-md-2 col-sm-4 col-xs-4">
						{!! Form::label('filter_language', 'Ngôn ngữ', ['class'=>'col-form-label font-weight-bold']) !!}
						{!! Form::select('filter_language',['vi' => 'Tiếng việt', 'illustrate' => 'Thuyết minh', 'dubbing' => 'Lồng tiếng', 'subtitle' => 'Vietsub'] ,'', ['class' => 'form-control rounded-0', 'placeholder' => 'Tất cả']) !!}
					</div>
					<div class="col-md-2 col-sm-4col-xs-4">
						{!! Form::label('filter_genres', 'Thể loại', ['class'=>'col-form-label font-weight-bold']) !!}
						{!! Form::select('filter_genres',$genres ,'', ['class' => 'form-control rounded-0', 'placeholder' => 'Tất cả']) !!}
					</div>
					<div class="col-md-2 col-sm-4 col-xs-4">
						{!! Form::label('filter_country', 'Quốc gia', ['class'=>'col-form-label font-weight-bold']) !!}
						{!! Form::select('filter_country', $country ,'', ['class' => 'form-control rounded-0', 'placeholder' => 'Tất cả']) !!}
					</div>
					<div class="col-md-2 col-sm-4 col-xs-4">
						{!! Form::label('filter_year', 'Năm phát hành', ['class'=>'col-form-label font-weight-bold']) !!}
						{!! Form::select('filter_year', $year ,'', ['class' => 'form-control rounded-0', 'placeholder' => 'Tất cả']) !!}
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2 h-100 pt-4">
						{!! Form::submit('Duyệt phim', ['class' => 'btn btn-danger text-white mt-3 rounded-0']) !!}
					</div>
				</div>
			{!! Form::close() !!}
			<div class="row mx-0 w-100 ml-2">
				@foreach ($films_genres as $item)
					<div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 mx-0 pl-0">
						<div class="box-overlay">
							<img class="img-responsive w-100" src="{{ asset('vendor/images/'.$item->avatar) }}" style="max-height: 220px;">
							<a href="{{ route('front-end.filminfo', $item->slug) }}" class="overlay" title="{{ $item->title }}">
								<p class="play-icon font-weight-bold text-uppercase-icon"><i class="fas fa-play-circle fa-3x"></i></p>
							</a>
							<span class="ribbon">{{ $item->quality.' - '.$item->language }}{{ count($item->episodes) > 1 ? ' | '.$item->episodes->last()->episode : '' }}</span>
						</div>
						<div class="box-title mt-2 mb-2 pl-2 pr-2">
							<a href="{{ route('front-end.filminfo', $item->slug) }}" class="text-info text-uppercase">
								<h6 class="mb-0 title_films" title="{{ $item->title }}">{{ substr($item->title, 0, 22) }}{{ strlen($item->title) > 22 ? " ..." : "" }}</h6>
							</a>
							<small class="w-100 text-secondary">{{ $item->run_time }} phút{{ count($item->episodes) > 1 ? '/tập' : '' }}</small>
						</div>
					</div>
				@endforeach
			</div>
			<div class="row mt-5 m-0 d-md-flex">
				<div class="col-md-12">
					<div class="float-right">
						{!! $films_genres->links() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection