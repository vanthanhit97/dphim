@extends('frontends.master')
@section('title','Dphim - Contact us if you want...')
@section('content')
	<div class="user-change-password mt-3">
		<div class="container d-flex flex-wrap">
			<form class="w-100">
				<label class="form-inline col-12">
					<b class="col-5 col-md-4 col-lg-3">Mật khẩu hiện tại:</b> 
					<input type="password" name="" class="form-control ml-2 col-6">
				</label>
				<label class="form-inline col-12">
					<b class="col-5 col-md-4 col-lg-3 ">Mật khẩu mới:</b> 
					<input type="password" name="" class="form-control ml-2 col-6">
				</label>
				<label class="form-inline col-12">
					<b class="col-5 col-md-4 col-lg-3">Xác nhận mật khẩu:</b> 
					<input type="password" name="" class="form-control ml-2 col-6">
				</label>
				<input type="submit" name="" value="Đồng ý" class="form-control col-4 col-md-3 col-lg-2 mx-auto mt-4 btn-warning">
			</form>
		</div>
	</div>
@endsection