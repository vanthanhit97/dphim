<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng nhập tài khoản người dùng</title>
    <link rel="stylesheet" href="{{ asset('vendor/css/app.css') }}">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">ĐĂNG NHẬP TÀI KHOẢN NGƯỜI DÙNG</h5>
                        <div class="form-label-group">
                            @if(session('status'))
                            <p class="alert alert-danger">
                                {!! session('status') !!}
                            </p>
                            @endif
                        </div>
                        {{ Form::open(array('url' => '/login', 'method' => 'post', 'class' => 'horizontal')) }}
                        {{ csrf_field() }}
                            <div class="form-label-group">
                                {{Form::label('inputEmail', 'Tên tài khoản hoặc E-mail', ['class' => 'form-label'])}}
                                {{Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Tên tài khoản hoặc E-mail đăng nhập','id' => 'inputEmail'])}}
                                @if ($errors->has('email'))
                                <span class="text-danger">
                                    <span>{{ $errors->first('email') }}</span>
                                </span>
                                @endif
                            </div>
                            <div class="form-label-group mt-2">
                                {{Form::label('inputPassword', 'Mật khẩu', ['class' => 'form-label'])}}
                                {{Form::password('password', ['class' => 'form-control', 'placeholder' => 'Mật khẩu', 'id' => 'inputPassword'])}}
                                @if ($errors->has('password'))
                                <span class="text-danger">
                                    <span>{{ $errors->first('password') }}</span>
                                </span>
                                @endif
                            </div>
                            <div class="custom-control custom-checkbox mb-3 mt-2">
                                {{Form::checkbox('remember', '0', false, ['class' => 'custom-control-input', 'id' => 'remenberCheck'])}}
                                {{Form::label('remenberCheck', 'Nhớ mật khẩu', ['class' => 'custom-control-label'])}}
                                <a href="#" class="d-flex justify-content-end">Quên mật khẩu?</a>
                            </div>
                            {{Form::submit('Đăng nhập', ['class' => 'btn btn-lg btn-primary btn-block text-uppercase'])}}
                            <hr class="my-4">
                            <small class="w-100 text-secondary p-0">Nếu bạn chưa có tài khoản hãy chọn "Đăng ký" hoặc về <a href="{{ URL::to('/') }}">Trang chủ</a></small>
                            <a href="{{ route('front-end.sign-up') }}" class="btn btn-lg btn-success btn-block text-uppercase mt-2">ĐĂNG KÝ</a>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>