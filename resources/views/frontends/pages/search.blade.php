@extends('frontends.layouts.master')
@section('title',$keyword.' | Phim '.$keyword.' hay')
@section('content')
	<div class="container featured-movies mt-3 p-4">
		<ul class="breadcrumb">
			<li><a href="{{ route('front-end.index') }}" class="text-uppercase font-weight-bold">Trang chủ</a></li>
            <li class="is-active text-uppercase font-weight-bold">Tìm kiếm</li>
        </ul>
		<div class="container p-0">
			<div class="col-12 mx-0 px-0 my-4">
				<h4 class="p-title-box mr-3">Tìm kiếm : {{ $keyword }} <small class="text-dark">({{ $films->total().' kết quả' }})</small></h4>
			</div>
			<div class="row mx-0 w-100 ml-2">
				@foreach ($films as $item)
					<div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 mx-0 pl-0">
						<div class="box-overlay">
							<img class="img-responsive w-100" src="{{ asset('vendor/images/'.$item->avatar) }}">
							<a href="{{ route('front-end.filminfo', $item->slug) }}" class="overlay" title="{{ $item->title }}">
								<p class="play-icon font-weight-bold text-uppercase-icon"><i class="fas fa-play-circle fa-3x"></i></p>
							</a>
						</div>
						<div class="box-title mt-2 mb-2 pl-2 pr-2">
							<a href="{{ route('front-end.filminfo', $item->slug) }}" class="text-info text-uppercase">
								<h6 class="mb-0 title_films" title="{{ $item->title }}">{{ substr($item->title, 0, 13) }}{{ strlen($item->title) > 13 ? " ..." : "" }}</h6>
							</a>
							<small class="w-100 text-secondary">{{ $item->run_time }} phút{{ count($item->episodes) > 1 ? '/tập' : '' }}</small>
						</div>
					</div>
				@endforeach
			</div>
			<div class="row mt-5 m-0 d-md-flex">
				<div class="col-md-12">
					<div class="float-left">
						{!! $films->links("pagination::simple-bootstrap-4") !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection