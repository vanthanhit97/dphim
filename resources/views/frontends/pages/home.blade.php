@extends('frontends.layouts.master')
@section('title','Dphim - Xem phim miễn phí tốc độ cao')
@section('content')
    @include('frontends.blockeds.slide')
    <div class="film-infor container w-100">
        <div class="form-group row w-100 m-0 p-0">
            <div class="col-md-9 col-sm-12 w-100 m-0 p-0">         
                <div class="container-fluid featured-movies mt-3 p-5">
                    <h2 class="container w-100 p-0 active text-uppercase mb-3">Phim lẻ mới cập nhật</h2>
                    <ul class="container nav nav-pills d-sm-flex d-flex p-0" role="tablist">
                        <li class="nav-item col-sm-3 col-4 p-0 col-xl-2">
                            <a class="nav-link btn-light btn rounded-0 active text-uppercase" data-toggle="pill" href="#list-odd-new">Mới nhất</a>
                        </li>
                        <li class="nav-item col-sm-3 col-4 p-0 col-xl-3">
                            <a class="nav-link btn-light btn rounded-0 text-uppercase" data-toggle="pill" href="#list-odd-viewed">Xem nhiều</a>
                        </li>
                    </ul>
                    <div class="tab-content mt-3">
                        <div id="list-odd-new" class="container tab-pane active shadow"><br>
                            <div class="d-xl-flex flex-xl-wrap row mr-0 ml-0">
                                @foreach ($film_odd as $item)
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 box p-1">
                                        <div class="box-overlay">
                                            <img class="img-responsive w-100" src="{{ asset('vendor/images/'.$item->avatar) }}">
                                            <a href="{{ route('front-end.filminfo', $item->slug) }}" class="overlay" title="{{ $item->title }}">
                                                <p class="play-icon font-weight-bold text-uppercase-icon"><i class="fas fa-play-circle fa-3x"></i></p>
                                            </a>
                                            <span class="ribbon">{{ $item->quality.'-'.$item->language }}</span>
                                        </div>
                                        <div class="box-title mt-2 mb-2 pl-2 pr-2">
                                            <a href="{{ route('front-end.filminfo', $item->slug) }}" class="text-dark text-uppercase"><h6 class="mb-0" title="{{ $item->title }}">{{ substr($item->title, 0, 13) }}{{ strlen($item->title) > 13 ? " ..." : "" }}</h6></a>
                                            <small class="w-100 text-secondary">{{ $item->run_time }} phút</small>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div id="list-odd-viewed" class="container tab-pane shadow"><br>
                            <div class="d-xl-flex flex-xl-wrap row mr-0 ml-0">
                                @foreach ($film_old_mostview as $item)
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 box p-1">
                                        <div class="box-overlay">
                                            <img class="img-responsive w-100" src="{{ asset('vendor/images/'.$item->avatar) }}"
                                            alt="">
                                            <a href="{{ route('front-end.filminfo', $item->slug) }}" class="overlay" title="{{ $item->title }}">
                                                <p class="play-icon font-weight-bold text-uppercase-icon"><i class="fas fa-play-circle fa-3x"></i></p>
                                            </a>
                                            <span class="ribbon">{{ $item->quality.'-'.$item->language }}</span>
                                        </div>
                                        <div class="box-title mt-2 mb-2 pl-2 pr-2">
                                            <a href="{{ route('front-end.filminfo', $item->slug) }}" class="text-dark font-weight-bold text-uppercase"><h6 class="mb-0" title="{{ $item->title }}">{{ substr($item->title, 0, 13) }}{{ strlen($item->title) > 13 ? " ..." : "" }}</h6></a>
                                            <small class="w-100 text-secondary">{{ $item->run_time }} phút</small>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid featured-movies px-5">
                    <h2 class="container w-100 p-0 active text-uppercase mb-3">Phim bộ mới cập nhật</h2>
                    <ul class="container nav nav-pills d-sm-flex d-flex p-0" role="tablist">
                        <li class="nav-item col-sm-3 col-4 p-0 col-xl-2">
                            <a class="nav-link btn-light btn rounded-0 active text-uppercase" data-toggle="pill" href="#film-series">Mới nhất</a>
                        </li>
                        <li class="nav-item col-sm-3 col-4 p-0 col-xl-2">
                            <a class="nav-link btn-light btn rounded-0 text-uppercase" data-toggle="pill" href="#anime-series">Anime</a>
                        </li>
                    </ul>
                    <div class="tab-content mt-3">
                        <div id="film-series" class="container tab-pane active shadow"><br>
                            <div class="d-xl-flex flex-xl-wrap row mr-0 ml-0">
                                @foreach ($film_series as $item)
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 box p-1">
                                        <div class="box-overlay">
                                            <img class="img-responsive w-100" src="{{ asset('vendor/images/'.$item->avatar) }}"
                                            alt="">
                                            <a href="{{ route('front-end.filminfo', $item->slug) }}" class="overlay" title="{{ $item->title }}">
                                                <p class="play-icon font-weight-bold text-uppercase-icon"><i class="fas fa-play-circle fa-3x"></i></p>
                                            </a>
                                            @if (count($item->episodes) >= 1)
                                                <span class="ribbon">{{ $item->episodes->last()->episode }}</span>
                                            @endif
                                        </div>
                                        <div class="box-title mt-2 mb-2 pl-2 pr-2">
                                            <a href="{{ route('front-end.filminfo', $item->slug) }}" class="text-dark font-weight-bold text-uppercase"><h6 class="mb-0" title="{{ $item->title }}">{{ substr($item->title, 0, 13) }}{{ strlen($item->title) > 13 ? " ..." : "" }}</h6></a>
                                            <small class="w-100 text-secondary">{{ $item->run_time }} phút/tập</small>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div id="anime-series" class="container tab-pane shadow"><br>
                            <div class="d-xl-flex flex-xl-wrap row mr-0 ml-0">
                                @foreach ($anime_series as $item)
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 box p-1">
                                        <div class="box-overlay">
                                            <img class="img-responsive w-100" src="{{ asset('vendor/images/'.$item->avatar) }}"
                                            alt="">
                                            <a href="{{ route('front-end.filminfo', $item->slug) }}" class="overlay" title="{{ $item->title }}">
                                                <p class="play-icon font-weight-bold text-uppercase-icon"><i class="fas fa-play-circle fa-3x"></i></p>
                                            </a>
                                            <span class="ribbon">{{ $item->quality.' - '.$item->language }}{{ count($item->episodes) > 1 ? ' | '.$item->episodes->last()->episode : '' }}</span>
                                        </div>
                                        <div class="box-title mt-2 mb-2 pl-2 pr-2">
                                            <a href="{{ route('front-end.filminfo', $item->slug) }}" class="text-dark font-weight-bold text-uppercase"><h6 class="mb-0" title="{{ $item->title }}">{{ substr($item->title, 0, 13) }}{{ strlen($item->title) > 13 ? " ..." : "" }}</h6></a>
                                            <small class="w-100 text-secondary">{{ $item->run_time }} phút/tập</small>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-sm-none d-none d-md-flex p-0 mt-5 pt-5">
                @include('frontends.blockeds.box-right')
            </div>
        </div>
    </div>
@endsection