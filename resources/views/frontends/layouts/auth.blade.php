<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
	<link rel="stylesheet" href="{{ asset('vendor/css/bootstrap.min.css') }}">
	<link href="{{ asset('vendor/css/fontawesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendor/css/front-end.css') }}">
	@yield('css')
</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="1">
		<div class="">
			@include('frontends.blockeds.auth-nav')
		</div>
			@yield('content')
	<script src="{{ asset('vendor/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('vendor/js/popper.min.js') }}"></script>
	<script src="{{ asset('vendor/js/bootstrap.min.js') }}"></script>
</body>
</html>