<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
	<link rel="stylesheet" href="{{ asset('vendor/css/bootstrap.min.css') }}">
	<link href="{{ asset('vendor/css/fontawesome.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('vendor/css/front-end.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/css/film-infor.css') }}">
	@yield('css')
</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="1">
		<div class="">
			@include('frontends.blockeds.navbar')
		</div>
		<div class="">
			@yield('content')
		</div>
		<div class="mt-5">
			@include('frontends.blockeds.footer')
		</div>
		<div class="scroll-top-wrapper ">
			<span class="scroll-top-inner">
				<i class="fa fa-2x fa-arrow-circle-up"></i>
			</span>
		</div>
	<script src="{{ asset('vendor/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('vendor/js/popper.min.js') }}"></script>
	<script src="{{ asset('vendor/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('vendor/js/frontend.js') }}"></script>
	@yield('javascript')
	@if ($errors->has('password_old') || $errors->has('password_new') || session('checkPassword'))
		<script>
			$(document).ready(function () {
				$('#changepassword').modal('show');
			});
		</script>
	@endif
</body>
</html>