<div class="slideshow d-flex flex-wrap text-light p-0 justify-content-center">
	<div class="col-12 col-md-12 col-xl-12 col-lg-12 m-0 p-0">
		<div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel" data-pause=false>
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
				<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				@foreach ($film_slide as $key => $slide)
					<div class="carousel-item {{ $key == 0 ? 'active' : '' }}" data-interval="5000">
						<a href="{{ route('front-end.filminfo', $slide->slug) }}"><img id="img{{++$key}}" src="{{ asset('vendor/images/'.$slide->cover_image) }}"
							class="d-block w-100 animated fadeIn fast"></a>
						<div class="carousel-caption text-left d-block" style="margin-bottom: 16%;">
							<a href="{{ route('front-end.filminfo', $slide->slug) }}"><h5 class="text-uppercase item3">{{ $slide->title }}</h5></a>
							<p class="item3">{{ $slide->description }}</p>
						</div>
					</div>
				@endforeach
			</div>
			<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</div>