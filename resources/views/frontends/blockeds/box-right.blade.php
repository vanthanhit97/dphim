<div class="side-content p-0">
    <h4 class="text-uppercase mycolor">Trailer Phim Mới</h4>
    <div class="trailer side-scroll border" id="film-scroll">
        @foreach ($trailers as $key => $item)
            <a href="{{ route('front-end.filminfo', $item->film->slug) }}" class="d-flex p-0 m-1" title="{{ $item->film->title }}">
                <div class="col-4 p-0">
                    <img src="{{ asset('vendor/images/'.$item->film->avatar) }}" class="w-100 img-responsive img-thumbnail p-0">
                </div>
                <div class="col-8 w-100 m-0 px-2">
                    <h5 class="w-100 my-0"><small>{{ $item->film->title }}</small></h5>
                    <p class="w-100 text-secondary"><small>Lượt xem : {{ $item->film->viewed }}</small></p>
                </div>
            </a>
            @if (++$key < count($trailers))
                <hr class="my-2 ml-1">
            @endif
        @endforeach
    </div>
    <h4 class="text-uppercase mt-4 mycolor w-100">Phim mới cập nhật</h4>
    <div class="list-film-news side-scroll border" id="film-scroll">
        @foreach ($films_news as $key => $item)
            <a href="{{ route('front-end.filminfo', $item->slug) }}" class="d-flex p-0 m-1" title="{{ $item->title }}">
                <div class="col-4 p-0">
                    <img src="{{ asset('vendor/images/'.$item->avatar) }}" class="w-100 img-responsive img-thumbnail p-0">
                </div>
                <div class="col-8 w-100 m-0 px-2">
                    <h5 class="w-100 my-0"><small>{{ $item->title }}</small></h5>
                    <p class="w-100 text-secondary"><small>Lượt xem : {{ $item->viewed }}</small></p>
                </div>
            </a>
            @if (++$key < count($films_news))
                <hr class="my-2 ml-1">
            @endif
        @endforeach
    </div>
    <h4 class="text-uppercase mt-3 mycolor"><i class="fas fa-tag"></i> Từ khóa nổi bật</h4>
    <div class="tag-box pl-2 border">
        <div class="td-post-tag mt-3 mb-3">
            @foreach ($tags_hl as $item)
                <a href="" class="tag">{{ $item->name }}</a>
            @endforeach
        </div>
    </div>
</div>