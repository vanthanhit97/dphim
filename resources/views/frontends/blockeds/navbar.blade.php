<div class="border border-warning"></div>
<div class="header-content p-lg-4">
	<div class="container d-flex d-sm-flex d-lg-flex d-xl-flex flex-wrap text-center">
		<div class="mx-auto col-3 col-md-1 col-lg-1 col-xl-2 p-0 m-0 p-md-0 p-lg-0 flex-fill">
			<img src="{{ asset('vendor/css/images/logo-dphim.png') }}" alt="logo" class="img-responsive w-100">
		</div>
		<div class="mx-auto col-12 m-1 col-sm-9 col-md-8 col-lg-8 col-xl-6 p-0 m-0 my-lg-auto my-md-auto">
			<form action="{{ route('search') }}" method="POST" role="search">
				{{ csrf_field() }}
				<div class="input-group col-sm-12 col-md-12 col-xl-12 mx-auto mt-2 mt-lg-0 border-0">
					<input type="text" name="keyword" value="{{ request()->keyword }}" class="form-control border-0 btn btn-secondary" placeholder="Tìm kiếm phim">
					<div class="input-group-append p-0 col-4 col-lg-2 col-xl-3">
						<button class="btn btn-warning col-12 border-0 text-white" type="submit">Tìm</button> 
					</div>
				</div>
			</form>
		</div>
		<div class="my-xl-auto my-lg-auto nav-link col-12 col-sm-12 col-md-12 col-xl-3 p-0 text-center">
			<div class="my-lg-auto">
                @if (Auth::guard('user')->check())
					<ul class="nav navbar-top-links navbar-right">
						<li class="dropdown">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<span class="m-r-sm text-muted welcome-message">Xin chào, <span class="text-info font-weight-bold">{{ Auth::guard('user')->user()->name }} <b class="caret"></b></span>
							</a>
							<ul class="dropdown-menu animated fadeInRight m-t-xs">
							<li><a class="dropdown-item" href="{{ route('front-end.profile') }}">Thông tin cá nhân</a></li>
								<li class="dropdown-divider"></li>
								<li><a class="dropdown-item" href="#" data-toggle="modal" data-target="#changepassword">Thay đổi mật khẩu</a></li>
								<li class="dropdown-divider"></li>
								<li><a class="dropdown-item" href="{{ route('front-end.logout') }}">Đăng xuất</a></li>
							</ul>
						</li>
					</ul>
					@include('frontends.blockeds.changepassword')
                @else
					<a href="{{ route('front-end.sign-up') }}" class="btn btn-success rounded-0">Đăng ký</a>
					<a href="{{ route('front-end.login') }}" class="btn btn-success rounded-0">Đăng nhập</a>
                @endif
			</div>
		</div>
	</div>
</div>
@if (session('status_s') )
	<div class="positon-info col-md-3">
		<ul>
			<li id="notification-alert" class="alert alert-success alert-dismissible fade show" role="alert">
				<a class="nav-item font-weight-bold">{{ session('status_s') }}</a>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</li>
		</ul>
	</div>
@endif
<div class="nav-content">
	<nav class="navbar navbar-dark navbar-expand-sm navbar-expand-md bg-warning p-0">
		<button type="" class="navbar-toggler bg-warning col-sm-12" data-toggle="collapse" data-target="#nav-collapse">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-center" id="nav-collapse">
			<ul class="navbar-nav d-flex container justify-content-center text-center p-0">
				<li class="border"></li>
				<li class="nav-item col-12 col-xl-1 col-md-auto border-0 p-0">
					<a href="{{ route('front-end.index') }}" class="btn btn-warning rounded-0 col-12 nav-link text-uppercase">Phim mới</a>
				</li>
				@foreach ($categories as $key => $category)
					@switch($key + 1)
						@case(1)
							<li class="border"></li>
							<li class="nav-item col-12 col-xl-1 col-md-auto border-0 p-0">
								<div class="dropdown">
									<a href="#" class="btn btn-warning rounded-0 col-12 nav-link text-uppercase dropdown-toggle" data-toggle="dropdown">{{ $category->name }}</a>
									@if (count($category->childs))
										<div class="dropdown-menu dropdown-menu1 rounded-0">
											<ul class="d-flex flex-wrap list-unstyled">
												@foreach ($category->childs as $child)
													<li class="dropdown-item col-6 col-md-4">
														<a class="text-dark" href="{{ route('front-end.genres', $child->slug) }}">{{ $child->name }}</a>
													</li>
												@endforeach
											</ul>
										</div>
									@endif
								</div>
							</li>
							@break
						@case(2)
							<li class="border"></li>
							<li class="nav-item col-12 col-xl-1 col-md-auto border-0 p-0">
								<div class="dropdown">
									<a href="#" class="btn btn-warning rounded-0 col-12 nav-link text-uppercase dropdown-toggle" data-toggle="dropdown">{{ $category->name }}</a>
									@if (count($category->childs))
										<div class="dropdown-menu rounded-0">
											<ul class="d-flex flex-wrap list-unstyled">
												@foreach ($category->childs as $child)
													<li class="dropdown-item">
														<a class="text-dark" href="{{ route('front-end.countries', $child->slug) }}">{{ $child->name }}</a>
													</li>
												@endforeach
											</ul>
										</div>
									@endif
								</div>
							</li>
							@break
						@case(3)
							<li class="border"></li>
							<li class="nav-item col-12 col-xl-1 col-md-auto p-0 border-0">
								<div class="dropdown">
									<a href="{{ route('front-end.films', $category->slug) }}" class="btn btn-warning rounded-0 col-12 nav-link text-uppercase dropdown-toggle">{{ $category->name }}</a>
									@if (count($category->childs))
										<div class="dropdown-menu rounded-0">
											<ul class="d-flex flex-wrap list-unstyled">
												@foreach ($category->childs as $child)
													<li class="dropdown-item">
														<a class="text-dark" href="{{ route('front-end.filmsodd', $child->slug) }}">Phim lẻ {{ $child->name }}</a>
													</li>
												@endforeach
											</ul>
										</div>
									@endif
								</div>
							</li>
							@break
						@case(4)
							<li class="border"></li>
							<li class="nav-item col-12 col-xl-1 col-md-auto p-0 border-0">
								<div class="dropdown">
									<a href="{{ route('front-end.films', $category->slug) }}" class="btn btn-warning rounded-0 col-12 nav-link text-uppercase dropdown-toggle">{{ $category->name }}</a>
									@if (count($category->childs))
										<div class="dropdown-menu rounded-0">
											<ul class="d-flex flex-wrap list-unstyled">
												@foreach ($category->childs as $child)
													<li class="dropdown-item">
														<a class="text-dark" href="{{ route('front-end.filmsseries', $child->slug) }}">{{ $child->name }}</a>
													</li>
												@endforeach
											</ul>
										</div>
									@endif
								</div>
							</li>
							@break
						@case(5)
							<li class="border"></li>
							<li class="nav-item col-12 col-md-auto p-0 border-0">
								<div class="dropdown">
									<a href="{{ route('front-end.films', $category->slug) }}" class="btn btn-warning rounded-0 col-12 nav-link text-uppercase">{{ $category->name }}</a>
								</div>
							</li>
							@break
						@case(6)
							<li class="border"></li>
							<li class="nav-item col-12 col-md-auto p-0 border-0">
								<div class="dropdown">
									<a href="{{ route('front-end.films', $category->slug) }}" class="col-12 btn btn-warning rounded-0 nav-link text-uppercase">{{ $category->name }}</a>
								</div>
							</li>
							@break
						@default
							@break
					@endswitch
				@endforeach
				<li class="border"></li>
			</ul>
		</div>
	</nav>
</div>