<div class="footer">
	<div class="jumbotron-fluid bg-dark text-white p-3">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-lg-6 col-xl-6">
					<h2 class="row mycolor text-uppercase rounded-0">Theo dõi fanpage</h2>
					<div class="col-12 p-1">
						<iframe
							src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FDphim-Online-2456203317725206%2F&tabs&width=340&height=180&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=210462983091453"
							width="100%" height="180" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"
							allow="encrypted-media"></iframe>
					</div>
				</div>
			</div>
			<div class="border border-secondary m-5"></div>
			<div class="row">
				<div class="col-12 col-sm-12 col-lg-12 col-xl-12 text-center">
					<p>© {{ $globalSetting->company_copyright }}</p>
				</div>
			</div>
		</div>
	</div>
</div>
