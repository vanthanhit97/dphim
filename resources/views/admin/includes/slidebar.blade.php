<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <h2><span class="block m-t-xs font-bold text-uppercase"><i class="fa fa-film" aria-hidden="true"></i> Dphim</span></h2>
                    </a>
                </div>
                <div class="logo-element">
                    <i class="fa fa-film" style='font-size:28px' aria-hidden="true"></i>
                </div>
            </li>
            <li>
                <a href="{{ route('dashboard.index') }}"><i class="fa fa-home"></i> <span class="nav-label">Dashboards</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-th-list"></i> <span class="nav-label">Quản lý danh mục</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('categories.index') }}">Tạo danh mục chính</a></li>
                    <li><a href="{{ route('categories.childs') }}">Tạo danh mục phụ</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fas fa-tag"></i> <span class="nav-label">Quản lý tags</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('tags.create') }}">Tạo tags mới</a></li>
                    <li><a href="{{ route('tags.index') }}">Danh sách tags</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="far fa-id-card"></i> <span class="nav-label">Diễn viên / Đạo diễn</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('actors.create') }}">Thêm mới</a></li>
                    <li><a href="{{ route('actors.index') }}">Danh sách</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-ad"></i> <span class="nav-label">Quản lý quảng cáo</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('advertisements.index') }}">Tạo quảng cáo</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('trailers.index') }}"><i class="fa fa-video"></i> <span class="nav-label">Quản lý trailer</span></a>
            </li>
            <li>
                <a href="{{ route('episodes.index') }}"><i class="fab fa-youtube"></i> <span class="nav-label">Quản lý tập phim</span></a>
            </li>
            <li>
                <a href="#"><i class="fas fa-film"></i> <span class="nav-label">Quản lý phim</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('films.create') }}">Thêm phim mới</a></li>
                    <li><a href="{{ route('films.index') }}">Danh sách phim</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('users.index') }}"><i class="fa fa-user"></i> <span class="nav-label">Quản lý user</span></a>
            </li>
            <li>
                <a href="{{ route('comments.index') }}"><i class="fa fa-comment"></i> <span class="nav-label">Quản lý bình luận</span></a>
            </li>
            <li>
                <a href="#"><i class="far fa-images"></i> <span class="nav-label">Quản lý thư viện ảnh</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('mediashow') }}">Tất cả ảnh</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('global-setting.index') }}"><i class="fas fa-blog"></i> <span class="nav-label">Quản lý thông tin website</span></a>
            </li>
        </ul>
    </div>
</nav>
