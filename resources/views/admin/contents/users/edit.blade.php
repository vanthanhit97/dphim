@extends('admin.layouts.master') 
@section('title','Quản lý Profile') 
@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2 class="text-uppercase font-weight-bold">Quản lý Profile</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Profiles</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <div class="form-group row bg-white pb-4">
        <div class="col-md-12 position-alert">
            @include('admin.includes.alert')
        </div>
        <div class="col-md-12">
            {!! Form::model($users,array('route' => array('users.update',$users->slug),'method' => 'PUT','files' => true,'enctype' => 'multipart/form-data')) !!}
            {!! csrf_field() !!}
        <div class="card-body">
            <div class="container">
                <div class="form-group required">
        <div class="form-group">
        {!! Form::label('name', 'Tên người dùng :', ['class' => 'font-weight-bold']) !!} 
        {!! Form::text('name', $users->name, ['class' => $errors->has('name') ? 'form-control error' : 'form-control', 'placeholder' => 'Họ người dùng']) !!}
        <span class="text-danger">{{ $errors->first('name') }}</span>
        </div>
        <div class="form-group">
        {!! Form::label('email', 'Email :', ['class' => 'font-weight-bold']) !!} 
        {!! Form::text('email', $users->email, ['class' => $errors->has('email') ? 'form-control error' : 'form-control', 'placeholder' => 'Họ người dùng']) !!}
        <span class="text-danger">{{ $errors->first('email') }}</span>
        </div>
        <div class="form-group">
            {!! Form::label('first_name', 'Họ :', ['class' => 'font-weight-bold']) !!} 
            {!! Form::text('first_name', $users->profile ? $users->profile->first_name : '', ['class' => $errors->has('first_name') ? 'form-control error' : 'form-control', 'placeholder' => 'Họ người dùng']) !!}
            <span class="text-danger">{{ $errors->first('first_name') }}</span>
        </div>
        <div class="form-group">
            {!! Form::label('last_name', 'Tên :', ['class' => 'font-weight-bold']) !!} 
            {!! Form::text('last_name',$users->profile ? $users->profile->last_name : '', ['class' => $errors->has('last_name') ? 'form-control error' : 'form-control', 'placeholder' => 'Tên người dùng']) !!}
            <span class="text-danger">{{ $errors->first('last_name') }}</span>
        </div>
        <div class="form-group">
            {!! Form::label('address', 'Địa chỉ :', ['class' => 'font-weight-bold']) !!} 
            {!! Form::text('address',$users->profile ? $users->profile->address : '', ['class' => $errors->has('address') ? 'form-control error' : 'form-control', 'placeholder' => 'Nhập địa chỉ']) !!}
            <span class="text-danger">{{ $errors->first('address') }}</span>
        </div>
        <div class="form-group">
            {!! Form::label('phone', 'Số điện thoại :', ['class' => 'font-weight-bold']) !!} 
            {!! Form::text('phone',$users->profile ? $users->profile->phone : '', ['class' => $errors->has('phone') ? 'form-control error' : 'form-control', 'placeholder' => 'Nhập số điện thoại']) !!}
            <span class="text-danger">{{ $errors->first('phone') }}</span>
        </div>
        <div class="form-group">
            {!! Form::label('date_of_birth', 'Ngày sinh :', ['class' => 'font-weight-bold']) !!} 
            {!! Form::date('date_of_birth',$users->profile ? \Carbon\Carbon::parse($users->profile->date_of_birth) : \Carbon\Carbon::now(),['class' => $errors->has('date_of_birth') ? 'form-control error' : 'form-control']) !!}
            <span class="text-danger">{{ $errors->first('date_of_birth') }}</span>
        </div>
        <div class="form-group">
            {!! Form::label('gender', 'Giới tính :', ['class' => 'font-weight-bold']) !!}
            <input type="radio" name="gender" class="default-control-input" id="gender" value ="1" {{($users->profile) ? ($users->profile->gender != null) ? 'checked': 'Nam' : 'checked'}}>Nam</option>
            
            <input type="radio" name="gender" class="default-control-input" id="gender" value ="0" {{$users->profile ? ($users->profile->gender == null ) ? 'checked': 'Nữ' : ''}}>Nữ</option>
                            
        </div>
        <div class="form-group">
            {!! Form::label('country', 'Quốc gia :', ['class' => 'font-weight-bold']) !!} 
            {!! Form::text('country',$users->profile ? $users->profile->country : '', ['class' => $errors->has('country') ? 'form-control error' : 'form-control', 'placeholder' => 'Nhập quốc gia']) !!}
            <span class="text-danger">{{ $errors->first('country') }}</span>
        </div>
        <div class="form-group avatar-upload">
            {!! Form::label('avatar', 'Avatar :', ['class' => 'font-weight-bold']) !!} 
             <div class="avatar-edit">
            {!! Form::file('avatar', ['id' => 'imageUpload' , 'accept' => '.png,.jpg,.jpeg']) !!}
            <label for="imageUpload"></label>
            </div>
            
            <div class="avatar-preview">
                
                <div id="imagePreview" style="background-image: url( {{ $users->profile ? url('vendor/images/' . $users->profile->avatar) : url('vendor/images/tony.png') }} );">
            </div>
            <span class="text-danger">{{ $errors->first('avatar') }}</span>
            </div>
        </div>
       
            </div>
                </div>
                <div class="form-group">
                    {!! Form::submit('Lưu thay đổi', ['class' => 'btn btn-dark border-0 shadow b-r-xs']) !!}
                    <a href="{{ route('users.index') }}" class="btn btn-light btn-outline-dark b-r-xs">Hủy</a>
                    </div>
                    </div>
                    {!! Form::close() !!}  
                    </div>                            
                    </div>
        
@endsection
