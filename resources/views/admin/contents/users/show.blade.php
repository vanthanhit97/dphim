@extends('admin.layouts.master')
@section('title','Thông tin user')
@section('page-heading')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2 class="text-uppercase font-weight-bold">Quản lý phim</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>films</strong>
            </li>
            <li class="breadcrumb-item active">
                <strong>show</strong>
            </li>
        </ol>
    </div>
   
</div>
@endsection
@section('content')
<div class="form-group row bg-white pb-4">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="form-group avatar-upload">
            <div class="avatar-preview center"> 
                <div id="imagePreview" style="background-image: url( {{ $users->profile ? url('vendor/images/' . $users->profile->avatar) : '' }} ); ">
                </div>   
            </div>
            </div>
            <div class="form-group">
            <center>
            <span><strong>Email: </strong></span>
            <span class="label label-warning">{{$users->email}}</span>
            </center>
            <hr>
            <center>
            <p><strong>Họ: </strong>
            <span class="">{{$users->profile ? $users->profile->first_name : ''}}</span>
            </p>
            <p><strong>Tên: </strong>
            <span class="">{{$users->profile ? $users->profile->last_name : ''}}</span>
            </p>
            <p><strong>Địa chỉ: </strong>
            <span class="">{{$users->profile ? $users->profile->address : ''}}</span>
            </p>
            <p><strong>Số điện thoại: </strong>
            <span class="">{{$users->profile ? $users->profile->phone : ''}}</span>
            </p>
            <p><strong>Ngày sinh: </strong>
            <span class="">{{$users->profile ? date('d/m/Y',strtotime(date($users->profile->date_of_birth))) : ''}}</span>
            </p>
            <p><strong>Giới tính: </strong>
            <label >{{ $users->profile ? (($users->profile->gender) == 0 ? 'Nữ' : 'Nam') : 'Chưa có' }}</label>
            </p>
            <p><strong>Quốc gia: </strong>
            <span class="">{{$users->profile ? $users->profile->country : ''}}</span>
            </p>
            <a href="{{ route('users.index') }}" class="btn btn-info float-right b-r-xs ml-3">Back</a>
            </center>
            </div>
        </div>
    </div>
</div>
@endsection




