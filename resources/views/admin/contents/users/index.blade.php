@extends('admin.layouts.master') 
@section('title','Quản lý user') 
@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2 class="text-uppercase font-weight-bold">Quản lý user</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>User</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <div class="form-group row bg-white pb-4">
        <div class="col-md-12 position-alert">
            @include('admin.includes.alert')
        </div>
        <div class="col-md-12">
        <div class="container mt-4">
    <div class="form-group border-0 shadow">
        <div class="card">
            <div class="card-header bg-dark text-white text-center font-weight-bold text-uppercase py-3">
                Danh sách danh mục
            </div>
            <div class="card-body">
                <div class="container-fruild">
                    <div class="form-group">
                        <table class="table table-hover table-responsive-md table-striped">
                            <thead class="table-primary">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Tên người dùng</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Ngày tạo</th>
                                    <th scope="col" width="200px" class="align-middle">Xử lý</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users_all as $key => $user)
                                <tr class="{{ (isset($users) && $users->id == $user->id) ? 'bg-info font-weight-bold' : 'table-primary' }}">
                                        <td><strong>{{ ++$key }}.</strong></td>
                                        <td>{!! $user->name !!}</td>
                                        <td>{!! $user->email !!}</td>
                                        <td>{!! date('d/m/Y',strtotime(date($user->created_at))) !!}</td>
                                        <td class="align-middle">
                                            @include('admin.modals.users.delete')
                                            <a class='btn btn-primary btn-xs b-r-xs' href="{!! route('users.show',$user->slug) !!}"><i class="far fa-eye"></i> Show</a>
                                            <a class='btn btn-success btn-xs b-r-xs' href="{!! route('users.edit',$user->slug) !!}"><i class="far fa-edit"></i> Edit</a>
                                            <a href='#Delete-{{$user->slug}}' data-toggle="modal" class="btn btn-danger btn-xs b-r-xs"><i class="far fa-trash-alt"></i> Delete</a>
                                            
                                        </td>
                                </tr>     
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>
    </div>
@endsection
