<div class="col-md-12 position-alert">
    @include('admin.includes.alert')
</div>
<div class="form-group" id="content">
    <div class="row">
        @foreach ($medias as $key => $media)
            <div class="col-lg-3 col-md-4 col-xs-6 thumb mb-0">
                <a href="{{ asset('vendor/images/'.$media) }}" class="fancybox" rel="ligthbox" title="{{ $media }}"> 
                    <img src="{{ asset('vendor/images/'.$media) }}" class="zoom img-fluid " alt="">
                </a>
                <div class="pl-2 pr-2">
                    <h4 class="media__title text-center  text-truncate w-100">{{ $media }}</h4>
                </div>
                <hr class="mt-0 ">
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" value="{{ $media }}" name="medias[]" id="{{ $key }}" class="custom-control-input">
                            <label for="{{ $key }}" class="custom-control-label">Đánh dấu</label>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <input type="hidden" name="_method" value="delete" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a class="btn btn-danger btn-xs b-r-xs float-right" data-toggle="modal" href='#modalDelete' data-id="{{$media}}" data-token="{{csrf_token()}}" ><i class="far fa-trash-alt"></i> Delete</a>    
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>