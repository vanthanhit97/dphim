@extends('admin.layouts.master')
@section('title','Thêm phim mới')
@section('page-heading')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2 class="text-uppercase font-weight-bold">Quản lý thư viện ảnh</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>media</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        {{ Form::open(['method'=>'DELETE','route'=>['media.deletefiles'],'id'=>'delete_data']) }}
        <div class="title-action">
            {!! Form::submit('Xóa tất cả ảnh đánh dấu',['class'=>"btn btn-danger b-r-xs"]) !!}
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="form-group bg-white row pb-4 gallery__image">
    @if (session('notification'))
        <div class="middle-box text-center animated fadeInRightBig mt-3">
            <h2 class="font-bold">{{ session('notification') }}</h2>
            <div class="error-desc">
                Bạn có thể tải lên hình ảnh thông qua upload phim
                <br><a href="{{ URL::to('admin/dashboard') }}" class="btn btn-primary m-t">Dashboard</a>
            </div>
        </div>
    @endif
    <div class="container-fluid page-top mb-5">
        @include('admin.contents.medias.list')
    </div>
    <div class="loading">
        <i class="fa fa-sync-alt fa-spin fa-2x fa-fw"></i><br />
        <span>Loading</span>
    </div>
    {{ Form::close() }}
    @include('admin.modals.medias.delete')
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/css/gallery.css') }}">
@endsection
@section('javascript')
    <script src="{{ asset('vendor/js/ajax-crud.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            }); 
            $(".zoom").hover(function(){             
                $(this).addClass('transition');
            }, function(){             
                $(this).removeClass('transition');
            });
        });
    </script>
@endsection

