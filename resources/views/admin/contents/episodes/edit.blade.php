@extends('admin.layouts.master') 
@section('title','Quản lý Trailer') 
@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2 class="text-uppercase font-weight-bold">Quản lý Trailer</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Trailers</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <div class="form-group row bg-white pb-4">
        <div class="col-md-12 position-alert">
            @include('admin.includes.alert')
        </div>
        <div class="col-md-8">
            {!! Form::model($episodes,array('route' => array('episodes.update',$episodes->id),'method' => 'PUT')) !!}
            {!! csrf_field() !!}
            <div class="container mt-5">
                <div class="form-group row">
                    <div class="col-md-12">
                        <h3><i class="fas fa-edit"></i> Sửa tập phim</h3>
                    </div>
                </div>
                <div class="form-group border-0 shadow">
                    <div class="card">
                        <div class="card-header bg-dark text-white text-uppercase font-weight-bold">
                            Danh mục
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="form-group required">
                                <div class="form-group">
                                    {!! Form::label('film_id', 'Tên phim', ['class' => 'font-weight-bold']) !!} 
                                    {!! Form::select('film_id', $films_all, null, ['class' => 'form-control', 'placeholder' => 'Chọn phim']) !!}
                                    <span class="text-danger">{{ $errors->first('film_id') }}</span>
                                    </div>
                                   <div class="form-group">
                                    {!! Form::label('episode', 'Tập phim', ['class' => 'font-weight-bold']) !!} 
                                    {!! Form::text('episode',$episodes->episode, ['class' => $errors->has('episode') ? 'form-control error' : 'form-control', 'placeholder' => 'Nhập tập phim']) !!}
                                    <span class="text-danger">{{ $errors->first('episode') }}</span>
                                   </div>
                                    <div class="form-group">
                                    {!! Form::label('link', 'URL', ['class' => 'font-weight-bold']) !!} 
                                    {!! Form::text('link',$episodes->link, ['class' => $errors->has('link') ? 'form-control error' : 'form-control', 'placeholder' => 'Nhập đường dẫn']) !!}
                                    <span class="text-danger">{{ $errors->first('link') }}</span>
                                    </div>    
                                </div>
                                <div class="form-group">
                                    <div class="row ml-0">
                                        {!! Form::label('', 'Trạng thái : ', ['class' => 'font-weight-bold']) !!}
                                        <div class="custom-control custom-switch ml-4">
                                            <input type="checkbox" name="status" class="custom-control-input" id="status" {{($episodes->status != null) ? 'checked value=1':'value=0'}}>
                                            {{Form::label('status', ($episodes->status != null) ? 'Công khai':'Tạm ẩn', ['class' => 'custom-control-label'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::submit('Lưu thay đổi', ['class' => 'btn btn-dark border-0 shadow b-r-xs']) !!}
                    <a href="{{ route('episodes.index') }}" class="btn btn-light btn-outline-dark b-r-xs">Hủy</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
@endsection
