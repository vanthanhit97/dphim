
    <div class="form-group row bg-white pb-4">
        <div class="col-md-12 position-alert">
            @include('admin.includes.alert')
        </div>
        <div class="col-md-12">
            <div class="container mt-4">
    <div class="form-group border-0 shadow" id="content">
        <div class="card">
            <div class="card-header bg-dark text-white text-center font-weight-bold text-uppercase py-3">
                Danh sách episodes
            </div>
            <div class="col-md-12">
        <div class="mt-5 mb-5" >
        <div class="row">
            <div class="col-sm-2">
                <div class="row">
                    <div class="col-sm-11 input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text border-0" style="background: none;font-size: 15px;">Hiển thị</label>
                        </div>
                        {!! Form::select('table_length',
                            ['10'=>'10','25'=>'25','50'=>'50','100'=>'100'],
                            request()->session()->get('table_length'),
                            ['class'=>'form-control browser-default custom-select rounded','onChange'=>'ajaxLoad("'.url("admin/episodes").'?table_length="+this.value)'])
                        !!}
                    </div>
                </div>
            </div>
            <div class="col-sm-10 form-group align-right">
                <div class="col-sm-9">
                    <div class="input-group">
                        <input type="hidden" id="xoa" value="" name="xoa">
                        <input class="form-control" id="search" value="{{ request()->session()->get('search') }}" 
                            onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('admin/episodes')}}?search='+this.value)"
                            placeholder="Nhập tập phim cần tìm..." name="search" type="text"/>
                        <div class="input-group-btn ml-1">
                            <button type="submit" class="btn btn-info b-r-xs" 
                                onclick="ajaxLoad('{{url('admin/episodes')}}?search='+$('#search').val())">Tìm kiếm
                            </button>
                        </div>
                        <div class="input-group-btn ml-1">
                            <button type="submit" class="btn btn-outline-secondary b-r-xs"
                                onclick="ajaxLoad('{{url('admin/episodes')}}?search='+$('#xoa').val())"><i class="fas fa-redo-alt"></i> 
                                Reload
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (!empty($keyword_s))
            <div class="row">
                <h3 class="pl-3">Tìm kiếm từ khóa : <span class="text-navy">{{ $keyword_s }}</span></h3>
            </div>
        @endif
            <div class="card-body">
                <div class="container p-0">
                    <div class="form-group">
                        <table class="table table-hover table-responsive-md table-striped">
                            <thead class="table-primary">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Tên phim</th>
                                    <th scope="col">Tập phim</th>
                                    <th scope="col">URL</th>
                                    <th scope="col" class="text-center" width="140px">Xử lý</th>
                                </tr>
                            </thead>
                            @if (count($episodes_all) >= 1)
                            <tbody>
                                @foreach ($episodes_all as $key => $episode)
                                    <tr class="{{ (isset($episodes) && $episodes->id == $episode->id) ? 'bg-info font-weight-bold' : '' }}">
                                        <td><strong>{{ ++$key }}.</strong></td>
                                        <td>{{$episode->film->title }}</td>
                                        <td>{{$episode->episode }}</td>
                                        <td>{{ $episode->link}}</td>
                                        <td class="text-center" >
                                            @include('admin.modals.episodes.delete')
                                            <a class='btn btn-success btn-xs b-r-xs' href="{!! route('episodes.edit',$episode->id) !!}"><i class="far fa-edit"></i> Edit</a>
                                            <a href="#Delete-{{$episode->id}}" data-toggle="modal" class="btn btn-danger btn-xs b-r-xs"><i class="far fa-trash-alt"></i> Delete</a>
                                        </td>
                                    </tr>     
                                @endforeach
                            </tbody>
                            @else
                                <div class="d-md-flex justify-content-center pt-4">
                                    <h3>Dữ liệu không có hoặc không tồn tại</h3>
                                </div>
                            @endif
                        </table>
                        <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Hiển thị từ {{ (count($episodes_all) >= 1) ? '1' : '0' }} đến {{ count($episodes_all) }} trong tổng số {{ $episodes_all->total() }} mục</label>
                        </div>
                        <div class="col-md-6">          
                            <div class="float-right">
                                {!! $episodes_all->links("pagination::bootstrap-4") !!}
                            </div>        
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>
        
    </div>


