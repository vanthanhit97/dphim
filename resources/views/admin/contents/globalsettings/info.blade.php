<div class="container mt-5">
    <div class="form-group border-0 shadow">
        <div class="card">
            <div class="card-header bg-dark text-center text-white text-uppercase font-weight-bold">
                Hiển thị thông tin website
            </div>
            <div class="card-body pl-0 pr-0">
                <div class="container-fluid">
                    <div class="form-group">
                        {!! Form::label('company_name', 'Tên Công Ty', ['class' => 'font-weight-bold']) !!}
                        <p><i class="far fa-hand-point-right text-danger"></i> <span class="text-primary"> {{ $globalSetting->company_name }}</span></p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('company_phone', 'Số điện thoại liên hệ', ['class' => 'font-weight-bold']) !!}
                        <p><i class="far fa-hand-point-right text-danger"></i> <span class="text-primary"> {{ $globalSetting->company_phone }}</span></p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('company_email', 'Email Công Ty', ['class' => 'font-weight-bold']) !!}
                        <p><i class="far fa-hand-point-right text-danger"></i> <span class="text-primary"> {{ $globalSetting->company_email }}</span></p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('company_facebook', 'Link Facebook của Công Ty', ['class' => 'font-weight-bold']) !!}
                        <p><i class="far fa-hand-point-right text-danger"></i> <span class="text-primary"> {{ $globalSetting->company_facebook }}</span></p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('company_youtube', 'Link Youtube của Công Ty', ['class' => 'font-weight-bold']) !!}
                        <p><i class="far fa-hand-point-right text-danger"></i> <span class="text-primary"> {{ $globalSetting->company_youtube }}</span></p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('company_googleplus', 'Link Google Plus của Công Ty', ['class' => 'font-weight-bold']) !!}
                        <p><i class="far fa-hand-point-right text-danger"></i> <span class="text-primary"> {{ $globalSetting->company_googleplus }}</span></p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('company_address', 'Địa chỉ liên hệ của Công Ty', ['class' => 'font-weight-bold']) !!}
                        <p><i class="far fa-hand-point-right text-danger"></i> <span class="text-primary"> {{ $globalSetting->company_address }}</span></p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('company_copyright', 'Thông tin Copyright', ['class' => 'font-weight-bold']) !!}
                        <p><i class="far fa-hand-point-right text-danger"></i> <span class="text-primary"> {{ $globalSetting->company_copyright }}</span></p>
                    </div>
                    <hr>
                    <div class="form-group">    
                        <div class="float-left">
                            <a class="btn btn-primary b-r-xs" href="{{ route('global-setting.edit', $globalSetting->slug) }}">Cập nhật lại thông tin</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>