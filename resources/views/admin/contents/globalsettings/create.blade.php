@if (isset($global))
    {!! Form::model($global,array('route' => array('global-setting.update',$global->slug),'method' => 'PUT')) !!}
@else
    {!! Form::open(array('url' => 'admin/global-setting', 'method' => 'POST')) !!} 
@endif
{!! csrf_field() !!}
<div class="container mt-5">
    <div class="form-group border-0 shadow">
        <div class="card">
            <div class="card-header bg-dark text-white text-uppercase font-weight-bold">
                {{ isset($global) ? 'Cập nhật ' : '' }}Thông tin website
            </div>
            <div class="card-body">
                <div class="container">
                    <div class="form-group required">
                        {!! Form::label('name', 'Tên website ( công ty )', ['class' => 'font-weight-bold']) !!} 
                        {!! Form::text('name',isset($global) ? $global->company_name : '', ['class' => $errors->has('name') ? 'form-control error' : 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', 'Số điện thoai', ['class' => 'font-weight-bold']) !!} 
                        {!! Form::tel('phone', isset($global) ? $global->company_phone : '', ['class' => $errors->has('phone') ? 'form-control error' : 'form-control', 'maxlength' => '12']) !!}
                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail', ['class' => 'font-weight-bold']) !!} 
                        {!! Form::text('email', isset($global) ? $global->company_email : '', ['class' => $errors->has('email') ? 'form-control error' : 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('facebook', 'Link Facebook', ['class' => 'font-weight-bold']) !!} 
                        {!! Form::text('facebook', isset($global) ? $global->company_facebook : '', ['class' => $errors->has('facebook') ? 'form-control error' : 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('facebook') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('youtube', 'Link Youtube', ['class' => 'font-weight-bold']) !!} 
                        {!! Form::text('youtube', isset($global) ? $global->company_youtube : '', ['class' => $errors->has('youtube') ? 'form-control error' : 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('youtube') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('google', 'Link Google plus', ['class' => 'font-weight-bold']) !!} 
                        {!! Form::text('google', isset($global) ? $global->company_googleplus : '', ['class' => $errors->has('google') ? 'form-control error' : 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('google') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('address', 'Địa chỉ website ( Công ty )', ['class' => 'font-weight-bold']) !!} 
                        {!! Form::text('address', isset($global) ? $global->company_address : '', ['class' => $errors->has('address') ? 'form-control error' : 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('address') }}</span>
                    </div>
                    <div class="form-group required">
                        {!! Form::label('copyright', 'Copyright', ['class' => 'font-weight-bold']) !!} 
                        {!! Form::text('copyright', isset($global) ? $global->company_copyright : '', ['class' => $errors->has('coppright') ? 'form-control error' : 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('copyright') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Lưu thông tin', ['class' => 'btn btn-success border-0 shadow b-r-xs']) !!}
                        @if (isset($global))
                            <a href="{{ route('global-setting.index') }}" class="btn btn-dark b-r-xs ml-3">Hủy</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}