@extends('admin.layouts.master') 
@section('title','Quản lý danh mục') 
@section('content')
    <div class="form-group row bg-white pb-4">
        <div class="col-md-12 position-alert">
            @include('admin.includes.alert')
        </div>
        <div class="col-md-7">
            @include('admin.contents.globalsettings.info')
        </div>
        <div class="col-md-5">
            @include('admin.contents.globalsettings.create')
        </div>
    </div>
@endsection
