@extends('admin.layouts.master')
@section('title','Thêm phim mới')
@section('page-heading')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2 class="text-uppercase font-weight-bold">Quản lý phim</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>films</strong>
            </li>
            <li class="breadcrumb-item active">
                <strong>create</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="{{ route('films.index') }}" class="btn btn-primary b-r-xs">Danh sách phim</a>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="form-group row bg-white pb-4">
    <div class="col-md-12 position-alert">
        @include('admin.includes.alert')
    </div>
    <div class="col-md-12">
        {!! Form::open(array('url' => 'admin/films', 'method' => 'POST','files' => true,'enctype' => 'multipart/form-data')) !!}
        {!! csrf_field() !!}
        <div class="container-fluid mt-5">
            <div class="form-group border-0 shadow">
                <div class="card">
                    <div class="card-header bg-dark text-white text-uppercase font-weight-bold">
                        Thêm Phim mới
                    </div>
                    <div class="card-body">
                        <div class="container-fluid p-2">
                            <div class="form-group row mb-0">
                                <div class="col-md-7 form-group required">
                                    {!! Form::label('title', 'Tên phim', ['class' => 'font-weight-bold']) !!}
                                    {!! Form::text('title', '', ['class' => $errors->has('title') ? 'form-control error' :'form-control', 'placeholder' => 'Nhập tên phim']) !!}
                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                </div>
                                <div class="col-md-5">
                                    {!! Form::label('title_eng', 'Tên phim tiếng anh', ['class' => 'font-weight-bold']) !!}
                                    {!! Form::text('title_eng', '', ['class' => $errors->has('title_eng') ? 'form-control error' :'form-control']) !!}
                                    <span class="text-danger">{{ $errors->first('title_eng') }}</span>
                                </div>
                            </div>
                            <div class="form-group required">
                                {!! Form::label('description', 'Mô tả', ['class' => 'font-weight-bold']) !!}
                                {!! Form::textarea('description', '', ['class' => $errors->has('description') ? 'form-control error' :'form-control', 'placeholder' => 'Nhập mô tả cho phim', 'rows' => '3', 'maxlength' => '320']) !!}
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            </div>
                            <div class="form-group">
                                <div class="form-group required mb-0">
                                    {!! Form::label('content', 'Nội dung', ['class' => 'font-weight-bold']) !!}
                                </div>
                                <div class="ibox-content no-padding">
                                    <textarea name="content" rows="3" class="form-control summernote">{{ old('content') }}</textarea>
                                </div>
                                <span class="text-danger">{{ $errors->first('content') }}</span>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-4 form-group">
                                    {!! Form::label('language', 'Ngôn ngữ', ['class' => 'font-weight-bold']) !!}
                                    {!! Form::text('language', '', ['class' => $errors->has('language') ? 'form-control error' :'form-control']) !!}
                                    <span class="text-danger">{{ $errors->first('language') }}</span>
                                </div>
                                <div class="col-md-4 form-group">
                                    {!! Form::label('distributor', 'Nhà sản xuất', ['class' => 'font-weight-bold']) !!}
                                    {!! Form::text('distributor', '', ['class' => $errors->has('distributor') ? 'form-control error' :'form-control']) !!}
                                    <span class="text-danger">{{ $errors->first('distributor') }}</span>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-4 form-group">
                                    {!! Form::label('release_date', 'Ngày phát hành', ['class' => 'font-weight-bold']) !!}
                                    {!! Form::date('release_date', \Carbon\Carbon::now(),['class' => 'form-control']) !!}
                                </div>
                                <div class="col-md-2 form-group">
                                    {!! Form::label('run_time', 'Thời lượng', ['class' => 'font-weight-bold']) !!}
                                    {!! Form::number('run_time', '', ['class' => $errors->has('run_time') ? 'form-control error' :'form-control']) !!}
                                    <span class="text-danger">{{ $errors->first('run_time') }}</span>
                                </div>
                                <div class="col-md-3 form-group">
                                    {!! Form::label('quality', 'Chất lượng', ['class' => 'font-weight-bold']) !!}
                                    {!! Form::select('quality', ['CAM' => 'Bản CAM','Thường' => 'Bản Thường','Đẹp' => 'Bản Đẹp'], '', ['class' => $errors->has('quality') ? 'form-control error' :'form-control', 'placeholder' => 'Chất lượng phim']) !!}
                                    <span class="text-danger">{{ $errors->first('quality') }}</span>
                                </div>
                                <div class="col-md-3 form-group">
                                    {!! Form::label('resolution', 'Độ phân giải', ['class' => 'font-weight-bold']) !!}
                                    {!! Form::select('resolution', ['CAM' => 'CAM','CAM 720p' => 'CAM 720p','Thường' => 'Thường','Thường Đẹp' => 'Thường Đẹp','HD' => 'HD','HD 720p' => 'HD 720p', 'Full HD' => 'Full HD'], '', ['class' => $errors->has('resolution') ? 'form-control error' :'form-control', 'placeholder' => 'Độ phân giải phim']) !!}
                                    <span class="text-danger">{{ $errors->first('resolution') }}</span>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 form-group required">
                                    {!! Form::label('director_id', 'Đạo diễn', ['class' => 'font-weight-bold']) !!}
                                    {!! Form::select('director_id', $directors, old('director_id'), ['class' => $errors->has('director_id') ? 'form-control error' :'form-control', 'placeholder' => '-- Chọn đạo diễn --']) !!}
                                    <span class="text-danger">{{ $errors->first('director_id') }}</span>
                                </div>
                                <div class="col-md-6 form-group required">
                                    {!! Form::label('actor_id', 'Diễn viên', ['class' => 'font-weight-bold']) !!}
                                    {{ Form::select('actors[]', $actors,'', ['multiple', 'data-selected-text-format' => 'count > 2', 'class' => 'form-control selectpicker', 'title' => '-- Chọn diễn viên --'])}}
                                    <span class="text-danger">{{ $errors->first('actors') }}</span>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 form-group">
                                    <div class="form-group required mb-0">
                                        {!! Form::label('', 'Danh mục', ['class' => 'font-weight-bold required']) !!}
                                    </div>
                                    <div class="row">
                                        @foreach ($categories as $item)
                                            <div class="col-md-4">
                                                <div class="custom-control custom-checkbox mb-3">
                                                    {{Form::checkbox('categories[]', $item->id, ( is_array(old('categories')) && in_array(1, old('categories')) ? true : false), ['class' => 'custom-control-input', 'id' => $item->slug])}}
                                                    {{Form::label($item->slug, $item->name, ['class' => 'custom-control-label font-weight-bold pt-1'])}}
                                                </div>
                                                @if (count($item->childs))
                                                    @foreach ($item->childs as $child)
                                                        <div class="custom-control custom-checkbox mb-2 ml-3">
                                                            {{Form::checkbox('categories[]', $child->id, ( is_array(old('categories')) && in_array(1, old('categories')) ? true : false), ['class' => 'custom-control-input', 'id' => $child->slug])}}
                                                            {{Form::label($child->slug, $child->name, ['class' => 'custom-control-label pt-1'])}}
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                    <span class="text-danger">{{ $errors->first('categories') }}</span>
                                </div>
                                <div class="col-md-4 form-group required">
                                    <div class="form-group avatar-upload">
                                        {!! Form::label('avatar', 'Ảnh đại diện', ['class' => 'font-weight-bold']) !!}
                                        <div class="avatar-edit">
                                            {!! Form::file('avatar', ['id' => 'imageUpload' , 'accept' => '.png,.jpg,.jpeg']) !!}
                                            <label for="imageUpload"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview" style="background-image: url( {{ url('vendor/css/images/img-upload.png') }} );">
                                            </div>
                                        </div>
                                        <span class="text-danger">{{ $errors->first('avatar') }}</span>
                                    </div>
                                    <div class="form-group avatar-upload">
                                        {!! Form::label('cover_image', 'Ảnh bìa', ['class' => 'font-weight-bold']) !!}
                                        <div class="avatar-edit">
                                            {!! Form::file('cover_image', ['id' => 'coverUpload' , 'accept' => '.png,.jpg,.jpeg']) !!}
                                            <label for="coverUpload"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="coverPreview" style="background-image: url( {{ url('vendor/css/images/cover-upload.png') }} );">
                                            </div>
                                        </div>
                                        <span class="text-danger">{{ $errors->first('cover_image') }}</span>
                                    </div>
                                    <div class="form-group required mt-5">
                                        {!! Form::label('tags', 'Tag', ['class' => 'font-weight-bold']) !!}
                                        {{ Form::select('tags[]', $tags,'', ['multiple', 'data-selected-text-format' => 'count > 2', 'class' => 'form-control selectpicker', 'title' => '-- Chọn tag --'])}}
                                        <span class="text-danger">{{ $errors->first('tags') }}</span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <a href="{{ route('films.create') }}" class="btn btn-dark float-right b-r-xs ml-3">Hủy</a>
                                {!! Form::submit('Lưu dữ liệu', ['class' => 'btn btn-primary b-r-xs float-right']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.summernote').summernote({
                height: "250px",
                placeholder: 'Nội dung phim',
                callbacks: {
                    onImageUpload:function(files, editor, welEditable) {
                        for (var i = files.length - 1; i >= 0; i--) {
                            sendFile(files[i],this);
                        }
                    }
                }
            });
        });
        function sendFile (file, el) {
            var form_data = new FormData();
            form_data.append('file', file);
            $.ajax({
                data: form_data,
                type: "POST",
                url: '../editor-upload.php',
                cache: false,
                contentType: false,
                processData: false,
                success:function(url) {
                    $(el).summernote('editor.insertImage', url);
                }
            });
        }
    </script>
@endsection

