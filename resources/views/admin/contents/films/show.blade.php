@extends('admin.layouts.master')
@section('title','Thêm phim mới')
@section('page-heading')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2 class="text-uppercase font-weight-bold">Quản lý phim</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>films</strong>
            </li>
            <li class="breadcrumb-item active">
                <strong>show</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="{{ route('films.create') }}" class="btn btn-primary b-r-xs">Thêm phim mới <i class="fas fa-plus"></i></a>
            <a href="{{ route('films.index') }}" class="btn btn-info b-r-xs">Danh sách phim</a>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="form-group row bg-white pb-4">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="tabs">
                <nav class="tabs-nav">
                    <ul class="tabs-nav-items">
                        <li class="tabs-nav-item active-tab-nav-item">
                            <a href="#section-1" class="tabs-nav-btn">Thông tin phim</a>
                        </li>
                        <li class="tabs-nav-item">
                            <a href="#section-2" class="tabs-nav-btn">Trailer phim</a>
                        </li>
                        <li class="tabs-nav-item">
                            <a href="#section-3" class="tabs-nav-btn">Tập phim</a>
                        </li>
                    </ul>
                </nav>
                <div class="tabs-content">
                    <section id="section-1" class="tabs-content-item active-tab-content-item">
                        <div class="col-lg-12">
                            @include('admin.contents.films.info')
                        </div>
                    </section>
                    <section id="section-2" class="tabs-content-item">
                        <div class="col-lg-12">
                            @include('admin.contents.films.trailer')
                        </div>
                    </section>
                    <section id="section-3" class="tabs-content-item">
                        <div class="col-lg-12">
                            @include('admin.contents.films.episode')
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group row bg-white pb-5">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="row">
                <h2 class="font-weight-bold w-100"><u>Bình luận của phim :</u> <span class="text-navy">{{ $films->title }}</span></h2>
            </div>
            <div class="col-lg-12 mt-3">
                <input type="search" class="form-control" id="input-search" placeholder="Tìm kiếm...">
            </div>
            <div class="row chat-discussion searchable-container mt-3">
                @php
                    Carbon\Carbon::setLocale('vi');
                @endphp
                @foreach ($films->comments as $comment)
                    <div class="chat-message left align-middle w-100">
                        <img class="message-avatar mt-2" src="{{ url('vendor/images/'.$comment->user->profile->avatar) }}" alt="">
                        <div class="message">
                            <a class="message-author" href="#"> {{ $comment->user->name }} </a>
                            <span class="message-date">{!! Carbon\Carbon::parse($comment->created_at)->diffForHumans() !!}</span>
                            <span class="message-content">
                                {!! $comment->content !!}
                            </span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
    <script src="{{ asset('vendor/js/tab.js') }}"></script>
    <script src="{{ asset('vendor/js/n2jplayer.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(function() {    
                $('#input-search').on('keyup', function() {
                var rex = new RegExp($(this).val(), 'i');
                    $('.searchable-container .chat-message').hide();
                    $('.searchable-container .chat-message').filter(function() {
                        return rex.test($(this).text());
                    }).show();
                });
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: './{{ $films->slug }}/watch',
                data: {},
                contentType: "application/json; charset=utf-8",
                datatype: 'JSON',
                success:function(data){
                    data = JSON.stringify(data);
                    var values = [], arrEp = [];         
                    JSON.parse(data, function (key, value) {
                        if (typeof(value) != "object") {
                            values.push({[key]:value});
                        }
                    });
                    for(var i = 0; i < values.length; i++) {
                        const objEp = {};
                        objEp.name = Object.getOwnPropertyNames(values[i])[0];
                        objEp.link = Object.values(values[i])[0];
                        arrEp.push(objEp);
                    }
                    var setting = {
                        stepTime: 10,
                        width: "100%",
                        progressBarColor: "#1ab394",
                        autonext: false,
                        buttonBoxId: "box-episode",
                        isGetlinkDrive: false,
                        logo : "http://localhost/dphim/public/vendor/css/images/logo-dphim.png",
                        noNavButton: true,
                        source: arrEp
                    }
                    var player = n2jplayer('videoPlayer');
                    player.setting(setting);
                },
                error:function(data){
                        console.log(data);
                }
            });
            return false;
        });
    </script>
@endsection


