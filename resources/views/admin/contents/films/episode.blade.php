<div class="col-md-12">
    <div class="row">
        <h2 class="text-center font-weight-bold animated fadeInRightBig w-100">Danh sách tập của phim : <span class="text-navy">{{ $films->title }}</span></h2>
    </div>
    @if (count($films->episodes) > 0)
        <div class="row mt-3 w-100 justify-content-center">
            <div id="videoPlayer">
            </div>
        </div>
        <div class="row w-100 mx-2 my-3">
            <div class="ah-le-server font-weight-bold ah-le-fs35 mb-2"><span><u>Tổng Hợp :</u></span></div>
            <div id="box-episode">
            </div>
        </div>
        <hr>
    @endif
</div>
