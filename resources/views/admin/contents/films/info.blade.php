<div class="col-md-12">
    <div class="row">
        <h2 class="text-center font-weight-bold animated fadeInRightBig w-100">Thông tin phim : <span class="text-navy">{{ $films->title }}</span></h2>
    </div>
    <div class="row mt-3">
        <div class="col-md-4">
            <div class="row cover_image_div">
                <div class="col-md-12">
                    <img src="{{ asset('vendor/images/'.$films->cover_image) }}" class="img-responsive w-100">
                    <label class="font-weight-bold w-100 text-center mt-3">Ảnh bìa</label>
                </div>
                <div class="col-md-12 h-50" id="my-box-avatar">
                    <img src="{{ asset('vendor/images/'.$films->avatar) }}" class="img-responsive w-100 my-img-avatar ">
                    <label class="font-weight-bold w-100 text-center mt-3">Ảnh đại diện</label>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            @php
                $val_1 = !empty($f_kind[0]) ? $f_kind[0] : 9999;
                $val_2 = !empty($f_country[0]) ? $f_country[0] : 9999;
                $val_3 = !empty($f_year[0]) ? $f_year[0] : 9999;
            @endphp
            <h3>Tên phim : {{ $films->title }}</h3>
            <label class="font-weight-bold w-100">Tên phim ( tiếng anh ) : {{ ($films->title_eng) ? $films->title_eng : 'Chưa có' }}</label>
            <label class="font-weight-bold w-100">Đạo diễn : {{ $films->director->first_name.' '.$films->director->last_name }}</label>
            <label class="font-weight-bold w-100">Diễn viên : 
                @foreach ($films->actors as $key => $item)
                    {{ $item->first_name.' '.$item->last_name }}
                    @if (++$key < count($films->actors))
                        , 
                    @endif 
                @endforeach
            </label>
            <label class="font-weight-bold w-100">Quốc gia : 
                @foreach ($films->categories as $item)
                    @if ($item->parent_id == $val_2)
                        {{ $item->name }} ,
                    @endif
                @endforeach
            </label>
            <label class="font-weight-bold w-100">Năm :  
                @foreach ($films->categories as $item)
                    @if ($item->parent_id == $val_3)
                        {{ str_replace('Phim lẻ', '', $item->name) }} ,
                    @endif
                @endforeach
            </label>
            <label class="font-weight-bold w-100">Ngày phát hành : {{ date('d/m/Y',strtotime(date($films->release_date))) }}</label>
            <label class="font-weight-bold w-100">Thời lượng : {{ $films->run_time ? $films->run_time.' phút' : 'Chưa rõ' }}</label>
            <label class="font-weight-bold w-100">Chất lượng : {{ $films->quality ? 'Bản '.$films->quality : 'Chưa rõ' }}</label>
            <label class="font-weight-bold w-100">Độ phân giải : {{ $films->resolution ? $films->resolution : 'Chưa rõ' }}</label>
            <label class="font-weight-bold w-100">Ngôn ngữ : {{ $films->language ? $films->language : 'Chưa rõ' }}</label>
            <label class="font-weight-bold w-100">Thể loại : 
                @foreach ($films->categories as $key => $item)
                    @if ($item->parent_id == $val_1)
                        {{ $item->name }} ,
                    @endif
                @endforeach
            </label>
            <label class="font-weight-bold w-100">Nhà SX : {{ $films->distributor ? $films->distributor : 'Chưa rõ' }}</label>
            <label class="font-weight-bold w-100">Lượt xem : {{ $films->viewed }}</label>
            <label class="font-weight-bold w-100">Tags : 
                @foreach ($films->tags as $key => $item)
                    <span class="badge badge-info">{{ $item->name }}</span>
                @endforeach
            </label>
            <label class="w-100"><span class="font-weight-bold">Mô tả phim :</span> {{ $films->description}}</label>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h3 class="font-weight-bold">Nội dung phim : </h3>
        </div>
        <div class="col-md-12 ml-5">
            {!! $films->content !!}
        </div>
    </div>
    <hr>
</div>
<div class="col-md-12 mb-5">
    <a href="{{ route('films.index') }}" class="btn btn-info float-right b-r-xs ml-3">Quay lại</a>
    <a href="{{ route('films.edit', $films->slug) }}" class="btn btn-warning float-right b-r-xs">Sửa thông tin</a>
</div>