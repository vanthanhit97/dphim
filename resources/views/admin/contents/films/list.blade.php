<div class="col-md-12 position-alert">
    @include('admin.includes.alert')
</div>
<div class="col-md-12">
    <div class="mt-5 mb-5" id="content">
        <div class="row">
            <div class="col-sm-2">
                <div class="row">
                    <div class="col-sm-11 input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text border-0" style="background: none;font-size: 15px;">Hiển thị</label>
                        </div>
                        {!! Form::select('table_length',
                            ['10'=>'10','25'=>'25','50'=>'50','100'=>'100'],
                            request()->session()->get('table_length'),
                            ['class'=>'form-control browser-default custom-select rounded','onChange'=>'ajaxLoad("'.url("admin/films").'?table_length="+this.value)'])
                        !!}
                    </div>
                </div>
            </div>
            <div class="col-sm-10 form-group align-right">
                <div class="col-sm-9">
                    <div class="input-group">
                        <input type="hidden" id="xoa" value="" name="xoa">
                        <input class="form-control" id="search" value="{{ request()->session()->get('search') }}" 
                            onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('admin/films')}}?search='+this.value)"
                            placeholder="Nhập tên phim cần tìm..." name="search" type="text"/>
                        <div class="input-group-btn ml-1">
                            <button type="submit" class="btn btn-info b-r-xs" 
                                onclick="ajaxLoad('{{url('admin/films')}}?search='+$('#search').val())">Tìm kiếm
                            </button>
                        </div>
                        <div class="input-group-btn ml-1">
                            <button type="submit" class="btn btn-outline-secondary b-r-xs"
                                onclick="ajaxLoad('{{url('admin/films')}}?search='+$('#xoa').val())"><i class="fas fa-redo-alt"></i> 
                                Reload
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (!empty($keyword_s))
            <div class="row">
                <h3 class="pl-3">Tìm kiếm từ khóa : <span class="text-navy">{{ $keyword_s }}</span></h3>
            </div>
        @endif
        <div class="form-group">
            <table class="table table-hover table-responsive-md table-striped">
                <thead class="table-info">
                    <tr class="text-center">
                        <th scope="col" class="align-middle">#</th>
                        <th scope="col" width="200px" class="align-middle">Tên phim</th>
                        <th scope="col" width="140px" class="align-middle">Thể loại</th>
                        <th scope="col" class="align-middle">Ảnh bìa</th>
                        <th scope="col" class="align-middle">Mô tả</th>
                        <th scope="col" width="100px" class="align-middle">Chất lượng</th>
                        <th scope="col" class="align-middle">Lượt xem</th>
                        <th scope="col" width="200px" class="align-middle">Xử lý</th>
                    </tr>
                </thead>
                @if (count($films_all) >= 1)
                    <tbody>
                        @foreach ($films_all as $key => $value)
                            <tr>
                                <td class="align-middle">{{ ++$key }}</td>
                                <td class="align-middle text-success">{{ $value->title }}</td>   
                                <td class="align-middle">
                                    @php
                                        $val = !empty($f_kind[0]) ? $f_kind[0] : 9999;
                                    @endphp
                                    @foreach ($value->categories as $key => $item)
                                        @if ($item->parent_id == $val)
                                            {{ $item->name }} ,
                                        @endif
                                    @endforeach
                                </td>     
                                <td width="90">
                                    <div class="avatar-imitation w-100">
                                        <img src="{{ url('vendor/images/'.$value->avatar) }}" class="w-100 d-block h-100 img-responsive">
                                    </div>
                                </td>
                                <td class="align-middle text-center">{{ $value->description }}</td>
                                <td class="align-middle text-center">{{ $value->quality ? 'Bản '.$value->quality : '' }}</td>
                                <td class="align-middle text-center">{{ $value->viewed }}</td>
                                <td class="align-middle">
                                    <a class='btn btn-primary btn-xs b-r-xs' href="{!! route('films.show', $value->slug) !!}"><i class="far fa-eye"></i> Show</a>
                                    <a class='btn btn-success btn-xs b-r-xs' href="{!! route('films.edit', $value->slug) !!}"><i class="far fa-edit"></i> Edit</a>
                                    <input type="hidden" name="_method" value="delete" />
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <a class="btn btn-danger btn-xs b-r-xs" data-toggle="modal" href='#modalDelete' data-id="{{$value->slug}}" data-token="{{csrf_token()}}" ><i class="far fa-trash-alt"></i> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                @else
                    <div class="d-md-flex justify-content-center pt-4">
                        <h3>Dữ liệu không có hoặc không tồn tại</h3>
                    </div>
                @endif
            </table>
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label">Hiển thị từ {{ (count($films_all) >= 1) ? '1' : '0' }} đến {{ count($films_all) }} trong tổng số {{ $films_all->total() }} mục</label>
                </div>
                <div class="col-md-6">          
                    <div class="float-right">
                        {!! $films_all->links("pagination::bootstrap-4") !!}
                    </div>        
                </div>
            </div>  
        </div>
    </div>
</div>