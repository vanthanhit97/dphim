<div class="col-md-12">
    <div class="row">
        <h2 class="text-center font-weight-bold animated fadeInRightBig w-100">Trailer phim : <span class="text-navy">{{ $films->title }}</span></h2>
    </div>
    @if ($films->trailer)
        <div class="row mt-3 w-100 justify-content-center">
            {!! $films->trailer ? \Embed::make($films->trailer->link)->parseUrl()->setAttribute(['width' => 956])->getHtml() : '' !!}
        </div>
        <hr>
        <div class="col-md-12 mb-5">
            <a href="{{ route('films.index') }}" class="btn btn-info float-right b-r-xs ml-3">Quay lại</a>
            <a href="{{ route('trailers.edit', $films->trailer->id) }}" class="btn btn-warning float-right b-r-xs">Sửa thông tin</a>
        </div>
    @endif
</div>