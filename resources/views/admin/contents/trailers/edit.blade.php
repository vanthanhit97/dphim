@extends('admin.layouts.master') 
@section('title','Quản lý Trailer') 
@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2 class="text-uppercase font-weight-bold">Quản lý Trailer</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Trailers</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <div class="form-group row bg-white pb-4">
        <div class="col-md-12 position-alert">
            @include('admin.includes.alert')
        </div>
        <div class="col-md-8">
            {!! Form::model($trailers,array('route' => array('trailers.update',$trailers->id),'method' => 'PUT')) !!}
            {!! csrf_field() !!}
            <div class="container mt-5">
                <div class="form-group row">
                    <div class="col-md-12">
                        <h3><i class="fas fa-edit"></i> Sửa trailer</h3>
                    </div>
                </div>
                <div class="form-group border-0 shadow">
                    <div class="card">
                        <div class="card-header bg-dark text-white text-uppercase font-weight-bold">
                            Danh mục
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="form-group required">
                                    {!! Form::label('film_id_', 'Tên phim', ['class' => 'font-weight-bold']) !!} 
                                    {!! Form::select('film_id', $films_all, null, ['class' => 'form-control', 'placeholder' => 'Chọn phim']) !!}
                                    <span class="text-danger">{{ $errors->first('film_id') }}</span>
                                    {!! Form::label('link', 'Đường dẫn trailer', ['class' => 'font-weight-bold']) !!} 
                                    {!! Form::text('link',$trailers->link, ['class' => $errors->has('link') ? 'form-control error' : 'form-control', 'placeholder' => 'Nhập đường dẫn trailer']) !!}
                                    <span class="text-danger">{{ $errors->first('link') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::submit('Lưu thay đổi', ['class' => 'btn btn-dark border-0 shadow b-r-xs']) !!}
                    <a href="{{ route('trailers.index') }}" class="btn btn-light btn-outline-dark b-r-xs">Hủy</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
@endsection
