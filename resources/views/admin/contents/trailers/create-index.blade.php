@extends('admin.layouts.master') 
@section('title','Quản lý Trailer') 
@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2 class="text-uppercase font-weight-bold">Quản lý Trailer</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Trailer</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <div class="form-group row bg-white pb-4" id="content">
        <div class="col-md-12 position-alert">
            @include('admin.includes.alert')
        </div>
        <div class="col-md-5">
            {!! Form::open(array('url' => 'admin/trailers', 'method' => 'POST')) !!} 
            {!! csrf_field() !!}
            <div class="container mt-5">
                <div class="form-group row">
                    <div class="col-md-12">
                        <h3><i class="fas fa-pencil-alt"></i> Tạo mới trailer</h3>
                    </div>
                </div>
                <div class="form-group border-0 shadow"  >
                    <div class="card">
                        <div class="card-header bg-dark text-white text-uppercase font-weight-bold">
                            Danh mục
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="form-group required">
                                    <div class="form-group">
                                    {!! Form::label('film_id', 'Tên phim', ['class' => 'font-weight-bold']) !!} 
                                    {!! Form::select('film_id', $films_all, null, ['class' => 'form-control', 'placeholder' => 'Chọn phim']) !!}
                                    <span class="text-danger">{{ $errors->first('film_id') }}</span>
                                    </div>
                                    <div class="form-group">
                                    {!! Form::label('link', 'Đường dẫn trailer', ['class' => 'font-weight-bold']) !!} 
                                    {!! Form::text('link', '', ['class' => $errors->has('link') ? 'form-control error' : 'form-control', 'placeholder' => 'Nhập đường dẫn trailer']) !!}
                                    <span class="text-danger">{{ $errors->first('link') }}</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::submit('Lưu trailer', ['class' => 'btn btn-dark border-0 shadow b-r-xs']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-7">
            @include('admin.contents.trailers.table')
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('vendor/js/ajax-crud.js') }}"></script>
@endsection

