<div class="container mt-4">
    <div class="form-group border-0 shadow">
        <div class="card">
            <div class="card-header bg-dark text-white text-center font-weight-bold text-uppercase py-3">
                Danh sách Trailers
            </div>
            <div class="card-body">
                <div class="container-fluid p-0">              
                    <div class="form-group">
                        <table class="table table-hover table-responsive-md table-striped">
                            <thead class="table-primary">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Tên phim</th>
                                    <th scope="col">URL</th>
                                    <th scope="col" class="text-center" width="140px">Xử lý</th>
                                </tr>
                            </thead>
                            @if (count($trailers_all) >= 1)
                            <tbody>
                                @foreach ($trailers_all as $key => $trailer)
                                    <tr class="{{ (isset($trailers) && $trailers->id == $trailer->id) ? 'bg-info font-weight-bold' : '' }}">
                                        <td><strong>{{ ++$key }}.</strong></td>
                                        <td>{{ $trailer->film->title }} </td>
                                        <td>{{ $trailer->link }}</td>
                                        <td class="text-center" >
                                            @include('admin.modals.trailers.delete')
                                            <a class='btn btn-success btn-xs b-r-xs' href="{!! route('trailers.edit', $trailer->id) !!}"><i class="far fa-edit"></i> Edit</a>
                                            <a href="#Delete-{{$trailer->id}}" data-toggle="modal" class="btn btn-danger btn-xs b-r-xs"><i class="far fa-trash-alt"></i> Delete</a>
                                        </td>
                                    </tr>     
                                @endforeach
                            </tbody>
                            @else
                                <div class="d-md-flex justify-content-center pt-4">
                                    <h3>Dữ liệu không có hoặc không tồn tại</h3>
                                </div>
                            @endif
                        </table>
                        <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Hiển thị từ {{ (count($trailers_all) >= 1) ? '1' : '0' }} đến {{ count($trailers_all) }} trong tổng số {{ $trailers_all->total() }} mục</label>
                        </div>
                        <div class="col-md-6">          
                            <div class="float-right">
                                {!! $trailers_all->links("pagination::bootstrap-4") !!}
                            </div>        
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
