<div class="col-lg-3">
    <div class="ibox ">
        <div class="ibox-title">
            <span class="label label-success float-right"><i class="pull-left fa fa-chart-bar"></i></span>
            <h5>Tổng số lượt xem</h5>
        </div>
        <div class="ibox-content">
            <h1 class="no-margins timer count-number" data-to="{{ $viewed_total }}" data-speed="1500"></h1>
            <div class="w-100 text-right">
                <small class="text-success font-bold">lượt xem</small>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3">
    <div class="ibox ">
        <div class="ibox-title">
            <span class="label label-info float-right"><i class="pull-left fa fa-users"></i></span>
            <h5>Tổng số người dùng</h5>
        </div>
        <div class="ibox-content">
            <h1 class="no-margins timer count-number" data-to="{{ $users_total }}" data-speed="1500"></h1>
            <div class="w-100 text-right">
                <small class="text-info font-bold">tài khoản</small>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3">
    <div class="ibox ">
        <div class="ibox-title">
            <span class="label label-primary float-right"><i class="fa fa-film"></i></span>
            <h5>Tổng số phim</h5>
        </div>
        <div class="ibox-content">
            <h1 class="no-margins timer count-number" data-to="{{ $films_total }}" data-speed="1500"></h1>
            <div class="w-100 text-right">
                <small class="text-primary font-bold">bộ phim</small>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3">
    <div class="ibox ">
        <div class="ibox-title">
            <span class="label label-danger float-right"><i class="pull-left fa fa-list"></i></span>
            <h5>Tổng số chuyên mục</h5>
        </div>
        <div class="ibox-content">
            <h1 class="no-margins">{{ $categories_total[0].' - '. $categories_total[1] }}</h1>
            <div class="w-100 text-right">
                <small class="text-danger font-bold">chuyên mục (chính - phụ)</small>
            </div>
        </div>
    </div>
</div>
