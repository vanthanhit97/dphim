@extends('admin.layouts.master')
@section('title','Quản trị hệ thống')
@section('content')
<div class="row">
    @include('admin.contents.dashboards.generalreport')
</div>
<div class="row bg-white pb-4 ml-0 mr-0 mb-4">
    @include('admin.contents.dashboards.filmsreport')
</div>
@endsection
@section('javascript')
	<script src="{{ asset('vendor/js/tab.js') }}"></script>
    <script type="text/javascript">
        function getRandomColor() {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 10)];
            }
            return color;
        }
        $(document).ready(function(){
            //thong ke phim xem nhieu nhat
            $.ajax({
				url:"{{ url('admin/dashboard/mostview') }}",
				method: 'get',
				success: function(data){
					var title = [],viewed=[], color = [];
				    for (var i = 0; i < data.length; i++) {
						title.push(data[i].title.substr(0,40));
				        viewed.push(data[i].viewed);
				        color.push(getRandomColor());
				    }
					var ctx = document.getElementById("most-viewed-chart").getContext('2d');
					var chart = {
						type: 'horizontalBar',
						data:{
							labels: title,
							datasets:[{
								label: "Số lượt xem ",
								data:viewed,
								fillColor: "rgba(220,220,220,0.5)", 
							    strokeColor: "rgba(220,220,220,0.8)", 
							    highlightFill: "rgba(220,220,220,0.75)",
							    highlightStroke: "rgba(220,220,220,1)",
							    backgroundColor: color,
							}]
						},
						options: {
		                    scales: {
		                        yAxes: [{
		                            ticks: {
		                                beginAtZero:true
		                            }
		                        }]
		                    },
		                    legend: {
						    	display: false
						    },
							title: {
								display: true,
								text: '5 phim có số lượt xem cao nhất'
							}, 	
		                },
		       
					};
					new Chart(ctx,chart);
				}
            });
            //thong ke phim theo thang
			$.ajax({
				url:"{{ url('admin/dashboard/viewedmonth') }}", 
				method: 'get',
				success: function(data){
					var labels = [],viewed=[], color = [];
					for (var i = 0; i < data.length; i++) {
						labels.push(data[i].monthNum);
						viewed.push(data[i].films);
						color.push(getRandomColor());
					}
					var ctx = document.getElementById("line-chart").getContext('2d');
					var chart = {
						type: 'bar',
						data:{
							labels: labels,
							datasets:[{
								label: "Số bài viết trong tháng ",
								data:viewed,
								fillColor: "rgba(220,220,220,0.5)", 
								strokeColor: "rgba(220,220,220,0.8)", 
								highlightFill: "rgba(220,220,220,0.75)",
								highlightStroke: "rgba(220,220,220,1)",
								backgroundColor: color, 
							}]
						},
						options: {
							scales: {
								yAxes: [{
									ticks: {
										beginAtZero:true
									}
								}]
							},
							legend: {
								display: false
							},
							title: {
								display: true,
								text: 'Tổng số phim trong các tháng'
							}
						},
					};
					new Chart(ctx,chart);
				}
            });
            //thong ke truy cap theo trinh duyet
            $.ajax({
				url:"{{ url('admin/dashboard/viewbybrowser') }}",
				method: 'get',
				success: function(data){
					var browser = [],viewed=[], color = [];
				    for (var i = 0; i < data.length; i++) {
				        browser.push(data[i].browser_name);
				        viewed.push(data[i].films);
				        color.push(getRandomColor());
				    }
					var ctx = document.getElementById("browser-chart").getContext('2d');
					var chart = {
						type: 'doughnut',
						data:{
							labels: browser,
							datasets:[{
								data:viewed,
								fillColor: "rgba(220,220,220,0.5)", 
							    strokeColor: "rgba(220,220,220,0.8)", 
							    highlightFill: "rgba(220,220,220,0.75)",
							    highlightStroke: "rgba(220,220,220,1)",
							    backgroundColor: color,
							}]
						},
						
					};
					new Chart(ctx,chart);
				}
            });       
            //thong ke truy cap theo thiet bi
			$.ajax({
				url:"{{ url('admin/dashboard/viewbydevice') }}",
				method: 'get',
				success: function(data){
					var device = [],viewed=[], color = [];
				    for (var i = 0; i < data.length; i++) {
				        device.push(data[i].device);
				        viewed.push(data[i].films);
				        color.push(getRandomColor());
				    }
					var ctx = document.getElementById("device-chart").getContext('2d');
					var chart = {
						type: 'doughnut',
						data:{
							labels: device,
							datasets:[{
								data:viewed,
								fillColor: "rgba(220,220,220,0.5)", 
							    strokeColor: "rgba(220,220,220,0.8)", 
							    highlightFill: "rgba(220,220,220,0.75)",
							    highlightStroke: "rgba(220,220,220,1)",
							    backgroundColor: color,
							}]
						},
						
					};

					new Chart(ctx,chart);
				}
            });           
            //Thong ke viewed phim theo thang
			$('.reportviewbymonth').click(function(){				
				var fromdate = $('.from_date').val();
				var todate = $('.to_date').val();
				//convert
				var convert_fromdate = new Date(fromdate);
				var convert_todate = new Date(todate);
				
				var fromdate_convert = convert_fromdate.getDate()+'/'+(convert_fromdate.getMonth() + 1)+'/'+convert_fromdate.getFullYear();
				var todate_convert = convert_todate.getDate()+'/'+(convert_todate.getMonth() + 1)+'/'+convert_todate.getFullYear();
				//alert('date:'+fromdate_convert);
				$.ajax({					
					url:"{{ route('viewedInMonth') }}",
					method: 'get',
					data: {
						from_date: fromdate,
						to_date: todate
					},
					success: function(data){
						// console.log(data);
						var date_time = [],viewed=[];
					    for (var i = 0; i < data.length; i++) {
					        date_time.push(data[i].day);
					        viewed.push(data[i].total_viewed);
					    }
						var ctx = document.getElementById("viewed_in_month").getContext('2d');
						//alert(data_viewed);
						var chart = {
							type: 'line',
							data:{
								labels: date_time,
								datasets:[{
									label: 'Thống kê Viewed phim từ ngày '+fromdate_convert+' đến ngày '+todate_convert,
									data:viewed,
									fillColor: "rgba(220,220,220,0.5)", 
								    strokeColor: "rgba(220,220,220,0.8)", 
								    highlightFill: "rgba(220,220,220,0.75)",
								    highlightStroke: "rgba(220,220,220,1)",
									backgroundColor: "#23EF0E",
									borderColor: "#EC0672",
								}]
							},
							
						};

						new Chart(ctx,chart);
					}
				});
			});
        });
    </script>
@endsection
