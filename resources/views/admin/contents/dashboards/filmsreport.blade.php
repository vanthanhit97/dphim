<div class="container-fluid">  
    <div class="col-md-12">
        <div class="tabs">
            <nav class="tabs-nav">
                <ul class="tabs-nav-items">
                    <li class="tabs-nav-item active-tab-nav-item">
                        <a href="#section-1" class="tabs-nav-btn">Thống kê sơ bộ về phim</a>
                    </li>
                    <li class="tabs-nav-item">
                        <a href="#section-2" class="tabs-nav-btn">Trình duyệt và thiết bị</a>
                    </li>
                    <li class="tabs-nav-item">
                        <a href="#section-3" class="tabs-nav-btn">Thống kê lượng truy cập theo tháng/năm</a>
                    </li>
                </ul>
            </nav>
            <div class="tabs-content">
                <section id="section-1" class="tabs-content-item active-tab-content-item">
                    <div class="col-lg-12">
                        <div class="">
                            <div><i class="fas fa-caret-right"></i> <u>Thống kê phim được xem nhiều nhất</u></div>
                            <div class="panel-body mt-4">
                                <canvas id="most-viewed-chart" height="110"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mt-3">
                        <div class="">
                            <div><i class="fas fa-caret-right"></i> <u>Thống kê phim theo tháng</u></div>
                            <div class="panel-body mt-4">
                                <canvas id="line-chart" height="110"></canvas>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="section-2" class="tabs-content-item">
                    <div class="row">
                        <div class="col-md-6">
                            <div><i class="fas fa-caret-right"></i> <u>Thống kê truy cập theo trình duyệt</u></div>
                            <div class="panel-body mt-4">
                                <canvas id="browser-chart"></canvas>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div><i class="fas fa-caret-right"></i> <u>Thống kê truy cập theo thiết bị</u></div>
                            <div class="panel-body mt-4">
                                <canvas id="device-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="section-3" class="tabs-content-item">
                    <div class="col-lg-12 mt-3">
                        <div class="">
                            <div><i class="fas fa-caret-right"></i> <u>Thống kê lượng truy cập theo tháng/Năm</u></div>
                            <div class="col-md-12 form-inline mt-3">
                                <div class="form-group">
                                    {!! Form::label("Từ ngày:") !!}
                                    {!! Form::date('fromdate', \Carbon\Carbon::now()->startOfMonth(),['class' => 'form-control from_date ml-2 mr-5']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label("Đến ngày:") !!}
                                    {!! Form::date('todate', \Carbon\Carbon::now(),['class' => 'form-control to_date ml-2 mr-2']) !!}
                                </div>
                                {!! Form::button('Xem thống kê',['class'=>'btn btn-primary b-r-xs reportviewbymonth']) !!}
                            </div>
                            <div class="panel-body mt-4">
                                <canvas id="viewed_in_month" height="110"></canvas>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>