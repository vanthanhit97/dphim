<div class="col-md-12 position-alert">
    @include('admin.includes.alert')
</div>
<div class="col-md-12">
    <div class="mt-5 mb-5" id="content">
        <div class="row">
            <div class="col-sm-2">
                <div class="row">
                    <div class="col-sm-11 input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text border-0" style="background: none;font-size: 15px;">Hiển thị</label>
                        </div>
                        {!! Form::select('table_length',
                            ['5'=>'5','10'=>'10','15'=>'15','20'=>'20'],
                            request()->session()->get('table_length'),
                            ['class'=>'form-control browser-default custom-select rounded','onChange'=>'ajaxLoad("'.url("admin/actors").'?table_length="+this.value)'])
                        !!}
                    </div>
                </div>
            </div>
            <div class="col-sm-2 input-group">
            <div class="row form-inline mb-5">
                <label class="input-group-text border-0" style="background: none;font-size: 15px;">Hiển thị</label>
                {!! Form::select('human_filter',
                ['actor'=>'actor','director'=>'director'],
                request()->session()->get('human_filter'),
                ['class'=>'form-control browser-default custom-select rounded','onChange'=>'ajaxLoad("'.url("admin/actors").'?human_filter="+this.value)'])
                !!}
            </div>
            </div>
            <div class="col-sm-8 form-group align-right">
                <div class="col-sm-8">
                    <div class="input-group">
                        <input type="hidden" id="xoa" value="" name="xoa">
                        <input class="form-control" id="search" value="{{ request()->session()->get('search') }}"
                            onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('admin/actors')}}?search='+this.value)"
                            placeholder="Nhập tên nhân sự cần tìm" name="search" type="text"/>
                        <div class="input-group-btn ml-1">
                            <button type="submit" class="btn btn-info b-r-xs"
                                onclick="ajaxLoad('{{url('admin/actors')}}?search='+$('#search').val())">Tìm kiếm
                            </button>
                        </div>
                        <div class="input-group-btn ml-1">
                            <button type="submit" class="btn btn-outline-secondary b-r-xs"
                                onclick="ajaxLoad('{{url('admin/actors')}}?search='+$('#xoa').val())"><i class="fas fa-redo-alt"></i>
                                Reload
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (!empty($keyword_s))
            <div class="row">
                <h3 class="pl-3">Tìm kiếm từ khóa : <span class="text-navy">{{ $keyword_s }}</span></h3>
            </div>
        @endif
        <div class="form-group">
            <table class="table table-hover table-responsive-md table-striped">
                <thead class="table-info">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Full name</th>
                        <th scope="col">URL</th>
                        <th scope="col">Công việc</th>
                        <th scope="col">Giới tính</th>
                        <th scope="col">Ngày tạo</th>
                        <th scope="col">Xử lý</th>
                    </tr>
                </thead>
                @if (count($actors_all) >= 1)
                    <tbody>
                        @foreach ($actors_all as $key => $actor)
                            <tr>
                                <td><strong>{{ ++$key }}.</strong></td>
                                <td>{!! ($actor->first_name.' '.$actor->last_name) !!}</td>
                                <td>{!! ($actor->slug) !!}</td>
                                <td>{!! ($actor->job) !!}</td>
                                <td>{!! ($actor->gender == 1) ? 'Nữ' : 'Nam' !!}</td>
                                <td>{!! date('d/m/Y',strtotime(date($actor->created_at))) !!}</td>
                                <td>
                                    <a class='btn btn-primary btn-xs b-r-xs' href="{!! route('actors.show', $actor->slug) !!}"><i class="far fa-eye"></i> Show</a>
                                    <a class='btn btn-success btn-xs b-r-xs' href="{!! route('actors.edit', $actor->slug) !!}"><i class="far fa-edit"></i> Edit</a>
                                    <input type="hidden" name="_method" value="delete" />
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <a class="btn btn-danger btn-xs b-r-xs" data-toggle="modal" href='#modalDelete' data-id="{{$actor->slug}}" data-token="{{csrf_token()}}" ><i class="far fa-trash-alt"></i> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                @else
                    <div class="d-md-flex justify-content-center pt-4">
                        <h3>Dữ liệu không có hoặc không tồn tại</h3>
                    </div>
                @endif
            </table>
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label">Hiển thị từ {{ (count($actors_all) >= 1) ? '1' : '0' }} đến {{ count($actors_all) }} trong tổng số {{ $actors_all->total() }} mục</label>
                </div>
                <div class="col-md-6">
                    <div class="float-right">
                        {!! $actors_all->links("pagination::bootstrap-4") !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
