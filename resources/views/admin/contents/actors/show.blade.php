@extends('admin.layouts.master')
@section('title','Xem thông tin')
@section('page-heading')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2 class="text-uppercase font-weight-bold">Đạo diễn / Diễn viên</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Actors - Directors</strong>
            </li>
            <li class="breadcrumb-item active">
                <strong>show</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="{{ route('actors.create') }}" class="btn btn-primary b-r-xs">Thêm mới <i class="fas fa-plus"></i></a>
            <a href="{{ route('actors.index') }}" class="btn btn-info b-r-xs">Danh sách</a>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="form-group row bg-white pb-4">
    <div class="col-md-12">
        <div class="row">
        <h2 class="text-center font-weight-bold animated fadeInRightBig w-100">Thông tin {{ (($actors->job) == 'actor' ? 'diễn viên' : 'đạo diễn') }} : <span class="text-navy">{{ $actors->first_name.' '.$actors->last_name }}</span></h2>
        </div>
        <div class="row mt-3">
            <div class="col-md-4">
                <img src="{{ asset('vendor/images/'.$actors->avatar) }}" class="img-responsive w-100">
                <label class="font-weight-bold w-100 text-center mt-3">Ảnh đại diện</label>
            </div>
            <div class="col-md-8">
                <label class="font-weight-bold w-100">Full name:  {{ ($actors->first_name.''.$actors->last_name) ? $actors->first_name.' '.$actors->last_name : 'Chưa có' }}</label>
                <label class="font-weight-bold w-100">Công việc : {{ (($actors->job) == 'actor' ? 'Diễn viên' : 'Đạo diễn') ? (($actors->job) == 'actor' ? 'Diễn viên' : 'Đạo diễn') : 'Chưa có' }}</label>
                <label class="font-weight-bold w-100">Giới tính : {{ (($actors->gender) == 1 ? 'Nữ' : 'Nam') ? (($actors->gender) == 1 ? 'Nữ' : 'Nam') : 'Chưa có' }}</label>
                <label class="font-weight-bold w-100">Chiều cao :  {{ ($actors->height). ' cm' ? $actors->height. ' cm' : 'Chưa có' }}</label>
                <label class="font-weight-bold w-100">Cân nặng : {{ ($actors->weight). ' kg' ? $actors->weight. ' kg' : 'Chưa có' }}</label>
                <label class="font-weight-bold w-100">Nhóm máu : {{ ($actors->blood_group) ? $actors->blood_group : 'Chưa có' }}</label>
                <label class="font-weight-bold w-100">Sở thích : {{ ($actors->hobby) ? $actors->hobby : 'Chưa có' }}</label>
                <label class="font-weight-bold w-100">Quốc tịch : {{ ($actors->country) ? $actors->country : 'Chưa có' }}</label>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <a href="{{ route('actors.index') }}" class="btn btn-info float-right b-r-xs ml-3">Quay lại</a>
        <a href="{{ route('actors.edit', $actors->slug) }}" class="btn btn-warning float-right b-r-xs">Sửa thông tin</a>
    </div>
</div>
@endsection


