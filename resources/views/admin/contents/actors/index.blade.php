@extends('admin.layouts.master')
@section('title','Đạo diễn / Diễn Viên')
@section('page-heading')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2 class="text-uppercase font-weight-bold">Đạo diễn / Diễn viên</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Actors - Directors</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="{{ route('actors.create') }}" class="btn btn-primary b-r-xs">Thêm mới <i class="fas fa-plus"></i></a>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="form-group row bg-white pb-4">
    <div class="container-fluid">
        <div class="form-group">
            @include('admin.contents.actors.list')
        </div>
        <div class="loading">
            <i class="fa fa-sync-alt fa-spin fa-2x fa-fw"></i><br/>
            <span>Loading</span>
        </div>
        @include('admin.modals.actors.delete')
    </div>
</div>
@endsection

@section('javascript')
    <script src="{{ asset('vendor/js/ajax-crud.js') }}"></script>
@endsection
