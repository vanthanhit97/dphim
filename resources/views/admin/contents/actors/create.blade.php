@extends('admin.layouts.master')
@section('title','Thêm mới')
@section('page-heading')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2 class="text-uppercase font-weight-bold">Diễn viên / Đạo diễn</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Actors - Director</strong>
            </li>
            <li class="breadcrumb-item active">
                <strong>create</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="{{ route('actors.index') }}" class="btn btn-primary b-r-xs">Danh sách</a>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="form-group row bg-white pb-4">
    <div class="col-md-12 position-alert">
        @include('admin.includes.alert')
    </div>
    <div class="col-md-12">
        {!! Form::open(array('url' => 'admin/actors', 'method' => 'POST','files' => true,'enctype' => 'multipart/form-data')) !!}
        {!! csrf_field() !!}
        <div class="container-fluid mt-5">
            <div class="form-group border-0 shadow">
                <div class="card">
                    <div class="card-header bg-dark text-white text-uppercase font-weight-bold">
                        Thêm mới
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <div class="form-row">
                            <div class="col-md-4">
                            <div class="form-group required">
                                {!! Form::label('first_name', 'Họ lót', ['class' => 'font-weight-bold']) !!}
                                {!! Form::text('first_name', '', ['class' => 'form-control', 'placeholder' => 'Nhập họ lót']) !!}
                                <span class="text-danger">{{ $errors->first('first_name') }}</span>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group required">
                                {!! Form::label('last_name', 'Tên', ['class' => 'font-weight-bold']) !!}
                                {!! Form::text('last_name', '', ['class' => 'form-control', 'placeholder' => 'Nhập tên']) !!}
                                <span class="text-danger">{{ $errors->first('last_name') }}</span>
                            </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                {!! Form::label('job', 'Công việc', ['class' => 'font-weight-bold']) !!}
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="actor" name="job" class="custom-control-input" value="actor" checked>
                                    <label class="custom-control-label" for="actor">Diễn viên</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="director" name="job" class="custom-control-input" value="director">
                                    <label class="custom-control-label" for="director">Đạo diễn</label>
                                </div>
                                <span class="text-danger">{{ $errors->first('job') }}</span>
                                </div>
                            </div>
                            </div>
                            <div class="form-row">
                            <div class="col-md-4">
                            <div class="form-group">
                            {!! Form::label('gender', 'Giới tính', ['class' => 'font-weight-bold']) !!}
                            <div class="custom-control custom-radio">
                                <input type="radio" id="genderMale" name="gender" class="custom-control-input" value="0" checked>
                                <label class="custom-control-label" for="genderMale">Nam</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="genderFemale" name="gender" class="custom-control-input" value="1">
                                <label class="custom-control-label" for="genderFemale">Nữ</label>
                            </div>
                            <span class="text-danger">{{ $errors->first('gender') }}</span>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('height', 'Chiều cao', ['class' => 'font-weight-bold']) !!}
                                <div class="input-group">
                                {!! Form::number('height', '', ['class' => 'form-control', 'placeholder' => 'Nhập chiều cao']) !!}
                                <div class="input-group-append">
                                <span class="input-group-text">cm</span>
                                </div>
                                </div>
                                <span class="text-danger">{{ $errors->first('height') }}</span>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('weight', 'Cân nặng', ['class' => 'font-weight-bold']) !!}
                                <div class="input-group">
                                {!! Form::number('weight', '', ['class' => 'form-control', 'placeholder' => 'Nhập cân nặng']) !!}
                                <div class="input-group-append">
                                <span class="input-group-text">kg</span>
                                </div>
                                </div>
                                <span class="text-danger">{{ $errors->first('weight') }}</span>
                            </div>
                            </div>
                            </div>
                            <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                {!! Form::label('blood_group', 'Nhóm máu', ['class' => 'font-weight-bold']) !!}
                                {!! Form::select('blood_group', ['A' => 'A','B' => 'B','AB' => 'AB','O' => 'O'], '', ['class' => $errors->has('blood_group') ? 'form-control error' :'form-control', 'placeholder' => 'Chọn nhóm máu']) !!}
                                <span class="text-danger">{{ $errors->first('blood_group') }}</span>
                                </div>
                                </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('hobby', 'Sở thích', ['class' => 'font-weight-bold']) !!}
                                {!! Form::text('hobby', '', ['class' => 'form-control', 'placeholder' => 'Nhập sở thích']) !!}
                                <span class="text-danger">{{ $errors->first('hobby') }}</span>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group required">
                                {!! Form::label('country', 'Quốc gia', ['class' => 'font-weight-bold']) !!}
                                {!! Form::text('country', '', ['class' => 'form-control', 'placeholder' => 'Nhập quốc gia']) !!}
                                <span class="text-danger">{{ $errors->first('country') }}</span>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <div class="form-group avatar-upload">
                                {!! Form::label('avatar', 'Ảnh đại diện', ['class' => 'font-weight-bold']) !!}
                                <div class="avatar-edit">
                                {!! Form::file('avatar', ['id' => 'imageUpload' , 'accept' => '.png,.jpg,.jpeg']) !!}
                                <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url( {{ url('vendor/css/images/img-upload.png') }} );">
                                </div>
                                </div>
                                <span class="text-danger">{{ $errors->first('avatar') }}</span>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group mr-4">
                    <a href="{{ route('actors.index') }}" class="btn btn-dark float-right b-r-xs ml-3">Hủy</a>
                    {!! Form::submit('Lưu dữ liệu', ['class' => 'btn btn-primary b-r-xs float-right']) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

