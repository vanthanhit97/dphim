
    <div class="form-group row bg-white pb-4">
        <div class="col-md-12 position-alert">
            @include('admin.includes.alert')
        </div>
        <div class="col-md-12">
            <div class="container mt-4">
    <div class="form-group border-0 shadow" id="content">
        <div class="card">
            <div class="card-header bg-dark text-white text-center font-weight-bold text-uppercase py-3">
                Danh sách comments
            </div>
    <div class="col-md-12">
        <div class="mt-5 mb-5" >
        <div class="row">
            <div class="col-sm-2">
                <div class="row">
                    <div class="col-sm-11 input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text border-0" style="background: none;font-size: 15px;">Hiển thị</label>
                        </div>
                        {!! Form::select('table_length',
                            ['10'=>'10','25'=>'25','50'=>'50','100'=>'100'],
                            request()->session()->get('table_length'),
                            ['class'=>'form-control browser-default custom-select rounded','onChange'=>'ajaxLoad("'.url("admin/comments").'?table_length="+this.value)'])
                        !!}
                    </div>
                </div>
            </div>
            <div class="col-sm-10 form-group align-right">
                <div class="col-sm-9">
                    <div class="input-group">
                        <input type="hidden" id="xoa" value="" name="xoa">
                        <input class="form-control" id="search" value="{{ request()->session()->get('search') }}" 
                            onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('admin/comments')}}?search='+this.value)"
                            placeholder="Nhập tên bình luận cần tìm..." name="search" type="text"/>
                        <div class="input-group-btn ml-1">
                            <button type="submit" class="btn btn-info b-r-xs" 
                                onclick="ajaxLoad('{{url('admin/comments')}}?search='+$('#search').val())">Tìm kiếm
                            </button>
                        </div>
                        <div class="input-group-btn ml-1">
                            <button type="submit" class="btn btn-outline-secondary b-r-xs"
                                onclick="ajaxLoad('{{url('admin/comments')}}?search='+$('#xoa').val())"><i class="fas fa-redo-alt"></i> 
                                Reload
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (!empty($keyword_s))
            <div class="row">
                <h3 class="pl-3">Tìm kiếm từ khóa : <span class="text-navy">{{ $keyword_s }}</span></h3>
            </div>
        @endif
            <div class="card-body">
                <div class="container p-0">
                    <div class="form-group">
                        <table class="table table-hover table-responsive-md table-striped">
                            <thead class="table-primary">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Tên phim</th>
                                    <th scope="col">Tên người dùng</th>
                                    <th scope="col">Nội dung bình luận</th>
                                    <th scope="col" class="text-center" width="140px">Xử lý</th>
                                </tr>
                            </thead>
                            @if (count($comments_all) >= 1)
                            <tbody>
                                @foreach ($comments_all as $key => $comment)
                                    <tr class="{{ (isset($comments) && $comments->id == $comment->id) ? 'bg-info font-weight-bold' : '' }}">
                                        <td><strong>{{ ++$key }}.</strong></td>
                                        <td>{{$comment->film->title }}</td>
                                        <td>{{$comment->user->profile->first_name.' '.$comment->user->profile->last_name }}</td>
                                        <td>{{ $comment->content}}</td>
                                        <td class="text-center" >
                                            @include('admin.modals.comments.delete')
                                            <a href="#Delete-{{$comment->id}}" data-toggle="modal" class="btn btn-danger btn-xs b-r-xs"><i class="far fa-trash-alt"></i> Delete</a>
                                        </td>
                                    </tr>     
                                @endforeach
                            </tbody>
                            @else
                                <div class="d-md-flex justify-content-center pt-4">
                                    <h3>Dữ liệu không có hoặc không tồn tại</h3>
                                </div>
                            @endif
                        </table>
                        <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Hiển thị từ {{ (count($comments_all) >= 1) ? '1' : '0' }} đến {{ count($comments_all) }} trong tổng số {{ $comments_all->total() }} mục</label>
                        </div>
                        <div class="col-md-6">          
                            <div class="float-center">
                                {!! $comments_all->links("pagination::bootstrap-4") !!}
                            </div>        
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
</div>
        



