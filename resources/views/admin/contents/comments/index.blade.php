@extends('admin.layouts.master')
@section('title','Quản lý bình luận')
@section('page-heading')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2 class="text-uppercase font-weight-bold">Quản lý comment</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>comments</strong>
            </li>
        </ol>
    </div>
</div>
@endsection
@section('content')
<div class="form-group row bg-white pb-4">
    <div class="container-fluid">
        <div class="form-group">
            @include('admin.contents.comments.list')
        </div>
        <div class="loading">
            <i class="fa fa-sync-alt fa-spin fa-2x fa-fw"></i><br/>
            <span>Loading</span>
        </div>
        
    </div>
</div>
@endsection

@section('javascript')
    <script src="{{ asset('vendor/js/ajax-crud.js') }}"></script>
@endsection