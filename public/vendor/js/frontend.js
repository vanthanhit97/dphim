let id = 0;
$('#carouselExampleCaptions').on('slide.bs.carousel', function (ev) {
    id = $(this).find('.active').index();
    changeEffect();
});

let arrEffect = ['fadeIn', 'zoomIn','slideInDown', 'slideInUp', 'pulse']

function changeEffect() {
    let slider1 = document.getElementById("img1");
    let slider2 = document.getElementById("img2");
    let slider3 = document.getElementById("img3");
    let arrSlider = [slider1, slider2, slider3];

    for (let j = 0; j < arrSlider.length; j++) {
        if (id == j) {
            continue;
        }
        for (let i = 0; i < arrEffect.length; i++) {
            if (arrSlider[j].classList.contains(arrEffect[i])) {
                arrSlider[j].classList.remove(arrEffect[i]);
                let number = getRndInteger(0, arrEffect.length - 1, i);
                arrSlider[j].classList.add(arrEffect[number]);
                break;
            }
        }
    }
}

function getRndInteger(min, max, i) {
    let random = Math.floor(Math.random() * (max - min + 1)) + min;
    while (random === i) {
        random = Math.floor(Math.random() * (max - min + 1)) + min;
    }
    return random;
}

$(document).ready(function () {

    $(function () {

        $(document).on('scroll', function () {

            if ($(window).scrollTop() > 100) {
                $('.scroll-top-wrapper').addClass('show');
            } else {
                $('.scroll-top-wrapper').removeClass('show');
            }
        });

        $('.scroll-top-wrapper').on('click', scrollToTop);
    });

    function scrollToTop() {
        verticalOffset = typeof (verticalOffset) != 'undefined' ? verticalOffset : 0;
        element = $('body');
        offset = element.offset();
        offsetTop = offset.top;
        $('html, body').animate({ scrollTop: offsetTop }, 500, 'linear');
    }

    $("#notification-alert").fadeTo(3000, 500).slideUp(500, function () {
        $("#notification-alert").slideUp(600);
    });
});
