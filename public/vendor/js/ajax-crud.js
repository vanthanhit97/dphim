$(document).on('click', 'a.page-link', function (event) {
    event.preventDefault();
    ajaxLoad($(this).attr('href'));
});

function ajaxLoad (filename, content) {
    content = typeof content !== 'undefined' ? content : 'content';
    $('.loading').show();
    $.ajax ({
        type: 'GET',
        url: filename,
        contentType: false,
        success: function (data) {
            $('#'+content).html (data);
            $('.loading').hide();
        },
        error: function (xhr, status, error) {
            alert (xhr.responseText);
        },
    });
}

function ajaxDelete(filename, token, content) {
    content = typeof content !== 'undefined' ? content : 'content';
    $('.loading').show();
    $.ajax({
        type: 'POST',
        data: { _method: 'DELETE', _token: token },
        url: filename,
        success: function (data) {
            $('#modalDelete').modal('hide');
            $("#" + content).html(data);
            $('.loading').hide();
            $("#notification-alert").fadeTo(3000, 500).slideUp(500, function () {
                $("#notification-alert").slideUp(600);
            });
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
}

$('#modalDelete').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    $('#delete_id').val(button.data('id'));
    $('#delete_token').val(button.data('token'));
});
