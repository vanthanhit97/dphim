<?php
    if(empty($_FILES['file'])) {
        exit();
    }
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $sever = ($_SERVER["SERVER_NAME"] === 'localhost') ? $_SERVER["SERVER_NAME"].'/dphim' : $_SERVER["SERVER_NAME"];
    $url = $protocol . $sever;
    $errorImage = $url.'/public/storage/images/img_upload_error.jpg';
    $temp = explode(".",$_FILES["file"]["name"]);
    $newfilename = round(microtime(true)).'.'.end($temp);
    if (file_exists('storage/images/img_upload_error.jpg')) {
        $newfilename = round(microtime(true)).'.'.end($temp);
    }
    if(!move_uploaded_file($_FILES['file']['tmp_name'], '../storage/images/' . $newfilename)) {
        echo $errorImage;
    } else {
        echo $fileImage = $url . "/public/storage/images/" . $newfilename;
    }
       
?>