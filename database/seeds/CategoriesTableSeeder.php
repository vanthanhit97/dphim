<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();

        $data = [
            [
                'name' => 'Thể loại',
                'slug' => 'the-loai',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ],
            [
                'name' => 'Quốc gia',
                'slug' => 'quoc-gia',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ],
            [
                'name' => 'Phim lẻ',
                'slug' => 'phim-le',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ],
            [
                'name' => 'Phim bộ',
                'slug' => 'phim-bo',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ],
            [
                'name' => 'Phim chiếu rap',
                'slug' => 'phim-chieu-rap',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ],
            [
                'name' => 'Phim thuyết minh',
                'slug' => 'phim-thuyet-minh',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]
        ];

        $dataChild = [
            [
               'name' => 'Phim hành động',
               'slug' => 'phim-hanh-dong',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim phiêu lưu',
               'slug' => 'phim-phieu-luu',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim khoa hoc viễn tưởng',
               'slug' => 'phim-khoa-hoc-vien-tuong',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim thần thoại',
               'slug' => 'phim-than-thoai',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim võ thuật',
               'slug' => 'phim-vo-thuat',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim kinh dị',
               'slug' => 'phim-kinh-di',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim tâm lý',
               'slug' => 'phim-tam-ly',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim hình sự',
               'slug' => 'phim-hinh-su',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim cổ trang',
               'slug' => 'phim-co-trang',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim hài hước',
               'slug' => 'phim-hai-huoc',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim gia đình',
               'slug' => 'phim-gia-dinh',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim hoạt hinh',
               'slug' => 'phim-hoat-hinh',
               'parent_id' => 1,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],

            [
               'name' => 'Mỹ',
               'slug' => 'my',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Việt Nam',
               'slug' => 'viet-nam',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Pháp',
               'slug' => 'phap',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Hàn Quốc',
               'slug' => 'han-quoc',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Nhật Bản',
               'slug' => 'nhat-ban',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Anh',
               'slug' => 'anh',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Trung Quốc',
               'slug' => 'trung-quoc',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Ấn Độ',
               'slug' => 'an-do',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Hồng Kong',
               'slug' => 'hong-kong',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Thái Lan',
               'slug' => 'thai-lan',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Khác',
               'slug' => 'khac',
               'parent_id' => 2,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],

            [
               'name' => '2019',
               'slug' => '2019',
               'parent_id' => 3,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => '2018',
               'slug' => '2018',
               'parent_id' => 3,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => '2017',
               'slug' => '2017',
               'parent_id' => 3,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => '2016',
               'slug' => '2016',
               'parent_id' => 3,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => '2015',
               'slug' => '2015',
               'parent_id' => 3,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => '2014',
               'slug' => '2014',
               'parent_id' => 3,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => '2013',
               'slug' => '2013',
               'parent_id' => 3,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'trước 2013',
               'slug' => 'truoc-2013',
               'parent_id' => 3,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],

            [
               'name' => 'Phim bộ Hàn Quốc',
               'slug' => 'phim-bo-han-quoc',
               'parent_id' => 4,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim bộ Hồng Kong',
               'slug' => 'phim-bo-hong-kong',
               'parent_id' => 4,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim bộ Trung Quốc',
               'slug' => 'phim-bo-trung-quoc',
               'parent_id' => 4,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim bộ Đài Loan',
               'slug' => 'phim-bo-dai-loan',
               'parent_id' => 4,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim bộ Thái Lan',
               'slug' => 'phim-bo-thai-lan',
               'parent_id' => 4,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
            [
               'name' => 'Phim bộ Mỹ',
               'slug' => 'phim-bo-my',
               'parent_id' => 4,
               'created_at' => new DateTime,
               'updated_at' => new DateTime,
            ],
        ];

        Category::insert($data);
        Category::insert($dataChild);
        // factory(Category::class, 'parentCategory')->times(8)->create();

        // factory(Category::class, 'category')->times(20)->create();
    }
}
