<?php

namespace App\Events;

use App\Models\Film;
use Jenssegers\Agent\Agent;
use Illuminate\Session\Store;
use App\Repositories\Tracker\TrackerRepositoryInterface;

class ViewedFilmHandler
{

    private $session;
    protected $trackerRepository;

    public function __construct(Store $session, TrackerRepositoryInterface $trackerRepository)
    {
        $this->session = $session;
        $this->trackerRepository = $trackerRepository;
    }

    public function handle(Film $film)
    {
        if (!$this->isFilmViewed($film)) {
            $film->increment('viewed');
            $this->storeFilm($film);
            $agent = new Agent();

            (\Request::ip() == '::1') ? $ip = '127.0.0.1' : $ip = \Request::ip();
            $device = '';
            if ($agent->is('Windows')) {
                $device = 'Máy tính';
            } else if ($agent->is('iPhone')) {
                $device = 'Điện thoại';
            } else {
                $device = 'Máy tính bảng';
            }

            $browser = $agent->browser();

            $data = [
                'ip_add' => $ip,
                'device' => $device,
                'browser_name' => $browser,
                'browser_vesion' => $agent->version($browser),
                'browser_platform' => $agent->platform(),
                'film_id' => $film->id
            ];

            $this->trackerRepository->create($data);
        }
    }

    private function isFilmViewed($film) {
        $viewed = $this->session->get('viewed_films', []);
        return array_key_exists($film->id, $viewed);
    }

    private function storeFilm($film) {
        $key = 'viewed_films.'.$film->id;
        $this->session->put($key, time());
    }
}
