<?php

namespace App\Http\Requests\V1\Admin\Trailer;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'film_id' => 'required',
            'link' => 'required|max:191|unique:trailers',
        ];
    }

    public function messages()
    {
        return [
            'film_id.required' => 'Vui lòng chọn tên phim',
            'link.required' => 'Vui lòng nhập đường dẫn !',
            'link.max' => 'Đường dẫn quá dài ! (tối đa 191 ký tự).',
            'link.unique' => 'Đường dẫn đã tồn tại.',
        ];
    }
}
