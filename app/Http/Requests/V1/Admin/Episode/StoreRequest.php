<?php

namespace App\Http\Requests\V1\Admin\Episode;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'film_id' => 'required',
            'episode' => 'required|max:191',
            'link' => 'required|max:191|unique:episodes',
                   
        ];
    }

    public function messages()
    {
        return [
            'film_id.required' => 'Vui lòng chọn tên phim !',
            'episode.max' => 'Tên tập phim quá dài ! (tối đa 191 ký tự).',
            'episode.required' => 'Vui lòng nhập tập phim.',
            'link.required' => 'Bạn chưa nhập đường dẫn tập phim.',
            'link.max' => 'Đường dẫn phim quá dài ! (tối đa 191 ký tự).',
            'link.unique' =>  'Đường dẫn tập phim  đã tồn tại',
           
        ];
    }
}
