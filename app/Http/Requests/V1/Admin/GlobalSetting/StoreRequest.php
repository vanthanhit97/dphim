<?php

namespace App\Http\Requests\V1\Admin\GlobalSetting;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        return [
            'name' => 'required',
            'phone' => 'nullable|numeric|min:6',
            'email' => 'nullable|email',
            'facebook' => 'nullable|regex:'.$regex,
            'youtube' => 'nullable|regex:'.$regex,
            'google' => 'nullable|regex:'.$regex,
            'address' => 'nullable',
            'copyright' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên website hoặc tên công ty',
            'phone.numeric' => 'Số điện thoại phải là số',
            'phone.min' => 'Phải nhập ít nhất 6 số',
            'email.email' => 'E-mail không hợp lệ',
            'facebook.regex' => 'Link không hợp lệ',
            'google.regex' => 'Link không hợp lệ',
            'youtube.regex' => 'Link không hợp lệ',
            'copyright.required' => 'Vui lòng nhập nội dung copyright'
        ];
    }
}
