<?php

namespace App\Http\Requests\V1\Admin\Actor;
use App\Models\Actor;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $actor = Actor::find($this->actor);
        return [
            //
            'first_name'  => 'required',
            'last_name'   => 'required',
            'country'     => 'required',
            'job'         => 'required',
        ];
    }
    public function message()
    {
        return [
            'first_name.required'  => 'Bạn chưa nhập tên đệm',
            'last_name.required'   => 'Bạn chưa nhập tên',
            'country.required'     => 'Bạn chưa nhập quốc tịch',
            'job.required'         => 'Bạn chưa chọn công việc'
        ];
    }
}
