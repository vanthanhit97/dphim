<?php

namespace App\Http\Requests\V1\Admin\Film;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Film;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $film = Film::findBySlugOrFail($this->film);
        return [
            'title' => 'required|min:3|max:191|unique:films,title,' . $film->id . ',id',
            'description' => 'required|min:3',
            'content' => 'required',
            'actors' => 'required',
            'director_id' => 'required',
            'categories' => 'required',
            'tags' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'cover_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Tiêu đề không được để trống',
            'title.min' => 'Tiêu đề quá ngắn ( tối thiểu 3 kí tự )',
            'title.max' => 'Tiêu đề quá dài ( tối đa 191 kí tự )',
            'title.unique' => 'Tiêu đề phim này đã tồn tại',
            'description.required' => 'Mô tả không được để trống',
            'description.min' => 'Mô tả quá ngắn ( tối thiểu 3 kí tự )',
            'content.required' => 'Nội dung không được để trống',
            'actors.required' => 'Bạn chưa chọn diễn viên cho phim',
            'categories.required' => 'Bạn chưa chọn danh mục cho phim',
            'tags.required' => 'Bạn chưa chon tags cho phim',
            'avatar.image' => 'Chỉ cho phép upload ảnh',
            'avatar.mimes' => 'Chỉ chấp nhận cái file ảnh đuôi : jpeg,png,jpg,gif,svg',
            'avatar.max' => 'Dung lượng ảnh quá lớn ( tối đa 2048 kb)',
            'cover_image.mimes' => 'Chỉ chấp nhận cái file ảnh đuôi : jpeg,png,jpg,gif,svg',
            'cover_image.max' => 'Dung lượng ảnh quá lớn ( tối đa 2048 kb)',
            'director_id.required' => 'Bạn chưa chọn đạo diễn cho phim'
        ];
    }
}
