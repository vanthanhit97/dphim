<?php

namespace App\Http\Requests\V1\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'               => 'required|max:150|unique:users',
            'email'              => 'required|email',
            'password'           => 'required|min:6|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'name.required'      => 'Bạn chưa nhập tên',
            'name.max'           => 'Bạn chỉ đượcn nhập tối đa 150 kí tự',
            'name.unique' => 'Tên tài khoản đã tồn tại',
            'email.required'     => 'Bạn chưa nhập email',
            'email.email'        => 'Bạn nhập không đúng định dạng email',
            'password.required'  => 'Bạn chưa nhập mật khẩu',
            'password.confirmed' => 'Mật khẩu xác nhận không khớp',
        ];
    }
}
