<?php

namespace App\Http\Requests\V1\Admin\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $category = User::findBySlugOrFail($this->user);
        return [
            'name' => 'required|min:4|max:191|unique:categories,name,' . $category->id . ',id',
            'email' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'date_of_birth' => 'required',
            'gender'    => 'required',           
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên danh mục !',
            'name.min' => 'Tên tag quá ngắn ! (tối thiểu 4 ký tự).',
            'name.max' => 'Tên danh mục quá dài ! (tối đa 191 ký tự).',
            'name.unique' => 'Tên chuyên mục đã tồn tại.',
            'email.required' => 'Bạn chưa điền email.',
            'first_name.required' =>  'Bạn chưa điền họ.',
            'last_name.required'    => 'Bạn chưa điền tên.',
            'phone.required'    => 'Bạn chưa nhập số điện thoại.',
            'date_of_birth.required'  => 'Bạn chưa nhập ngày sinh.',
            'gender.required' => 'Bạn chưa chọn giới tính.',
            'avatar.image' => 'Chỉ cho phép upload ảnh',
            'avatar.mimes' => 'Chỉ chấp nhận cái file ảnh đuôi : jpeg,png,jpg,gif,svg',
            'avatar.max' => 'Dung lượng ảnh quá lớn ( tối đa 2048 kb)',
        ];
    }
}
