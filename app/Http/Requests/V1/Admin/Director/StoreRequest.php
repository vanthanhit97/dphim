<?php

namespace App\Http\Requests\V1\Admin\Director;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        'first_name' => 'required',
        'last_name'  => 'required',
        'height'     => 'required',
        'weight'     => 'required',
        'hobby'      => 'required',
        'country'    => 'required',
        'avatar'     => 'required'
        ];
    }
    public function message()
    {
        return [
        'first_name.required' => 'Bạn chưa nhập tên đệm',
        'last_name.required'  => 'Bạn chưa nhập tên',
        'height.required'     => 'Bạn chưa nhập chiều cao',
        'weight.required'     => 'Bạn chưa nhập cân nặng',
        'hobby.required'      => 'Bạn chưa nhập sở thích',
        'country.required'    => 'Bạn chưa nhập quốc tịch',
        'avatar.required'     => 'Bạn chưa đặt ảnh đại diện'
        ];
    }
}
