<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Film\FilmRepositoryInterface;
use App\Repositories\Comment\CommentRepositoryInterface;
use Exception;
class CommentController extends Controller
{
    protected $userRepository;
    protected $filmRepository;
    protected $commentRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        FilmRepositoryInterface $filmRepository,
        CommentRepositoryInterface $commentRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->data['users_all'] = $this->userRepository->pluck('name','slug');
        $this->filmRepository = $filmRepository;
        $this->data['films_all'] = $this->filmRepository->pluck('title','id');
        $this->commentRepository = $commentRepository;    
        $this->data['comments_all'] = $this->commentRepository->orderBy('created_at', 'DESC');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('table_length', $request->has('table_length') ? $request->get('table_length') : ($request->session()->has('table_length') ? $request->session()->get('table_length') : 10));
        $this->data['comments_all'] = $this->commentRepository->paginateSearch('content', $request->session()->get('search'), 'created_at', 'DESC',  $request->session()->get('table_length'));
        $this->data['keyword_s'] = $request->session()->get('search');
        $request->session()->forget('search');
        if ($request->ajax()) {
            return view('admin.contents.comments.list', $this->data);
        } else {
            return view('admin.contents.comments.index', $this->data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            $comments = $this->commentRepository->find($id);
            $this->commentRepository->destroy($comments->id);
            \DB::commit();
            return redirect()->route('comments.index')->with('status_p', 'Đã xóa thành công !');
        } catch (Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể xóa. Đã có lỗi xảy ra.');
        }
    }
}
