<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Tag\TagRepositoryInterface;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Actor\ActorRepositoryInterface;
use App\Repositories\Director\DirectorRepositoryInterface;
use App\Repositories\Film\FilmRepositoryInterface;
use App\Http\Requests\V1\Admin\Film\StoreRequest;
use App\Http\Requests\V1\Admin\Film\UpdateRequest;
use App\Models\Director;
use App\Models\Actor;
use phpDocumentor\Reflection\Types\This;
use App\Models\Episode;

class FilmController extends Controller
{
    protected $tagRepository, $categoryRepository, $filmRepository, $actorRepository, $directorRepository;

    public function __construct(TagRepositoryInterface $tagRepository, CategoryRepositoryInterface $categoryRepository, FilmRepositoryInterface $filmRepository,
    ActorRepositoryInterface $actorRepository, DirectorRepositoryInterface $directorRepository)
    {
        $this->tagRepository = $tagRepository;
        $this->categoryRepository = $categoryRepository;
        $this->filmRepository = $filmRepository;
        $this->actorRepository = $actorRepository;
        $this->directorRepository = $directorRepository;
        $this->data['directors'] = $this->directorRepository->selectAs('directors');
        $this->data['actors'] = $this->actorRepository->selectAs('actors');
        $this->data['tags'] = $this->tagRepository->orderBy('name', 'ASC')->where('status', 1)->pluck('name', 'id');
        $this->data['categories'] = $this->categoryRepository->whereNull('parent_id', 'name')->where('status', '1');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->data['f_kind'] = $this->categoryRepository->where('slug', 'the-loai')->pluck('id');
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('table_length', $request->has('table_length') ? $request->get('table_length') : ($request->session()->has('table_length') ? $request->session()->get('table_length') : 10));

        $this->data['films_all'] = $this->filmRepository->paginateSearch('title', $request->session()->get('search'), 'created_at', 'DESC',  $request->session()->get('table_length'));
        $this->data['keyword_s'] = $request->session()->get('search');
        $request->session()->forget('search');
        if ($request->ajax()) {
            return view('admin.contents.films.list', $this->data);
        } else {
            return view('admin.contents.films.index', $this->data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contents.films.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        \DB::beginTransaction();
        try {
            $data = $request->only([
                'title',
                'title_eng',
                'description',
                'content',
                'language',
                'distributor',
                'release_date',
                'run_time',
                'quality',
                'resolution',
                'director_id',
            ]);
            $data['avatar'] = $this->filmRepository->uploadImage($request->avatar);
            $data['cover_image'] = $this->filmRepository->uploadImage($request->cover_image);
            
            $result = $this->filmRepository->createManyToMany($data, $request->categories, $request->tags, $request->actors);
            if($result) {
                $this->filmRepository->moveImage($request->avatar, $data['avatar']);
                $this->filmRepository->moveImage($request->cover_image, $data['cover_image']);
            }
            \DB::commit();
            return redirect()->back()->with('status_s', 'Đã lưu dữ liệu thành công');
        } catch (\Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể thực hiện ! Đã có lỗi xảy ra.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        try {
            $this->data['films'] = $this->filmRepository->findBySlugOrFail($slug);
            $this->data['f_country'] = $this->categoryRepository->where('slug', 'quoc-gia')->pluck('id');
            $this->data['f_year'] = $this->categoryRepository->where('slug', 'phim-le')->pluck('id');
            $this->data['f_kind'] = $this->categoryRepository->where('slug', 'the-loai')->pluck('id');
            return view('admin.contents.films.show', $this->data);
        } catch (\Throwable $th) {
            return view('admin.errors.404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        try {
            $this->data['films'] = $this->filmRepository->findBySlugOrFail($slug);
            return view('admin.contents.films.edit', $this->data);
        } catch (\Throwable $th) {
            return view('admin.errors.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $slug)
    {
        \DB::beginTransaction();
        try {
            $film = $this->filmRepository->findBySlugOrFail($slug);
            $data = $request->only([
                'title',
                'title_eng',
                'description',
                'content',
                'language',
                'distributor',
                'release_date',
                'run_time',
                'quality',
                'resolution',
                'director_id',
            ]);
            $data['slug'] = str_slug($request->title);
            if($request->avatar) {
                $data['avatar'] = $this->filmRepository->uploadImage($request->avatar);
            }
            if ($request->cover_image) {
                $data['cover_image'] = $this->filmRepository->uploadImage($request->cover_image);
            }
            // dd($data);
            $result = $this->filmRepository->updateManyToMany($film, $data, $request->categories, $request->tags, $request->actors);
            if($request->avatar && $result) {
                $this->filmRepository->moveImage($request->avatar, $data['avatar']);
            }
            if($request->cover_image && $result) {
                $this->filmRepository->moveImage($request->cover_image, $data['cover_image']);
            }
            \DB::commit();
            return redirect()->route('films.index')->with('status_s', 'Đã cập nhật thành công !');
        } catch (\Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể thực hiện ! Đã có lỗi xảy ra.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        \DB::beginTransaction();
        try {
            $films = $this->filmRepository->findBySlugOrFail($slug);
            if (file_exists("vendor/images/".$films->avatar)) {
                unlink("vendor/images/".$films->avatar);
            }
            if (file_exists("vendor/images/".$films->cover_image)) {
                unlink("vendor/images/".$films->cover_image);
            }
            $films->categories()->detach();
            $films->actors()->detach();
            $films->tags()->detach();
            $this->filmRepository->destroy($films->id);
            \DB::commit();
            return redirect()->route('films.index')->with('status_p', 'Đã xóa thành công !');
        } catch (\Throwable $th) {
            \DB::rollback();
            return redirect()->route('films.index')->with('status_d', 'Không thể thực hiện ! Đã có lỗi xảy ra.');
        }
    }

    public function watchFilm($slug)
    {
        $films = $this->filmRepository->findBySlugOrFail($slug);
        $episodes = $films->episodes->pluck('link', 'episode');
        return response()->json($episodes);
    }
}
