<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class MediaController extends Controller
{
    private $media_path;
    public $category;
    public function __construct()
    {
        $this->media_path = public_path('vendor/images');
        $this->category = Category::where('parent_id', 0)->get();
    }

    public function index(Request $request)
    {
        session()->forget( 'notification');
        $medias = array();
        if (is_dir($this->media_path) && file_exists($this->media_path)) {
            if ($file = opendir($this->media_path)) {
                while (($title = readdir($file)) !== false) {
                    if (preg_match('/([a-zA-Z0-9\.\-\_\\s\(\)]+)\.([a-zA-Z0-9]+)$/', $title, $m)) {
                        array_push($medias, $title);
                    }
                }
                sort($medias);    
            }
        } else {
            \Session::flash('notification', 'Thư mục hình ảnh hiện tại trống');
        }
        if ($request->ajax()) {
            return view('admin.contents.medias.list', compact('medias'));
        } else {
            return view('admin.contents.medias.index', compact('medias'));
        }
    }

    public function destroy($id)
    {
        $file = file_exists($this->media_path.'/'.$id);
        if ($file) {
            unlink($this->media_path.'/'.$id);
            return redirect()->route('mediashow')->with('status_p', 'Đã xóa thành công !');
        } else {
            return redirect()->back()->with('status_d', 'Không thể xóa, đã xảy ra lỗi');
        }
    }

    public function deleteManyImage(Request $request)
    {
        if ($request->medias) {
            $key = 0;
            $medias = $request->only('medias');
            $countArr = count($medias['medias']);
            while ($key < $countArr) {
                if (file_exists($this->media_path.'/'.$medias['medias'][$key])) {
                    unlink($this->media_path.'/'.$medias['medias'][$key]);
                }
                $key++;
            }
            return redirect()->route('mediashow')->with('status_p', 'Đã xóa thành công !');
        }
        else {
            return redirect()->route('mediashow')->with('status_d', 'Lỗi ! Bạn chưa đánh dấu ảnh');
        }
    }
}
