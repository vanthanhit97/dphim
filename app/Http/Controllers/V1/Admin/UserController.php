<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Profile\ProfileRepositoryInterface;
use App\Http\Requests\V1\Admin\User\UpdateRequest;
use App\Models\Profile;
use Exception;

class UserController extends Controller
{
    protected $profileRepository;
    protected $userRepository;

    public function __construct(
        ProfileRepositoryInterface $profileRepository,
       UserRepositoryInterface $userRepository
    ) {
        $this->profileRepository = $profileRepository;
        $this->data['profiles_all'] = $this->profileRepository->all();
        $this->userRepository = $userRepository;
        $this->data['users_all'] = $this->userRepository->orderBy('slug', 'asc');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.contents.users.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        try {
            
            $this->data['users'] = $this->userRepository->findBySlugOrFail($slug);
            // dd($this->data);
            return view('admin.contents.users.show', $this->data);
            
        } catch (\Throwable $th) {
            return view('admin.errors.404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $this->data['users'] = $this->userRepository->findBySlugOrFail($slug);
        try {   
            return view('admin.contents.users.edit', $this->data);
        } catch (Exception $exception) {
            return view('admin.errors.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $slug)
    {
        
        \DB::beginTransaction();
        try {
            $this->data['users'] = $this->userRepository->findBySlugOrFail($slug);
            $data = [
                'name' => $request['name'],
                'slug' => str_slug($request['name']),
                'email' => $request['email'],
                // 'status' => $this->userRepository->checkStatus($request['status']),
            ];
            $data2 = [
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'address'   => $request['address'],
                'phone'     => $request['phone'],
                'date_of_birth'=> $request['date_of_birth'],
                'gender'    => $this->profileRepository->checkGender($request['gender']),
                'country'   => $request['country'],
            ];
            if($request->avatar){
                $data2['avatar'] = $this->profileRepository->uploadImage($request->avatar);
            }
            if($this->data['users']->profile) {
                $result = $this->data['users']->profile()->update($data2);
            } else {
                $result = $this->data['users']->profile()->create($data2);
            }
            if($request->avatar && $result) {
                $this->userRepository->moveImage($request->avatar, $data2['avatar']);
            }
            $this->data['users']->update($data);
            \DB::commit();
            return redirect()->route('users.index')->with('status_s', 'Đã cập nhật thành công !');
        } catch (\Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể thực hiện ! Đã có lỗi xảy ra.');
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        \DB::beginTransaction();
        try {
            $users = $this->userRepository->findBySlugOrFail($slug);
            if( $users->profile) {
                $users->profile->delete();
            }
            $this->userRepository->destroy($users->id);
            \DB::commit();
            return redirect()->route('users.index')->with('status_p', 'Đã xóa thành công !');
        } catch (Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể xóa. Đã có lỗi xảy ra.');
        }
    }
}
