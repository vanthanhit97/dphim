<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Film;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    protected $categories, $viewed, $users, $films;

    public function __construct()
    {
        $this->categories = $this->countCategories();
        $this->viewed = $this->countViewed();
        $this->users = $this->countUser();
        $this->films = $this->countFilm();
    }

    public function index()
    {
        $this->data['categories_total'] = $this->categories;
        $this->data['viewed_total'] = $this->viewed;
        $this->data['users_total'] = $this->users;
        $this->data['films_total'] = $this->films;
        return view('admin.contents.dashboards.index', $this->data);
    }

    public function countCategories()
    {
        $cat_total = Category::all()->count();
        $parent_cats = Category::where('parent_id', null)->count();
        $child_cats = $cat_total - $parent_cats;
        return [$parent_cats, $child_cats];
    }

    public function countViewed()
    {
        $view_total = Film::all()->sum('viewed');
        return $view_total;
    }

    public function countUser()
    {
        $user_total = User::all()->count();
        return $user_total;
    }

    public function countFilm()
    {
        $film_total = Film::all()->count();
        return $film_total;
    }

    //top 5 phim duoc xem nhieu nhat
    public function mostFilmView()
    {
        $most_viewed = DB::table('films')->orderBy('viewed', 'DESC')->select('viewed', 'title')->take(5)->get();
        return response()->json($most_viewed);
    }

    //Thong ke phim theo thang
    public function viewedFilmByMonth()
    {
        $filmsByMonth = DB::table('films')
            ->select(
                DB::raw('MONTHNAME(created_at) as month'),
                DB::raw("DATE_FORMAT(created_at,'%m/%Y') as monthNum"),
                DB::raw('count(*) as films')
            )
            ->groupBy('monthNum')
            ->get();
        return response()->json($filmsByMonth);
    }

    //Thong ke truy cap theo trinh duyet
    public function reportViewedByBrowser()
    {
        $view_browser = DB::table('films')
            ->join('trackers', 'films.id', '=', 'trackers.film_id')
            ->select(
                'browser_name',
                DB::raw('count(*) as films')
            )
            ->groupBy('browser_name')
            ->get();
        return response()->json($view_browser);
    }

    //Thong ke truy cap theo thiet bi
    public function reportViewedByDevice()
    {
        $view_devicer = DB::table('films')
            ->join('trackers', 'films.id', '=', 'trackers.film_id')
            ->select(
                'device',
                DB::raw('count(*) as films')
            )
            ->groupBy('device')
            ->get();
        return response()->json($view_devicer);
    }

    //Thong ke chi tiet tung thang duoc chon
    public function viewedFilmInMonth(Request $request)
    {
        $from_day = $request->input('from_date');
        $to_day =  $request->input('to_date');

        $viewed_in_month = DB::table('trackers')
            ->select(
                DB::raw('DATE_FORMAT(created_at, "%d/%m/%Y") as day'),
                DB::raw('count(*) as total_viewed')
            )
            ->whereDate('created_at', '>=', $from_day)
            ->whereDate('created_at', '<=', $to_day)
            ->orderBy('created_at')
            ->groupBy(DB::raw('day(created_at)'))
            ->get();
        return response()->json($viewed_in_month);
    }
}
