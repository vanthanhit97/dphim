<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Trailer\TrailerRepositoryInterface;
use App\Repositories\Film\FilmRepositoryInterface;
use App\Http\Requests\V1\Admin\Trailer\StoreRequest;
use App\Http\Requests\V1\Admin\Trailer\UpdateRequest;
use Exception;
use Cohensive\Embed\Facades\Embed;

class TrailerController extends Controller
{
    protected $trailerRepository;
    protected $filmRepository;

    public function __construct(
        TrailerRepositoryInterface $trailerRepository,
        FilmRepositoryInterface $filmRepository
    )
    {
        $this->trailerRepository = $trailerRepository;
        $this->filmRepository = $filmRepository;
        $this->data['trailers_all'] = $this->trailerRepository->orderBy('created_at', 'DESC');
        $this->data['films_all'] = $this->filmRepository->pluck('title','id');
    }
    
      

        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('table_length', $request->has('table_length') ? $request->get('table_length') : ($request->session()->has('table_length') ? $request->session()->get('table_length') : 10));

        $this->data['trailers_all'] = $this->trailerRepository->paginateSearch('link', $request->session()->get('search'), 'created_at', 'DESC',  $request->session()->get('table_length'));
        $this->data['keyword_s'] = $request->session()->get('search');
        // dd($this->data['trailers_all']);
        $request->session()->forget('search');
        
        if ($request->ajax()) {
            
            return view('admin.contents.trailers.table', $this->data);
        } else {
            return view('admin.contents.trailers.create-index', $this->data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        \DB::beginTransaction();
        try {  
            $data = $request->only([
                'film_id',
                'link'
            ]);
            $this->trailerRepository->create($data);
            \DB::commit();
            return redirect()->back()->with('status_s', 'Đã lưu dữ liệu thành công');
        } catch (\Throwable $th) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể thực hiện ! Đã có lỗi xảy ra.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        try {
            $this->data['trailers'] = $this->trailerRepository->find($id);
            return view('admin.contents.trailers.edit', $this->data);
        } catch (Exception $exception) {
            return view('admin.errors.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        \DB::beginTransaction();
        try {
            $this->trailerRepository->find($id);
            $data = [
                'film_id' => $request['film_id'],
                'link' => $request['link'],
            ];
            $this->trailerRepository->update($data, $id);
            \DB::commit();
            return redirect()->route('trailers.index')->with('status_s', 'Đã cập nhật thành công !');
        } catch (Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể cập nhật ! Đã có lỗi xảy ra.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            $trailers = $this->trailerRepository->find($id);
            $this->trailerRepository->destroy($trailers->id);
            \DB::commit();
            return redirect()->route('trailers.index')->with('status_p', 'Đã xóa thành công !');
        } catch (Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể xóa. Đã có lỗi xảy ra.');
        }
    }
}
