<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Admin\GlobalSetting\StoreRequest;
use App\Http\Requests\V1\Admin\GlobalSetting\UpdateRequest;
use App\Repositories\GlobalSetting\GlobalSettingRepositoryInterface;

class GlobalSettingController extends Controller
{

    protected $globalSettingRepository;

    public function __construct(GlobalSettingRepositoryInterface $globalSettingRepository)
    {
        $this->globalSettingRepository = $globalSettingRepository;
        $this->data['globalSetting'] = $this->globalSettingRepository->all()->first();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.contents.globalsettings.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            if(count($this->globalSettingRepository->all()) >= 1) {
                return redirect()->route( 'global-setting.index')->with('status_w', 'Đã tồn tại thông tin cho website');
            } else {
                $data['company_name'] = $request['name'];
                $data['company_phone'] = $request['phone'];
                $data['company_email'] = $request['email'];
                $data['company_facebook'] = $request['facebook'];
                $data['company_youtube'] = $request['youtube'];
                $data['company_googleplus'] = $request['google'];
                $data['company_address'] = $request['address'];
                $data['company_copyright'] = $request['copyright'];
                $this->globalSettingRepository->create($data);
                return redirect()->back()->with('status_s', 'Đã lưu thông tin cho website');  
            }
        } catch(\Throwable $th) {
            return redirect()->route('global-setting.index')->with('status_d', 'Đã có lỗi xảy ra, không thể lưu.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        try {
            $this->data['global'] = $this->globalSettingRepository->findBySlugOrFail($slug);
            return view('admin.contents.globalsettings.index', $this->data);
        } catch (\Throwable $th) {
            return view('admin.errors.404');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $slug)
    {
        try {
            $global = $this->globalSettingRepository->findBySlugOrFail($slug);
            $data = [
                'company_name' => $request['name'],
                'slug' => str_slug($request['name']),
                'company_phone' => $request['phone'],
                'company_email' => $request['email'],
                'company_facebook' => $request['facebook'],
                'company_youtube' => $request['youtube'],
                'company_googleplus' => $request['google'],
                'company_address' => $request['address'],
                'company_copyright' => $request['copyright'],
            ];
            $this->globalSettingRepository->update($data, $slug);
            return redirect()->route('global-setting.index')->with('status_s', 'Đã lưu chỉnh sửa thông tin cho website');
        } catch (\Throwable $th) {
            return redirect()->back()->with('status_d', 'Đã có lỗi xảy ra, không thể lưu.');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
