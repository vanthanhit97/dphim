<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Admin\Actor\StoreRequest;
use App\Http\Requests\V1\Admin\Actor\UpdateRequest;
use App\Repositories\Actor\ActorRepositoryInterface;
use App\Repositories\Director\DirectorRepositoryInterface;
use Exception;


class ActorController extends Controller
{
    protected $actorRepository, $directorRepository;

    public function __construct(
        ActorRepositoryInterface $actorRepository, DirectorRepositoryInterface $directorRepository
    ) {
        $this->actorRepository = $actorRepository;
        $this->directorRepository = $directorRepository;
        $this->data['actors_all'] = $this->actorRepository->all();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('job', $request->has('job') ? $request->get('job') : ($request->session()->has('job') ? $request->session()->get('job') : -1));
        $request->session()->put('table_length', $request->has('table_length') ? $request->get('table_length') : ($request->session()->has('table_length') ? $request->session()->get('table_length') : 5));
        $this->data['keyword_s'] = $request->session()->get('search');
        $request->session()->forget('search');
        $this->data['actors_all'] = $this->actorRepository->paginateSearch('first_name', $request->session()->get('search'), 'created_at', 'DESC',  $request->session()->get('table_length'));
        $request->session()->put('search', '');
        if ($request->ajax()) {
            return view('admin.contents.actors.list', $this->data);
        } else {
            return view('admin.contents.actors.index', $this->data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.contents.actors.create', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        //
            \DB::beginTransaction();
            try {
            $data = $request->only([
                'first_name',
                'last_name',
                'job',
                'gender',
                'height',
                'weight',
                'blood_group',
                'hobby',
                'country',
                'status',
            ]);
            if ($request->avatar) {
                $data['avatar'] = $this->actorRepository->uploadImage($request->avatar);
            }
            if ($request->job == 'actor') {
                $result = $this->actorRepository->create($data);
            } else {
                $result = $this->directorRepository->create($data);
            }
            if($result && $request->avatar)
            {
                $this->actorRepository->moveImage($request->avatar, $data['avatar']);
            }
            \DB::commit();
            return redirect()->route('actors.index')->with('status_s', 'Đã lưu dữ liệu thành công');
            } catch (\Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể thực hiện ! Đã có lỗi xảy ra.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
        try {
            $this->data['actors'] = $this->actorRepository->findBySlugOrFail($slug);
            return view('admin.contents.actors.show', $this->data);
        } catch (\Throwable $th) {
            return view('admin.errors.404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        //
        try {
            $this->data['actors'] = $this->actorRepository->findBySlugOrFail($slug);
            return view('admin.contents.actors.edit', $this->data);

        }   catch (Exception $exception) {
            return view('admin.errors.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $slug)
    {
        //
            \DB::beginTransaction();
            try {
            $this->actorRepository->findBySlugOrFail($slug);
            $data = [
                'first_name'  => $request['first_name'],
                'last_name'   => $request['last_name'],
                'slug'        => str_slug($request['first_name'].' '.$request['last_name']),
                'job'         => $request['job'],
                'gender'      => $request['gender'],
                'height'      => $request['height'],
                'weight'      => $request['weight'],
                'blood_group' => $request['blood_group'],
                'hobby'       => $request['hobby'],
                'country'     => $request['country'],
                'status'      => $this->actorRepository->checkStatus($request['status']),
            ];
            if($request->avatar) {
                $data['avatar'] = $this->actorRepository->uploadImage($request->avatar);
            }
            $result = $this->actorRepository->update($data, $slug);
            if($request->avatar && $result) {
                $this->actorRepository->moveImage($request->avatar, $data['avatar']);
            }
            // if ($request->job == 'actor') {
            //     $result = $this->actorRepository->update($data, $slug);
            // } else {
            //     $result = $this->directorRepository->update($data, $slug);
            // }
            // dd($data);
            \DB::commit();
            return redirect()->route('actors.index')->with('status_s', 'Đã cập nhật thành công !');
        }   catch (\Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể thực hiện ! Đã có lỗi xảy ra.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        //
        \DB::beginTransaction();
        try {
            $actors = $this->actorRepository->findBySlugOrFail($slug);
            $this->actorRepository->destroy($actors->id);
            \DB::commit();
            return redirect()->route('actors.index')->with('status_p', 'Đã xóa thành công !');
        } catch (Exception $exception) {
            \DB::rollback();
            return redirect()->route('actors.index')->with('status_d', 'Không thể xóa. Đã có lỗi xảy ra.');
        }
    }
}
