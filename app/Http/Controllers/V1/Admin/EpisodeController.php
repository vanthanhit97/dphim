<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Episode\EpisodeRepositoryInterface;
use App\Repositories\Film\FilmRepositoryInterface;
use App\Http\Requests\V1\Admin\Episode\StoreRequest;
use App\Http\Requests\V1\Admin\Episode\UpdateRequest;
use Exception;

class EpisodeController extends Controller
{
    protected $episodeRepository;
    protected $filmRepository;

    public function __construct(
       
        FilmRepositoryInterface $filmRepository,
        EpisodeRepositoryInterface $episodeRepository
    )
    {
        $this->filmRepository = $filmRepository;
        $this->data['films_all'] = $this->filmRepository->pluck('title','id');
        $this->episodeRepository = $episodeRepository;    
        $this->data['episodes_all'] = $this->episodeRepository->orderBy('created_at', 'DESC');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('table_length', $request->has('table_length') ? $request->get('table_length') : ($request->session()->has('table_length') ? $request->session()->get('table_length') : 10));

        $this->data['episodes_all'] = $this->episodeRepository->paginateSearch('episode', $request->session()->get('search'), 'created_at', 'DESC',  $request->session()->get('table_length'));
        $this->data['keyword_s'] = $request->session()->get('search');
        $request->session()->forget('search');
        if ($request->ajax()) {
            return view('admin.contents.episodes.list', $this->data);
        } else {
            return view('admin.contents.episodes.index', $this->data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        \DB::beginTransaction();
        try {  
            $data = $request->only([
                'film_id',
                'episode',
                'link'
            ]);
            $this->episodeRepository->create($data);
            \DB::commit();
            return redirect()->back()->with('status_s', 'Đã lưu dữ liệu thành công');
        } catch (\Throwable $th) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể thực hiện ! Đã có lỗi xảy ra.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $this->data['episodes'] = $this->episodeRepository->find($id);
            return view('admin.contents.episodes.edit', $this->data);
        } catch (Exception $exception) {
            return view('admin.errors.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        \DB::beginTransaction();
        try {
            $this->episodeRepository->find($id);
            $data = [
                'film_id' => $request['film_id'],
                'episode' => $request['episode'],
                'link' => $request['link'],
                'status'=> $this->episodeRepository->checkStatus($request['status']),
            ];
            $this->episodeRepository->update($data, $id);
            \DB::commit();
            return redirect()->route('episodes.index')->with('status_s', 'Đã cập nhật thành công !');
        } catch (Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể cập nhật ! Đã có lỗi xảy ra.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            $episodes = $this->episodeRepository->find($id);
            $this->episodeRepository->destroy($episodes->id);
            \DB::commit();
            return redirect()->route('episodes.index')->with('status_p', 'Đã xóa thành công !');
        } catch (Exception $exception) {
            \DB::rollback();
            return redirect()->back()->with('status_d', 'Không thể xóa. Đã có lỗi xảy ra.');
        }
    }
}
