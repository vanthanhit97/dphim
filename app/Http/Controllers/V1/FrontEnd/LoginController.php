<?php

namespace App\Http\Controllers\V1\FrontEnd;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Repositories\User\UserRepositoryInterface;
use App\Http\Controllers\V1\FrontEnd\FrontEndController;
use App\Http\Requests\V1\Admin\Login\ChangePasswordRequest;

class LoginController extends Controller
{
    //
    protected $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ){

        $this->userRepository = $userRepository;
    }
    public function getLogin()
    {
        if (!\Auth::guard('user')->check()) {
            return view('frontends.pages.login');
        } else {
            return redirect()->back();
        }
    }
    public function postLogin(LoginRequest $request)
    {
        $rememberMe = false;
        if (isset($request->remember)) {
            $rememberMe = true;
        }
        $login_type = filter_var($request['email'], FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'name';
        $request->merge([
            $login_type => $request['email'],
            'status' => 1
        ]);
        $data = $request->only([
            $login_type,
            'password',
            'status',
        ]);
        // dd($data);
        return $this->userRepository->login($data, $rememberMe);
    }
    public function getLogout()
    {
        return $this->userRepository->logout();
    }
    public function changepassword(ChangePasswordRequest $request)
    {
        if ($this->userRepository->changepassword($request->password_old, $request->password_new)) {
            return redirect()->back()->with('status_s', 'Thay đổi mật khẩu thành công !!!');
        }
            return redirect()->back()->with('checkpassword', 'Mật khẩu cũ không tồn tại. Vui lòng nhập lại');
    }
    public function getProfile()
    {

    }
}
