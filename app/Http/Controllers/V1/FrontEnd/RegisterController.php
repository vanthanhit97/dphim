<?php

namespace App\Http\Controllers\V1\FrontEnd;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
class RegisterController extends Controller
{
    // protected $userRepository;

    // public function __construct(
    //     UserRepositoryInterface $userRepository
    // ) {
    //     $this->userRepository = $userRepository;
    //     $this->data['users_all'] = $this->userRepository->all();
    // }
    // public function getRegister() {
    //     return view('frontends/pages/sign-up');
    // }
    // public function postRegister(RegisterRequest $request)
    // {
    //     \DB::beginTransaction();
    //         try {
    //         $data = $request->only([
    //             'name',
    //             'email',
    //             'password'
    //         ]);
    //         return $this->userRepository->create($data);
    //         // dd($data);
    //         \DB::commit();
    //         return redirect()->route('front-end.sign-up')->with('status_s', 'Đã lưu dữ liệu thành công');
    //         } catch (\Exception $exception) {
    //         \DB::rollback();
    //         return redirect()->back()->with('status_d', 'Không thể thực hiện ! Đã có lỗi xảy ra.');
    //         // dd($data);
    //     }

    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(array $data)
    {
        //
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        dd($data);
        return view('frontends.pages.sign-up', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterRequest $request)
    {
        //
        // $this->data= $request->only([
        //     'name',
        //     'email',
        //      Hash::make('password')
        // ]);
        // $data = User::create(request(['name', 'email', 'password']));
        // auth()->register($data);
        // dd($data);
        // return view('frontends.pages.sign-up', $this->data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
