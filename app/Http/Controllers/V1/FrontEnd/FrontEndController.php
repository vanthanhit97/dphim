<?php

namespace App\Http\Controllers\V1\FrontEnd;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Film\FilmRepositoryInterface;
use App\Repositories\GlobalSetting\GlobalSettingRepositoryInterface;
use App\Repositories\Trailer\TrailerRepositoryInterface;
use App\Repositories\Tag\TagRepositoryInterface;
use App\Repositories\Actor\ActorRepositoryInterface;
use App\Repositories\Director\DirectorRepositoryInterface;
use App\Models\Film;
use Illuminate\Support\Facades\Event;
use Exception;
use Log;
use App\Models\Tag;
use Illuminate\Support\Facades\Input;
use App\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use App\Http\Requests\V1\Admin\User\RegisterRequest;
use Illuminate\Support\Facades\Auth;

class FrontEndController extends Controller
{
    protected $category, $film, $global, $trailer, $tag, $actor, $director, $user;

    public function __construct(CategoryRepositoryInterface $category, FilmRepositoryInterface $film, GlobalSettingRepositoryInterface $global,
    TrailerRepositoryInterface $trailer, TagRepositoryInterface $tag, ActorRepositoryInterface $actor, DirectorRepositoryInterface $director,
    UserRepositoryInterface $user)
    {
        $this->category = $category;
        $this->film = $film;
        $this->global = $global;
        $this->trailer = $trailer;
        $this->tag = $tag;
        $this->actor = $actor;
        $this->director = $director;
        $this->user = $user;
        $this->data['categories'] = $category->whereNull('parent_id', 'id')->where('status', '1');
        $this->data['genres'] = $category->whereNotNull('parent_id', 'name')->where('parent_id', 1)->where('status', '1')->pluck('name', 'slug');
        $this->data['country'] = $category->whereNotNull('parent_id', 'created_at')->where('parent_id', 2)->where('status', '1')->pluck('name', 'slug');
        $this->data['year'] = $category->whereNotNull('parent_id', 'created_at')->where('parent_id', 3)->where('status', '1')->pluck('name', 'slug');
        $this->data['globalSetting'] = $global->all()->first(); 
        $this->data['tags_hl'] = $tag->countOfOccurrences('films', 'films_count', 'DESC', 25);
        $this->data['trailers'] = $trailer->orderBy('created_at', 'DESC')->take(10);
        $this->data['films_news'] = $film->orderBy('created_at', 'DESC')->take(10);

        $this->data['globalSetting'] = $global->all()->first();
    }

    public function homePage()
    {
        $this->data['film_slide'] = $this->film->orderBy('viewed', 'DESC')->take(3);

        $this->data['film_old_mostview'] = Film::whereHas('categories', function ($query) {
            $query->where('slug', 'phim-le')->orWhere('parent_id', 3);
        })->orderBy('viewed', 'DESC')->get()->take(8);
        $this->data['film_odd'] = Film::whereHas('categories', function ($query) {
            $query->where('slug', 'phim-le')->orWhere('parent_id', 3);
        })->with('episodes')
            ->join('episodes', 'episodes.film_id', '=', 'films.id')
            ->select('films.*')
            ->orderBy('episodes.created_at', 'DESC')
            ->get()->unique()->values()->take(8);

        $this->data['film_series'] = Film::whereHas('categories', function ($query) {
            $query->where('slug', 'phim-bo')->orWhere('parent_id', 4);
        })->with('episodes')
            ->join('episodes', 'episodes.film_id', '=', 'films.id')
            ->select('films.*')
            ->orderBy('episodes.created_at', 'DESC')
            ->get()->unique()->values()->take(12);

        $this->data['anime_series'] = Film::whereHas('categories', function ($query) {
            $query->where('slug', 'phim-hoat-hinh');
        })->with('episodes')
            ->join('episodes', 'episodes.film_id', '=', 'films.id')
            ->select('films.*')
            ->orderBy('episodes.created_at', 'DESC')
            ->get()->unique()->values()->take(12);

        return view('frontends.pages.home', $this->data);
    }

    public function getFilmsByGenres(Request $request, $slug)
    {
        try {
            $categoryFind = $this->data['category_genres'] = $this->category->findBySlugOrFail($slug);
            $sort = $request->filter_sort ? $request->filter_sort == 'posttime' ? 'created_at' : $request->filter_sort : 'created_at';
            $language = $request->filter_language ? ($request->filter_language == 'vi') ? 'viet' : (($request->filter_language == 'illustrate') ? 'thuyet minh' :
                        (($request->filter_language == 'dubbing') ? 'long tieng' : 'viet sub')) : '';
            $genres = $request->filter_genres ? $request->filter_genres : '';
            $country = $request->filter_country ? $request->filter_country : '';
            $year = $request->filter_year ? $request->filter_year : '';

            if($categoryFind->parent_id == null) {
                $films = Film::whereHas('categories', function ($query) use ($categoryFind){
                    $query->where('name', $categoryFind->name)->orWhere('parent_id', $categoryFind->id);
                });
            } else {
                $films = Film::whereHas('categories', function ($query) use ($categoryFind) {
                    $query->where('name', $categoryFind->name);
                });
            }
            if ($genres) {
                $films->whereHas('categories', function ($q) use ($genres) {
                    $q->where('slug', $genres);
                });
            }
            if ($country) {
                $films->whereHas('categories', function ($q) use ($country) {
                    $q->where('slug', $country);
                });
            }
            if ($year) {
                $films->whereHas('categories', function ($q) use ($year) {
                    $q->where('slug', $year);
                });
            }
            $this->data['films_genres'] = $films->where('language', 'like', '%' . $language . '%')->orderBy($sort, 'DESC')->paginate(18);
            return view('frontends.pages.genres', $this->data);
        } catch (\Throwable $th) {
            return view('admin.errors.404');
        }
    }

    public function getFilmsInfo($slug)
    {
        $films = $this->data['film_info'] = $this->film->findBySlugOrFail($slug);
        $this->data['f_country'] = $this->category->where('slug', 'quoc-gia')->pluck('id');
        $this->data['f_year'] = $this->category->where('slug', 'phim-le')->pluck('id');
        $this->data['f_kind'] = $this->category->where('slug', 'the-loai')->pluck('id');
        $this->data['cat_film'] = $films->categories()->whereNotIn('parent_id', [1, 2])->get()->first();
        $this->data['film_related'] = Film::whereHas('tags', function ($q) use ($films) {
            return $q->whereIn('name', $films->tags->pluck('name'));
        })->orWhereHas('categories', function ($query) use ($films) {
            $query->where('name', $films->categories->pluck('name'))->orWhere('parent_id', $films->categories->pluck('id'));
        })->where('id', '<>', $films->id)->orderBy('created_at', 'DESC')->inRandomOrder()
            ->get()->take(6);
        return view('frontends.pages.info-film', $this->data);
    }

    public function watchFilm($slug)
    {
        try {
            $films = $this->data['film_info'] = $this->film->findBySlugOrFail($slug);
            if (count($films->episodes) <= 0) {
                return view('admin.errors.404');
            }
            Event::dispatch('films.view', $films);
            $this->data['cat_film'] = $films->categories()->whereNotIn('parent_id', [1, 2])->get()->first();
            $this->data['film_related'] = Film::whereHas('tags', function ($q) use ($films) {
                return $q->whereIn('name', $films->tags->pluck('name'));
            })->orWhereHas('categories', function ($query) use ($films) {
                $query->where('name', $films->categories->pluck('name'))->orWhere('parent_id', $films->categories->pluck('id'));
            })->where('id', '<>', $films->id)->orderBy('created_at', 'DESC')->inRandomOrder()
                ->get()->take(12);
            return view('frontends.pages.single', $this->data);
        } catch(Exception $exception) {
            Log::error($exception);
            return view('admin.errors.404');
        }
    }

    public function episodeOfFilm($slug)
    {
        $films = $this->film->findBySlugOrFail($slug);
        $episodes = $films->episodes->pluck('link', 'episode');
        return response()->json($episodes);
    }

    public function getSearch()
    {
        $this->data['keyword'] = $keywords = Input::get('keyword');
        $this->data['films'] = Film::where('title', 'like', '%'.$keywords.'%')->orWhere('title_eng', 'like', '%'.$keywords.'%')
                    ->orWhere('description', 'like', '%'.$keywords.'%')->orderBy('created_at', 'DESC')->paginate(20)->setPath('');
        $pagination = $this->data['films']->appends(array ('q' => Input::get('keyword')));
        // dd($pagination);
        return view('frontends.pages.search', $this->data);
        
    }

    public function getDirectorProfile($slug) 
    {
        try {
            $profile = $this->data['profile'] = $this->director->findBySlugOrFail($slug);
            $this->data['films'] = Film::whereHas( 'director', function ($q) use ($profile) {
                return $q->where('id', $profile->id);
            })->orderBy('created_at', 'DESC')->paginate(6);
            return view('frontends.pages.profile', $this->data);
        } catch(Exception $exception) {
            Log::error($exception);
            return view('admin.errors.404');
        }
    }

    public function getActorProfile($slug)
    {
        try {
            $profile = $this->data['profile'] = $this->actor->findBySlugOrFail($slug);
            $this->data['films'] = Film::whereHas( 'actors', function ($q) use ($profile) {
                return $q->where('actor_id', $profile->id);
            })->orderBy('created_at', 'DESC')->paginate(6);
            return view('frontends.pages.profile', $this->data);
        } catch(Exception $exception) {
            Log::error($exception);
            return view('admin.errors.404');
        }
    }
	public function getSignUp()
    {
        if (!\Auth::guard('user')->check()) {
            return view('frontends.pages.sign-up', $this->data);
        } else {
            return redirect()->back();
        }
    }
	public function postSignUp(RegisterRequest $request)
    {
        $user = $request->only([
            'name',
            'email',
        ]);
        $user['password'] = Hash::make($request->password);
        if($this->user->create($user)) {
            return redirect()->route('front-end.login');
        } else {
            return redirect()->back()->with('status', 'Không thể thực hiện. Đã có lỗi xảy ra !!!');
        }
    }

    public function showProfile()
    {
        if (\Auth::guard('user')->check()) {
            $this->data['user'] = Auth::user();
            return view('frontends.pages.user-profile', $this->data);
        } else {
            return view('admin.errors.404');
        }
    }
}
