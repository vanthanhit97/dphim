<?php

namespace App\Http\Middleware;

use App\Http\Middleware\UserMiddleware as Middleware;
class UserMiddleware extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // protected function userAuth()
    // {
    //     if(Auth::guard('user')->check()){
    //         return redirect('/home');
    //     }
    //     else
    //     {
    //         return redirect('front-end.login');
    //     }
    // }
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('front-end.login');
        }
    }
}
