<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'user_id',
        'film_id',
        'content',
        'status',
    ];

    public function film()
    {
        return $this->belongsTo('App\Models\Film');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
