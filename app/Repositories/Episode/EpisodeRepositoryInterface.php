<?php

namespace App\Repositories\Episode;

use App\Repositories\BaseInterface;

interface EpisodeRepositoryInterface extends BaseInterface
{
    
    public function update(array $attributes, $id);
}
