<?php

namespace App\Repositories\Episode;

use App\Repositories\BaseRepository;
use App\Repositories\Episode\EpisodeRepositoryInterface;
use App\Models\Episode;

class EpisodeRepository extends BaseRepository implements EpisodeRepositoryInterface
{
    public function model()
    {
        return Episode::class;
    }
    public function update(array $attributes, $id)
    {
        $_model = $this->_model->findOrFail($id);
        $_model->fill($attributes);
        $_model->save();

        return $this;
    }
}
