<?php

namespace App\Repositories\GlobalSetting;

use App\Repositories\BaseRepository;
use App\Repositories\GlobalSetting\GlobalSettingRepositoryInterface;
use App\Models\GlobalSetting;

//use Your Model

/**
 * Class GlobalSettingRepository.
 */
class GlobalSettingRepository extends BaseRepository implements GlobalSettingRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return GlobalSetting::class;
    }
}
