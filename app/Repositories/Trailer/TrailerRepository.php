<?php

namespace App\Repositories\Trailer;

use App\Models\Trailer;
use App\Repositories\BaseRepository;
use App\Repositories\Trailer\TrailerRepositoryInterface;

/**
 * Class TrailerRepository.
 */
class TrailerRepository extends BaseRepository  implements TrailerRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Trailer::class;
    }
    public function update(array $attributes, $id)
    {
        $_model = $this->_model->findOrFail($id);
        $_model->fill($attributes);
        $_model->save();

        return $this;
    }
}
