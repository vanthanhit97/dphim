<?php

namespace App\Repositories\Trailer;

use App\Repositories\BaseInterface;

interface TrailerRepositoryInterface extends BaseInterface
{


    public function update(array $attributes, $id);

}
