<?php

namespace App\Repositories\Tracker;

use App\Repositories\BaseRepository;
use App\Repositories\Tracker\TrackerRepositoryInterface;
//use Your Model
use Soumen\Agent\Agent;
use App\Models\Tracker;

/**
 * Class TrackerRepository.
 */
class TrackerRepository extends BaseRepository  implements TrackerRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Tracker::class;
    }

    public function device()
    {
        return Agent::device()->isMobile == true ? 'Mobile' 
                    : Agent::device()->isTablet == true ? 'Tablet'
                    : Agent::device()->isDesktop == true ? 'Desktop'
                    : 'Bot';
    }

    public function browserName()
    {
        return  Agent::browser()->family;
    }

    public function browserVersion()
    {
        return  Agent::browser()->version;
    }

    public function browserPlatform()
    {
        return  Agent::platform()->family;
    }

    public function ip()
    {
        return  Agent::ip();
    }
}
