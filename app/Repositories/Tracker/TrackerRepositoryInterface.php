<?php

namespace App\Repositories\Tracker;
use App\Repositories\BaseInterface;

//use Your Model

/**
 * Class TrackerRepositoryInterface.
 */
interface TrackerRepositoryInterface extends BaseInterface
{
	public function device();
	public function browserName();
	public function browserVersion();
	public function browserPlatform();
	public function ip();
}