<?php

namespace App\Repositories\User;

use App\Repositories\BaseInterface;

interface UserRepositoryInterface extends BaseInterface
{
	public function login(array $attributes, $remember);

    public function logout();

    public function changepassword($pass_old, $pass_new);
}
