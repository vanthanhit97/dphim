<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseRepository;
use App\Repositories\User\UserRepositoryInterface;

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository  implements UserRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return User::class;
    }
    public function login(array $attributes, $remember)
    {
        if (\Auth::guard('user')->attempt($attributes, $remember)) {
            return redirect('/');
        } else {
            return redirect()->back()->with('status', 'Email và mật khẩu không đúng');
        }
    }

    public function logout()
    {
        if (\Auth::guard('user')->check()) {
            \Auth::guard('user')->logout();
            return redirect()->guest(route('front-end.index'));
        }
    }

    public function changepassword($pass_old, $pass_new)
    {
        $data = [
            'password' => \Hash::make($pass_new)
        ];
        try {
            if (\Auth::check()) {
                if (\Hash::check($pass_old, \Auth::guard('user')->user()->password)) {
                    $user = \Auth::guard('user')->user()->slug;
                    $userFind = $this->findBySlugOrFail($user);
                    $userFind->update($data);
                    return true;
                    // dd($data);
                }
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}
