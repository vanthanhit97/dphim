<?php

namespace App\Repositories\Film;

use App\Repositories\BaseRepository;
use App\Repositories\Film\FilmRepositoryInterface;
use App\Models\Film;

class FilmRepository extends BaseRepository implements FilmRepositoryInterface
{
    public function model()
    {
        return Film::class;
    }

    public function createManyToMany(array $attributes1, $attributes2, $attributes3, $attributes4)
    {
        $film = $this->create($attributes1);
        $film->categories()->attach($attributes2);
        $film->tags()->attach($attributes3);
        $film->actors()->attach($attributes4);
        return true;
    }

    public function updateManyToMany($key_value, array $attributes1, $attributes2, $attributes3, $attributes4)
    {
        $key_value->update($attributes1);
        $key_value->categories()->sync($attributes2);
        $key_value->tags()->sync($attributes3);
        $key_value->actors()->sync($attributes4);
        return true;
    }
}
