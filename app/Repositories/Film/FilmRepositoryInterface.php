<?php

namespace App\Repositories\Film;

use App\Repositories\BaseInterface;

interface FilmRepositoryInterface extends BaseInterface
{
    public function createManyToMany(array $attributes1, $attributes2, $attributes3, $attributes4);

    public function updateManyToMany($key_value, array $attributes1, $attributes2, $attributes3, $attributes4);
}
