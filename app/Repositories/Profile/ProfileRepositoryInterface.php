<?php

namespace App\Repositories\Profile;

use App\Repositories\BaseInterface;

interface ProfileRepositoryInterface extends BaseInterface
{


    public function update(array $attributes, $id);
    public function checkGender($value);

}
