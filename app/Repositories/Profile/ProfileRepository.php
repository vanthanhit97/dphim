<?php

namespace App\Repositories\Profile;

use App\Models\Profile;
use App\Repositories\BaseRepository;
use App\Repositories\Profile\ProfileRepositoryInterface;

/**
 * Class ProfileRepository.
 */
class ProfileRepository extends BaseRepository  implements ProfileRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Profile::class;
    }
    public function update(array $attributes, $id)
    {
        $_model = $this->_model->findOrFail($id);
        $_model->fill($attributes);
        $_model->save();

        return $this;
    }
    public function checkGender($value)
    {
        $gender = 0;
        if ($value != null) {
            $gender = $value;
        }
        return $gender;
    }
}
