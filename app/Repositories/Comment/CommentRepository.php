<?php

namespace App\Repositories\Comment;

use App\Repositories\BaseRepository;
use App\Repositories\Comment\CommentRepositoryInterface;
use App\Models\Comment;

class CommentRepository extends BaseRepository implements CommentRepositoryInterface
{
    public function model()
    {
        return Comment::class;
    }
}
