<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Repositories\Film\FilmRepository;
use App\Repositories\Film\FilmRepositoryInterface;
use App\Repositories\GlobalSetting\GlobalSettingRepositoryInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\Admin\AdminRepositoryInterface::class,
            \App\Repositories\Admin\AdminRepository::class
        );
        
        $this->app->bind(
            \App\Repositories\Category\CategoryRepositoryInterface::class,
            \App\Repositories\Category\CategoryRepository::class
        );
        $this->app->bind(
            \App\Repositories\Advertisement\AdvertisementRepositoryInterface::class,
            \App\Repositories\Advertisement\AdvertisementRepository::class
        );

        $this->app->bind(
            \App\Repositories\Tag\TagRepositoryInterface::class,
            \App\Repositories\Tag\TagRepository::class
        );

        $this->app->bind(
            \App\Repositories\Film\FilmRepositoryInterface::class,
            \App\Repositories\Film\FilmRepository::class
        );
		
		$this->app->bind(
            \App\Repositories\Actor\ActorRepositoryInterface::class,
            \App\Repositories\Actor\ActorRepository::class
        );
		
		$this->app->bind(
            \App\Repositories\Director\DirectorRepositoryInterface::class,
            \App\Repositories\Director\DirectorRepository::class
        );
		
        $this->app->bind(
            \App\Repositories\Trailer\TrailerRepositoryInterface::class,
            \App\Repositories\Trailer\TrailerRepository::class
        );

        $this->app->bind(
            \App\Repositories\Profile\ProfileRepositoryInterface::class,
            \App\Repositories\Profile\ProfileRepository::class
        );
        $this->app->bind(
            \App\Repositories\User\UserRepositoryInterface::class,
            \App\Repositories\User\UserRepository::class
        );
        $this->app->bind(
            \App\Repositories\Comment\CommentRepositoryInterface::class,
            \App\Repositories\Comment\CommentRepository::class
        );
        $this->app->bind(
            \App\Repositories\Episode\EpisodeRepositoryInterface::class,
            \App\Repositories\Episode\EpisodeRepository::class
        );

        $this->app->bind(
            \App\Repositories\GlobalSetting\GlobalSettingRepositoryInterface::class,
            \App\Repositories\GlobalSetting\GlobalSettingRepository::class
        );

        $this->app->bind(
            \App\Repositories\Tracker\TrackerRepositoryInterface::class,
            \App\Repositories\Tracker\TrackerRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
